#!/bin/bash

echo "Commiting and pushing to remote..."
git add . && git commit -m "$1" && git push origin master

echo "Building thesis..."
./build.sh

echo "Removing auxilary files..."
rm *.aux *.fdb* *.log *.fls *.acn *.glo *.ist *.toc *.bbl
rm -r _minted*
