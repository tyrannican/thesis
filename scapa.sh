#!/bin/bash

# FOR USE ON WORK PC - TOO BIG TO PUSH FROM SCAPA AT HOME
echo "Copying to Scapa..."
./build.sh
rm *.aux *.fdb* *.log *.fls *.acn *.glo *.ist *.toc *.bbl
rm -r _minted*
cp -rv ./* ~/scapa4/group/Graham\ Keenan/Thesis