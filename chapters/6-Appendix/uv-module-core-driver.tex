\begin{minted}[fontsize=\scriptsize]{python}
"""Core Driver for interfacing with OceanOptics Spectrometers

.. moduleauthor:: Graham Keenan 2019
                  Artem Leonov  2019
"""

# Time module for wait times
import time

# Logging to log information to the user
import logging

# Numpy for mathematical operations
import numpy as np

# Seabreeze library to communicate with the spectrometer hardware
import seabreeze
seabreeze.use('cseabreeze')

# Helper object to represent an underlying spectrometer class
import seabreeze.spectrometers as sb

# Spectrometer Types:
SPECS = {
    "UV": "2192",
    "RAMAN": "QE-PRO",
    "IR": "NIRQUEST"
}

class UnsupportedSpectrometer(Exception):
    """Exception for unsupported spectrometer types
    """


class NoSpectrometerDetected(Exception):
    """Exception for when no spectrometer is detected
    """


def _get_spectrometer(spec_type: str) -> str:
    """Gets the Spectrometer from Seabreeze that matches given type

    Arguments:
        spec_type {str} -- Type of spectrometer to look for

    Raises:
        UnsupportedSpectrometer -- If the spec_type is not present

    Returns:
        str -- Name of the spectrometer
    """

    # Get all devices detected by the computer
    devices = sb.list_devices()

    # Cannot find any devices, raise an error
    if not devices:
        raise NoSpectrometerDetected("Are the spectrometers plugged in?")

    # Check if the given spectrometer type is supported by the library
    if spec_type in SPECS.keys():
        # Iterate through all devices detected
        for dev in devices:
            # If the spectrometer is present, return the name as a string
            if SPECS[spec_type] in str(dev):
                return dev

    # Raise an error if the device supplied is not supported
    raise UnsupportedSpectrometer("Spectrometer {} unsupported!".format(spec_type))

class OceanOpticsSpectrometer():
    """Base class for interfacing with OceanOptics Spectrometers"""

    def __init__(self, spec_type, name=None):
        """
        Args:
            spec_type (str): The type of spectrometer, e.g. 'IR', 'raman', etc.
            name (str, optional): Device name for easier access
        """

        # Integration time for the device (time between measurements)
        self.integration_time = 0.01 # in seconds

        # Get the spectrometer name if it is present
        self.__spec = _get_spectrometer(spec_type)

        # Create a spectrometer object to communicate with
        self._spectrometer = sb.Spectrometer(self.__spec)

        # Set the name of the spectrometer
        self.name = name

        # Set a delay between measurements when scanning multiple times
        self._delay = 0.01

        # Create a logger to communicate information to the user
        self.logger = logging.getLogger('oceanoptics.spectrometer')
        self.logger.setLevel(logging.INFO)

        # Sets the logging handlers for nice output
        # Removing default handlers
        self.logger.handlers = []
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        console_formatter = logging.Formatter(
            "%(asctime)s ; %(module)s ; %(name)s ; %(message)s")
        ch.setFormatter(console_formatter)
        self.logger.addHandler(ch)

        # Sets the integration time
        self.set_integration_time(self.integration_time)

    def set_integration_time(self, integration_time):
        """Sets the integration time for the spectrometer

        Args:
            integration_time (float): Desired integration time in seconds!
        """

        # Open communication with the device
        self._spectrometer.open()

        # Update integration time for the device at runtime.
        self.integration_time = integration_time
        integration_time *= 1000 * 1000 # converting to microseconds

        # Set the integration time for the spectrometer
        self.logger.debug('Setting the integration time to %s microseconds', integration_time)
        self._spectrometer.integration_time_micros(integration_time)

        # Close communication
        self._spectrometer.close()

    def scan(self, n: int = 3):
        """Reads the spectrometer and returns the spectrum.
        Averages out n number of scans to give an average value for the spectrum.

        Args:
            n (int, opitonal): Number of 'scans'

        Returns:
            (Tuple): Tuple containing spectrum wavelengths and intensities as numpy arrays
                Example: (array(wavelengths), array(intensities))
        """

        # List to store the avareage intensities
        i_mean = []

        # Open communication with the device
        self.logger.debug('Scanning')
        self._spectrometer.open()

        # Iterate N times
        for i in range(n):
            # Get the wavelengths and intensities measured at that time
            wavelengths, intensities = self._spectrometer.spectrum()

            # Append the intensities to the average list
            # Wavelengths are ignored as they are static and do not change
            i_mean.append(intensities)

            # Wait for a short period before scanning again
            time.sleep(self._delay)

        # Average the intensities
        intensities = np.mean(i_mean, axis=0)

        # Close communication with the device
        self._spectrometer.close()

        # Return the measured wavelengths and average intensities
        return (wavelengths, intensities)

\end{minted}