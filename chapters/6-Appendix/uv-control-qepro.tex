\begin{minted}[fontsize=\scriptsize]{python}
"""
.. module:: QEPro2192
    :synopsis: Module representing the UV QEPro2192 spectrometer
    :platforms: Unix, Windows

.. moduleauthor:: Graham Keenan (Cronin Lab 2020)

"""

# JSON library for handling and parsing raw data
import json

# Path library to make OS path manipulation easier to work with
from pathlib import Path

# Import the UV Spectrum class to create objects from raw data
from .uv_spectrum import UVSpectrum

# Type checking for source code
from typing import Optional, Union, Dict

# Import the underlying spectrometer control code
from ..oceanoptics import OceanOpticsSpectrometer

class NoReferenceException(Exception):
    """Exception for calling spectrum without a reference
    """

class QEPro2192(OceanOpticsSpectrometer):
    """Class representing the QEPro2192 spectrometer

    Inherits:
        OceanOpticsSpectrometer
    """

    def __init__(self):
        # Initialise the spectrometer base class
        super().__init__("UV", name="QEPro2192 UV Spectrometer")

        # Data structure to store reference data
        self.reference = {}

        # Flag to determine if a reference has been called already
        # Required to gain accurate spectrum
        self.__ref_called = False

    def load_reference(self, ref: Union[str, Dict]):
        """Loads a pre-existing reference from disk or from dict.

        Args:
            ref (Union[str, Dict]): Reference as either a dictionary
            or JSON filepath
        """

        # Filepath, load and set reference data if present
        if isinstance(ref, str) or isinstance(ref, Path):
            with open(ref) as fd:
                self.reference = json.load(fd)
                self.__ref_called = True

        # Dict, set
        elif isinstance(ref, dict):
            self.reference = ref
            self.__ref_called = True
        
        # Not supported
        else:
            self.logger.warning(
                f'Reference {ref} is unsupported. Not loading'
            )

    def obtain_reference_spectrum(self) -> UVSpectrum:
        """Obtain a reference spectrum.

        Returns:
            UVSpectrum: Reference UV spectrum
        """

        # Scan the reference sample to obtain values
        wavelengths, intensities = self.scan()

        # Set the wavelengths for the reference
        self.reference["wavelength"] = wavelengths

        # Set the intensities for the reference
        self.reference["intensities"] = intensities

        # Set flag to say reference has been collected
        self.__ref_called = True

        # Return UV Spectrum object of the reference sample
        return UVSpectrum(wavelengths, intensities)

    def obtain_spectrum(self) -> UVSpectrum:
        """Obtain a UV spectrum of a sample.

        Raises:
            NoReferenceException: Attempting to measure a sample without
            a reference

        Returns:
            UVSpectrum: Sample UV spectrum.
        """

        # Raise an error if the reference has not been taken already.
        # Measuring without a reference is not allowed as will produce inaccurate
        # results
        if not self.__ref_called:
            raise NoReferenceException(
                "Attempting to call a spectrum without a valid reference\
                spectrum"
            )

        # Scan the sample to obtain wavelengths and intensities
        wavelengths, intensities = self.scan()

        # Return processed UV Spectrum object
        return UVSpectrum(wavelengths, intensities, ref=self.reference)

\end{minted}