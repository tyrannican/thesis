This discussion has highlighted many different forms of automation that exist for chemists in both industry and academia.
Many of these systems have their benefits but also their drawbacks.
To address these drawbacks, a set of criteria was discussed that would enable automated systems for use in chemistry research to become more affordable and accessible to the wider community.

What is required is an adaptable, open software architecture that can power automated systems to perform all manners of different chemistries.
This framework should be modular in that any hardware control modules can be easily added and removed as and when required.
To streamline hardware interfacing, these modules should be accessible through a single-point-of-access "\acrshort{api}" that is accessible from execution programs.
The framework should act as a general purpose blueprint that can be used to design any form of automated system for use within a chemistry lab with this centralised \acrshort{api} and hardware module controllers at its core.

To address this, and to create a system that is affordable and accessible to all, the work in this thesis details the projects that were undertaken to achieve such a goal.
This work highlights the overall design of a proposed software framework with detailed discussions of its inner workings as well as documenting the many software libraries that were developed to enable this.
This framework was then used to construct three automated systems for use in general inorganic and organic chemistry regimes.
The framework and libraries developed for it hope to achieve the goal of creating an eco-system in which cheap, automated systems for chemical research can be used with the efficiency of commercial alternatives at a fraction of their cost.

\textbf{Chapter \ref{chap-2}} discusses the implementation of three software libraries that were deemed crucial to control the movement operations of any automated system implementation.
As the systems using this framework were to be cost-effective, it was decided to use the cheap, open-source Arduino microcontrollers to act as the heart of the system which would control the moving parts of the automated solutions.
These libraries were developed to greatly simplify the process of communicating with these devices effectively so that each system would not need to implement their own communication logic.
This meant that the complexities of communicating with the underlying devices was abstracted away from the end-user so that any system could be picked up by any user, regardless of their technical or software skills.
This chapter discusses all three libraries in detail with detailed examples given throughout.

\textbf{Chapter \ref{chap-3}} is an overview of the proposed software architecture that would power these automated systems with the Arduino communication logic at its core.
It explains the design overview and the rationale for the programming language used to implement it.
This modular framework was developed using a "divide and conquer" approach in which each hardware controller was treated as its own bespoke software module.
These modules would then be made accessible to the system via a centralised single-point-of-access \acrshort{api} which could be invoked by an execution program.
The chapter discusses in-depth the approach used to design a software module that could be included as part of the framework with an implementation supplied for the Arduino control module as well as well as a detailed description of the proposed \acrshort{api}.

\textbf{Chapter \ref{chap-4}} discusses the three automated systems which used the proposed architecture in their implementations and the goals they achieved.
The first system was a platform used to synthesise inorganic clusters and demonstrated how the framework was used to achieve this.
The second system was designed to synthesise gold nanoparticles using a genetic algorithm, highlighting how the framework can be expanded to support different chemistries and modules as well as different execution programs to operate the workflow.
The third system demonstrates how adaptable the proposed framework is as it was not designed with the framework in mind.
It highlights that the framework can be used to re-design an already implemented system and how it can be used to streamline a workflow and make a project more manageable.

For each system implementation, detailed implementations of the software modules used are supplied as well as software libraries that were developed to further enhance the workflows.
