One of the major barriers to automation of chemical procedures is the financial costs involved.
Many of the systems described in this chapter, whilst versatile and productive, often come at a significant cost (e.g. ChemSpeed).
To overcome this issue, a number of research labs have been pursuing alternatives to commercially available systems and other closed-source systems.

\subsubsection{Droplet Synthesiser}
One such platform was an autonomous droplet synthesiser developed by Henson et. al. in 2015.
The system they developed was based on the three-dimensional printer technology developed by RepRap \cite{reprap}.
The \emph{X-Y} axes of the platform were elongated to increase the working space with the thermoplastic extruder replaced with a liquid-handling module \cite{dropbot}.
Other additions were added to the system as well such as a camera module to observe the reaction stage.
Apart from the metallic frame to hold the system together, most of the mechanical and liquid-handling components were designed and printed using a RepRap printer using polylactic acid material.
Using this printed technology means that any modifications or repairs requested for the system would simply be printed and added to the frame or modules.
An image of the platform can be seen in Figure \ref{fig:intro-dropbot}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{dropbot.png}
    \caption{
        Image of the RepRap droplet synthesiser as reported by Henson et. al.
        The pumps (\textbf{A}) and carriage dispense reactants into a 96-array vial plate (\textbf{B}) after positioning the dispensing station over the vials.
        The carriage (\textbf{C}) contained an automated syringe which would deposit the contents of a given vial to a Petri dish (\textbf{D}) which would generate droplets.
        The camera would then observe the experiment.
        Figure adapted from Henson et. al. \cite{dropbot}.
    }
    \label{fig:intro-dropbot}
\end{figure}

To control this system, two Arduino Mega boards were used to control the pumps, syringes, and stepper motors to drive movement in the \emph{X-Y} axes.
A connected computer to the system would generate \emph{"g-code"} for the system which is the code used by the RepRap printers to print objects.
This code can be thought of as a list of coordinates and step instructions which the firmware interprets and moves the motors to the appropriate locations.
This process was modified so as to operate the droplet synthesiser \cite{dropbot}.

From the outset, this system looks complicated for its capabilities and is relatively expensive however the only complexity is perhaps the software.
All software used within this sytem was built to address the project goals and hence was not designed with extensibility in mind.
All experimental parameters were generated via a custom-built algorithm which can obfuscate the path taken to generate experiments.
However, the software was suitable to achieve the results which the researchers were aiming for.

The hardware used in this system is considerably cheaper than those available commercially.
The metallic "V-slot" frame is available from a variety of CNC-milling providers for a relatively cheap cost.
The 3D-printed parts and modules are also considerably cheaper than custom-made parts from commercial providers.
The price range of 3D-Printers has been dropping over the years and many hobbyist organisations, such as RepRap, are providing fincancially viable "DIY" printer kits.
By purchasing one of these kits as opposed to more expensive commercial alternatives significantly drops the price of being able to fabricate your own custom-built parts.
However, the drawback of this system is the technical skill required to build, operate, and debug it.
Knowledge of firmware programming is necessary to be able to modify any existing code on this platform which can be an issue when new modules are required.
The fact that a dedicated 3D printer is required as well can also be another drawback however this can be overcome by purchasing a pre-built model.
3D design skills are also required to be able to add new features to the system; a skill that is not generally expected of chemists.

Despite the drawbacks, this system has demonstrated that automation of synthetic protocols (in this case droplet synthesis) does not need to be a financial burden.
Using 3D-printing technology and open-sourced electronics, Henson et. al. have developed an automated solution to a research task that can fit inside a standard fumehood and perform with relatively high throughput at a fraction of the cost of industry standards.
It is estimated that the cost of this system is in the region of several hundred dollars which is far more achievable for smaller research groups than the several thousands that commercial alternatives ask for.

\subsubsection{DIY Syringe Pumps}
Another example of an automated solution is one which is not a dedicated synthesis platform but one of significant use across many fields: the syringe pump.
Syringe pumps have been in use across many different scientific disciplines such as flow-chemistry \cite{vit2}, physics \cite{vit1}, microfluidics \cite{vit3, vit4}, and biology \cite{vit5}.
However, even with their extensive use and variety of applications, some of the cheapest pumps can cost in excess of €1000 \cite{vit6}.
To address this issue, Saggiomo et. al. took a "DIY" approach and created their own bespoke syringe pump constructed from an open-sourced 3D-Printer kit \cite{syringe-pump}.
Their approach consisted of constructing the 3D-Printer to create the pieces required for their construction which was then disassembled and the parts used to construct the body of the pump (Figure \ref{fig:intro-vittorio}).
Using the 3D-Printer firmware \cite{marlin}, which accepts \emph{"g-code"} as described previously, Saggiomo et. al. developed a customised system that could accept g-code to operate the 3D-Printer motors similar to a syringe pump.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.325]{vittorio.png}
    \caption{
        DIY syringe pumps constructed from the parts of an Ender 3D-Printer kit as demonstrated by Saggiomo et. al.
        Additional components were purchased at minimal costs with all custom parts 3D printed.
        Figure adapted from Saggiomo et. al. \cite{syringe-pump}
    }
    \label{fig:intro-vittorio}
\end{figure}

This strategy of constructing syringe pumps from a repurposed 3D-Printer controlled via g-code is demonstrated to be a simple procedure with small financial cost \cite{vit25}.
Using this approach opens up the use of custom-made hardware devices throughout research labs at a fraction of the cost of commercially available alternatives.
With automated control using g-code via the printer firmware, it allows researchers who may not have electronics or programming skills to operate the device with a simple text file detailing the instructions.
These instructions are detailed in full in the supporting publication \cite{syringe-pump}.
Even though this system is not a full-scale automated synthesis platform, it still is a cheap alternative to an automated solution within a research context that merits its own entry.