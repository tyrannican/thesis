Many other automated systems for chemical synthesis have been reported that do not fall under the banner of \acrshort{hts} or iterative synthesis.
These systems are usually focused on more general chemistry applications such as automation of synthetic steps in batch syntheses.
One such system is the ChemKonzert platform developed by Takeda et. al. in 2010 (Figure \ref{fig:intro-chemkonzert}).

\subsubsection{ChemKonzert System}
The ChemKonzert was a modular batch synthesis system which provided liquid-handling, drying, stirring and separation operations with the separation achieved via a centrifuge unit \cite{chemkonzert}.
To control the platform, Takeda et. al. developed their own bespoke control software known as \emph{KonzMeister}.
This gave users the ability to program their own syntheses using commonly used chemistry terms as found in the literature (\textbf{Separate}, \textbf{Stir Flask}, \textbf{Extraction}, etc.).
These steps would be joined together to arrange a sequence of operations that described the full synthesis for the system to execute.
In total, five separate experiments were executed to demonstrate the system's capabilities with notable compounds being produced after publication \cite{m25, m69}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.9]{chemkonzert.jpeg}
    \caption{
        Image of the ChemKonzert platform developed by Takeda et. al.
        The system is comprised of: Two reaction flasks  (\textbf{RF1} and \textbf{RF2}).
        A centrifugal separator (\textbf{SF} (700 mL)).
        Two receiving units (\textbf{SF2} and \textbf{SF2} (500 mL)).
        Two glass filters (\textbf{FF1} (500 ml) and \textbf{FF2} (100 mL)).
        Twelve substrate and reagent reserviors (\textbf{RR1-12} (100-200 mL)).
        Six solvent and wash solution flasks (\textbf{RS1-6}, (500 mL)).
        Three drying pads (\textbf{DT1-3}).
        One round-bottomed flask (\textbf{CF}).
        Two solvent tanks (\textbf{WT1} and \textbf{WT2}).
        One computer controller.
        Figure adapted from Takeda et. al. and reproduced with permission from ACS \cite{chemkonzert}.
    }
    \label{fig:intro-chemkonzert}
\end{figure}

The ChemKonzert system offers a general automation solution for batch synthesis protocols.
This implies that the system can effectively automated syntheses in the literature that conform to the system's capabilities.
By taking the batch synthesis approach, the ChemKonzert platform is more suited to a research lab environment as this can potentially automate what can be considered as laborious synthetic tasks.
Preparation of starting or intermediate materials would be an ideal application for this system which could be used to "skip" manual stages of a synthetic protocol.
However, with a small instruction set, this limits the number of syntheses that can be performed by the platform.
The cost of replicating the ChemKonzert is unknown as this was not detailed in their publications.

\subsubsection{Mobile Robotic Chemist System}
Another automated system that is in use is one that was reported by Cooper et. al. in 2020.
Their previous work reports using a ChemSpeed platform to perform \acrshort{hts} on a number of compounds including porous liquids \cite{d20}, organic crystallisation \cite{d21}, and organic cages and catenanes \cite{d19}.
However, they moved into a more general synthetic regime by constructing the "mobile robotic chemist".
This system consists of a robotic arm which was integrated with other lab instruments that could move autonomously around a dedicated space (Figure \ref{fig:intro-cooper}).
The system was primarily focused on developing a new photocatalyst for the hydrogen production from water \cite{cooper}.
The arm would prepare solids and liquids into reaction vials that would then be transferred to a sonication station for dissolution.
This would then transfer the prepared solutions to a photolysis platform that would then undergo UV-Vis photolysis before being transferred to a gas chromatography to analyse the gas phase for hydrogen production.
All of this occurred under autonomous control with the robotic apparatus moving around the lab space to the various different stations.

This approach is somewhat different to previously discussed automation solutions as this is not focused on automating the process of synthesis but attempting to replicate the movements and operations a human would perform.
However, this approach can be seen as a limitation of the system.
Whilst it effectively replicates human behaviour in a lab environment, it lacks the ability to perform many synthetic operations that may be required of synthesis such as purification and separation of liquids.
These features may be added at a future date but as this was reported, the system lacks these capabilities which limits the platform to perform simple synthetic protocols.
However, this platform has potential as it mimics human actions.
Over time it can be programmed to execute more synthetic steps.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.75]{cooper.jpeg}
    \caption{
        Image of the robotic arm used in the mobile chemist as reported by Cooper et. al. \cite{cooper}
        Figure reproduced with permission from Springer.
    }
    \label{fig:intro-cooper}
\end{figure}

\subsubsection{Plug-and-play Flow Chemistry}
In 2018, Bédard et. al. reported a reconfigurable automated system that was used for the optimisation of many different classes of chemical reactions.
This system was designed as a plug-and-play continuous-flow platform that could have different flow modules added/replaced dependent on the reactions being performed.
The softwaree for the platform controlled the specific synthetic operations and user-selected reagents as well as a suite of analytical instrumentation (mass spectrometry, high-performance liquid chromatography, and vibrational spectroscopy).
As well as hardware control, the software was also responsible for the automated optimisations.
The entire system was controlled via a graphical user interface (GUI) which was supplied via MATLAB and LabVIEW interfaces.
To demonstrate the versatility of this system, Bédard et. al. implemented a series of automated optimisations of the following reaction classes: reductive amination, C-C and C-N cross-coupling, olefination, photoredox catalysis, and nucleophilic aromatic substitution.
Once a reaction was optimised, the parameters and setup were made available as a downloadable file which could be used in any implementation of this system \cite{mit-flowchemistry}.
An overview of this sytem can be seen in Figure \ref{fig:mit-flowsystem}.

This reconfigurable flow system is a versatile platform that is pushing the boundaries of automation in a research setting.
With a modular hardware and software design, a user can configure the system to perform a variety of chemical sysntheses in a flow setting.
Using LabVIEW to control the hardware, this opens up the analytical instrumentation to a wide variety of devices as LabVIEW has ample support for many different types of device.
Having a graphical interface to control the system and optimisation parameters also simplifies the operation of the platform as no code or scripts have to be written to perform an experiment.
Although limited to flow chemistry, this platform demonstrates the versatility of automation in a chemistry lab and opens up the possibility of expanding this style of automation to other forms of chemical synthesis.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{mit.png}
    \caption{
        Plug-and-play, continuous-flow system developed by Bédard et. al.
        \textbf{(A)}: General four-step protocol for using the system.
        \textbf{(B)}: Representitive configuration of the components in the system.
        \textbf{(C)}: Representation of LED reactor.
        \textbf{(D)}: Schematic representation of the configuration shown in \textbf{(B)} and available modules.
        Figure adapted from Bédard et. al \cite{mit-flowchemistry}.
    }
    \label{fig:mit-flowsystem}
\end{figure}