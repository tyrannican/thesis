So far, a series of automation systems have been detailed that have varied applications in the field of chemistry.
From iterative synthesis platforms \cite{s5, d4} to \acrlong{hts} solutions \cite{chemspeed, d16}, a variety of different methods and strategies have been employed; each with their own distinct advantages and disadvantages.

The iterative synthesis platform described by Burke \cite{s5} has come a long way since the days of Merrifield and their peptide synthesiser in the 1960s \cite{d4}.
Being able to synthesise a wide variety of small molecular targets using multi-step iteration has allowed Burke et. al. to synthesise vast libraries of small organic targets.
However, it is significantly held back due to it only operating using one synthetic approach \cite{s5} which limits the availability of potential targets.
Even commercially available peptide synthesisers, which use the iterative approach as well, are limited to a few synthetic operations.
These systems are effective at what they were built to achieve but further expansion would benefit them more.

In the realm of \acrshort{hts}, the Eli-Lily platform is perhaps the most versatile and well-equipped automated solution discussed in this chapter.
It demonstrates a wide variety of automated techniques and synthetic protocols, all of which can be accessed remotely around the world by researchers \cite{d16}.
However, the system itself is an expensive, proprietary technology which is only available on request and would be difficult to replicate.
The cost of operation may dissuade potential customers from requesting access as well as companies not wishing to outsource their potentially sensitive research to a third-party.
Even the ChemSpeed platforms \cite{chemspeed}, which are much smaller scale \acrshort{hts} systems, are still expensive and generally out of the reach of most researchers.
The \acrshort{hts} systems are extremely effective at the synthesis of small molecular targets for generation of large synthetic libraries but there is an observable trend in the volume of output compared to price.
\acrshort{hts} systems, whilst useful, are generally out of the scope for academic research and are more confined to large-scale industry.

Other systems that have been discussed include the "mobile chemist" as described by Cooper et. al. in 2020.
This platform has impressive solid and liquid handling capabilities as well as autonomous movement around a dedicated space using a robotic arm \cite{cooper}.
However this system lacks the versatility required to perform other synthetic operations than what it was originally constructed to do (i.e, catalyst screening).
The ability for the mobile chemist to perform other operations exists though as these are just a matter of programming new routines into the robotic arm execution software.
It can be envisioned in the future that this system could be expanded further to include more synthetic operations, leading it to become a more general autonomous platform for use in a chemistry lab.
This future is unclear however as it is unknown what the future ambitions for this system are.

The ChemKonzert system is a well-rounded, versatile synthesis platform that is ideal for use in a chemistry fumehood.
The system has the ability to perform batch synthesis operations such as stirring, liquid-handling, and separations of product mixtures \cite{chemkonzert}.
The platform has its own bespoke software for operating the system which allows users to describe their synthetic experiments in a set of instructions that the platform would execute.
However, the system is limited in what capabilities it can perform due to a limited instruction set.
This effectively limited what syntheses can be performed on the ChemKonzert.
This may have changed since initial publication but, as it stands, this is still the case.
Regardless, the ChemKonzert is a platform that is best suited to an academic research lab due to its ideal size for use in a fumehood and coverage of basic synthetic operations.
This system could be used by research groups looking to enter into the automation regime, but the costs of this system are unknown.

The Droplet Synthesiser platform described by Henson et. al. is a good example of the "DIY"-style of automation in a chemistry lab.
The platform itself was constructed out of a low-cost metallic frame with moving parts consisting of parts obtained from a DIY 3D-Printer kit.
These parts could be adapted to control the movement in the \emph{X-Y} plane using a modified version of the 3D-Printer firmware \cite{reprap}.
The plastic extruder replaced with a liquid-handling module and many of the plastic mounting units were constructed by a 3D-Printer \cite{dropbot}.
This system is a good example of being able to create automated systems for the research lab without having to incur a significant financial cost to do so.
However, with the cheaper costs using DIY components, there comes an increase in the technical skills required to implement them.
For this system, this required the modification to the 3D-Printer firmware code to interpret the instructions required to operate the platform and the liquid-handling pumps.
Not all researchers have the software skills necessary to achieve this goal and many would consider it not worth the time investment.
The syringe pumps created by Saggiomo et. al. fall under the same banner.
These pumps, similar to the droplet synthesiser, are constructed of 3D-Printer DIY kits controlled via modified firmware for the 3D-Printer \cite{reprap,syringe-pump}.
These pumps require a similar skill to construct and operate compared to the droplet synthesiser; requiring operational instructions to be supplied to the software using the 3D-Printer "g-code" to interpret them.
For the technology enthusiast or hobbyist creator in the lab, these solutions may seem ideal however this cannot be said for the vast majority of chemists who simply want systems that just "work".

Overall, the automated solutions described thus far cover many different branches and flavours of chemistry from commercially available systems \cite{chemspeed} to "DIY"-style platforms \cite{syringe-pump, dropbot}.
It is clear that the technology exists to introduce automation into the every-day workflows of the chemistry researcher.
What is not clear is how to reduce the financial and technical barriers of entry into this field but not reduce the quality of work produced.
The discussion in this chapter will help address how these problems can be solved through the creation of a new software paradigm for automated chemical systems.