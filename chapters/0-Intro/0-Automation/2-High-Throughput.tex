\acrlong{hts} (\textbf{\acrshort{hts}}) is a technique in which many different reagents are combined together with one another using numerous different synthetic methods.
This can be thought of as a \emph{brute force} method in which multiple targets are synthesised to determine the best combinations of reagents to create molecules with specific properties
This strategy is generally used in the formation of multiple small molecule synthetic targets and is used widely in the field of drug-discovery \cite{d3}.
It allows the screening of large libraries of biologically active molecules that can benefit the pharmaceutical industry.

One of the first \acrshort{hts} systems was reported by Pfizer in 1986 to develop screening techniques for thousands of fermentation broths per week \cite{d12}.
Over the next few decades, numerous examples of \acrshort{hts} have been created throughout the industry from screening of DNA binding agents \cite{d13} to development of entire compound libraries \cite{d14,d15}.
With such versatility offered by the \acrshort{hts} technique, it makes it a perfect target for automation.

\subsubsection{Eli-Lily Cloud Robotics System}
One of the most well-known automated \acrshort{hts} systems is the "remote-controlled adaptive medchem lab" in use at the company Eli Lily \cite{d16} (Figure \ref{fig:intro-eli}).
Using a wide variety of autonomous instruments, the lab is capable of performing hundreds of reactions per day for small-scale reaction targets (100 mg).
This system offers a wide array of capabilities from liquid-handling operations, hydrogenation, evaporation, heating, and microwaving etc.
To achieve this, the use of robotic arms across several automated workbenches to perform chemical synthesis, characterisation, biological testing, and purifications with results reported back to the user.
The most appealing application of this technology though is that it is available to use on a global scale.
Researchers can submit their requests from anywhere in the world and have the results reported back to them when ready, removing the need for travel or constructing a similar system in their own institution.
The system is interacted with via a graphical editor in which users can design their experiments and submit them to the lab's orchestration software.
Manual actions were interjected into the workflow as well so lab technicians could support the operations of the automated system.
An efficient system, the platform performed 16,349 reactions in 2011 alone \cite{d16}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{eli.png}
    \caption{
        Layout of the Eli-Lily automated lab with it's characteristic "E" shape.
        Synthetic workflows were automated using the three workbenches with the analytical suite on the right performing characterisations.
        Figure adapted from Godfery et. al. \cite{d16}.
    }
    \label{fig:intro-eli}
\end{figure}

The system built at Eli-Lily demonstrates a highly adaptable and modular system capable of thousands of reactions per year.
Designed to perform general synthetic operations, and to operate for a global audience, the system is one which is of great benefit to the synthetic chemistry community.
However, at thousands of reactions per year, it is significantly slower than other \acrshort{hts} platforms available in the industry.
The cost of building and operating such a platform is unknown but is thought to be considerably high.
For enthusiastic researchers, this puts this type of system far beyond the reach of most.
The design of the system is one that would also be difficult to replicate.
The characteristic "E" shaped design of the lab would mean dedicated space is required to replicate even a fraction of the capability of the platform; space which is not always available to researchers.
Despite the high costs and space considerations, this system is a perfect example of lab-scale automation with wide-reaching success. 

\subsubsection{ChemSpeed Systems}
Other automated \acrshort{hts} systems that are currently in operation are the ones on offer from ChemSpeed.
These systems are in operation across a number of different fields of research from pharmaceuticals, nanomaterials, catalysts, to forensics \cite{chemspeed}.
One of these solutions for the pharmaceutical industry is their \emph{ISynth} system for automated chemical library synthesis (Figure \ref{fig:intro-chemspeed}).
This system allows for reagent preparation of both liquids and solids, multistep synthesis, automated workup and purification, and built-in analytics.
With 4 needle heads for volumetric dispensing and sampling, this allows for syntheses to occur in parallel.
One of the most interesting features of the \emph{ISynth} is their approach to automated solid handling, a particularly difficult task to automate accurately.
The scale of the solid handling is impressive, ranging from micrograms to grams \cite{chemspeed}.
Modular additions are also available which can increase the scope and versatility of the base system, allowing for more complex chemistries to be performed.
This is just one example of the many automated solutions that ChemSpeed have to offer and their solutions have made great headway in automating even the most difficult of synthetic operations.

With such versatile options available for the ChemSpeed systems, it can be assumed that the financial cost to obtain one of these platforms is considerably high.
Due to the lack of publications and details for these systems, it is difficult to gauge an accurate estimate however it is thought to be in the region of hundreds of thousands with modular additions available at a premium.
It is no surprise then that there is a lack of published works using these systems as these are out of the cost range of most research institutions.
These systems are generally used by industry as opposed to academic use.
However, it cannot be overstated that the overall versatility of the ChemSpeed platforms and their synthetic operations have advanced the field of automated synthesis.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{isynth.png}
    \caption{
        ISynth automated system available from ChemSpeed.
        Figure reproduced with permission from ChemSpeed \cite{chemspeed}.
    }
    \label{fig:intro-chemspeed}
\end{figure}

There are many more examples of \acrshort{hts} systems across other disciplines such as materials science and process development \cite{s43, s44}.
The arrival of automated solutions have advanced the fields of chemical research and industrial process development with many automated synthetic regimes being employed by the pharmaceutical industry \cite{d3}.
However, these autonomous \acrshort{hts} solutions are not without their drawbacks.
The amount of product obtained on average is generally on the small scale (milligram range) and often require specific protocols to synthesise them.
The ChemSpeed platforms help in this regard as they are aiming for a more generalised approach to automated synthesis but in the broad scope, most solutions tend to be focused on specific solutions rather than a generalised approach.
They are useful in the early stages of development such as drug discovery programs where certain reagents may be expensive as the scale of synthesis is small.
They also reduce waste production for this same reason.
However, these processes can impede the future stages when scale is the primary focus when materials should be isolated and analysed \cite{s52}.