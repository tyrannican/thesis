One of the key considerations for developing a new automated system is how the end-user will interact with it and how a developer may modify or maintain it.
From a development standpoint, the choice of programming language is crucial as this can affect the development cycles and future progress of the project.
A number of key-points \cite{prog-lang} must be considered when determining the programming language for this new automated system:
\begin{itemize}
    \item \textbf{Portability}: The compatibility of the language on different operating systems.
    \item \textbf{Efficiency}: Speed of program execution.
    \item \textbf{Learning Rate}: Difficulty of developers and users learning the language.
    \item \textbf{Error-Risk}: How easy it is for bugs to be introduced.
\end{itemize}

\subsubsection{Portability}
Portability of software can be one of the most overlooked points when developing software for a new system.
The ability for the system's software to run on any operating system (Windows, OSX, Linux-based) allows for maximum inclusion for both the developer and end-user.
With a system that is developed as cross-platform as possible, it eliminates the exclusion of a sizeable proportion of the user and developer base.

\subsubsection{Efficiency}
The speed at which programs execute is considered one of the most important factors when developing new software.
However, in the realm of automated platforms for synthesis, this is a point that can largely be overlooked.
As these systems will be interacting with hardware and moving parts, which operate at orders of magnitudes slower than CPU instruction execution, speed is not a top priority.

\subsubsection{Learning Rate}
The ability for developers, and even end-users, to learn the language which the system is written in is a key point.
To add new features or to write execution scripts for the system should be as expressive and easy as possible with little to no complexities.
There are several programming languages that exist that can seem overwhelming to newcomers.
The learning curve for any language used to implement these systems should be as simple as possible.

\subsubsection{Error-Risk}
As with the learning rate of a programming language, the error proneness of the language should be taken into consideration.
The more complex and less-intuitive languages can always lead to an increase in the number of bugs introduced into the system.
This can be true of any language, but the type of language used and the expressiveness of it can lead to shorter debugging times and over time, less errors.

These are the considerations that developers must consider when deciding on the implementation language of the new automation systems.
The choice of language can mean the difference between the success and failure of a project.
By selecting an easy-to-use language with a low learning curve, the developers can ensure that the system can be maintained and modified going forward into the future.

From an end-user perspective, the interface used to interact with the system is the most crucial part.
Most end-users would expect the minimum of a well-developed user-interface in order to interact with their system.
For commercially available systems, the user-interface is crucial as this is how researchers interact with the proprietary technology.
The Autosuite software for ChemSpeed \cite{chemspeed} and the Eli-Lily ASL software \cite{d16} are examples of interfaces designed for the end-user.
However, as this new paradigm is designed to be as general as possible, creating a dedicated user interface may be a difficult task to encompass all that is required.
An intermediate solution would be required until a suitable interface is developed.

\subsubsection{Existing Interface Example: LabVIEW}
LabVIEW is a graphical user-interface which allows for hardware to be controlled through its graphical programming language.
It has been used widely by the scientific community for a variety of different tasks and automation tools \cite{lv-ref-1}.
It allows both developer and end-users full control over their systems through the use of Virtual Instruments (\textbf{VI}s) (Figure \ref{fig:intro-labview}).
These VIs provide an interface to the physical hardware operating the system.
By chaining these VIs together using LabVIEW's graphical interface, a full experimental workflow can be developed in an automated fashion with dynamic instrument feedback incorporated into the workflow \cite{labview-manual}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.25]{labview-vi.png}
    \caption{
        Demonstration of a LabVIEW VI declared in the graphical interface.
        Figure adapted from the LabVIEW user manual \cite{labview-manual}
    }
    \label{fig:intro-labview}
\end{figure}

In terms of \textbf{Portability}, LabVIEW is available across a multitude of operating systems.
The VI structure means that it is language-independent and the user is only required to understand the structure of VIs.
This makes LabVIEW an ideal environment across multiple operating systems.

\textbf{Efficiency}, as mentioned previously, is not much of a consideration due to the timescales at which these automated systems will operate.
A point could be made in terms of the efficiency from a developer standpoint when creating the VIs.
Graphical languages are a contested topic with some declaring them easier to use than traditional code files whereas others contest that they can be confusing to follow.
In terms of this type of efficiency, this is at the discretion of the developers and end-users.

The \textbf{Learning Rate} of the LabVIEW ecosystem can be daunting at first glance.
The maintainers of LabVIEW have detailed operating manuals \cite{labview-manual} available for free for general use.
LabVIEW can be considered much easier to grasp than traditional text-based programming languages.

In terms of \textbf{Error Risk}, LabVIEW can be considered particularly vulnerable in this regard.
As it is a graphical language, VIs are prone to becoming tangled and confusing which can make it difficult for developers and end-users alike to understand.
The LabVIEW manual has sections dedicated to this \cite{labview-manual} which demonstrates that this is commonplace.
As this is intended to be used within automated synthesis and is coupled strongly to the underlying hardware, this can lead to confusing and even dangerous scenarios when the user cannot understand the VI powering their experiment.

However, LabVIEW has the advantage of being an industry leader in the automation of hardware with a wide selection of commercial drivers available for a wide variety of lab hardware.
With such a vast community and expert developers on hand, this had greatly reduced the errors that may be encountered in LabVIEW VI controllers.

\subsubsection{LabVIEW: OpenFlow System}
An example of how LabVIEW has been used in automated systems is the OpenFlowChem system developed by Cherkasov et. al.
OpenFlowChem is an open-source flow automation framework designed to create automated systems with flexible control and quick deployments.
Their choice of using LabVIEW is three-fold: a financially viable ecosystem, quick communication with laboratory equipment, and efficient expansion of the underlying systems with new hardware.
To demonstrate this framework, Cherkasov et. al. studied the reversibility of catalyst poisoning by constructing a fraction collection sampling system.
By constructing hardware modules controlled via LabVIEW (\ce{H2} sensors, Temperature controllers, HPLC pumps, and pressure regulators), they created an automated system to study the effect of this reversibilty by measuring \ce{H2} evolution over time \cite{openflow}.
This system is highlighted in Figure \ref{fig:openflow}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{openflow.png}
    \caption{
        Overview of the OpenFlowChem system to study catalyst poisoning reversibility.
        \textbf{(A)}: Scheme of the reactor used in the automated stepwise study of catalyst poisoning reversibility.
        \textbf{(B)}: Scheme of the OpenFlowChem automation system with the "jigsaw" joints showing digital communication.
        Figure adapted from Cherkasov et. al. \cite{openflow}.
    }
    \label{fig:openflow}
\end{figure}

\subsubsection{Existing Interfaces: Other}
Other systems, as described previously, have their own bespoke software; some proprietary such as ChemSpeed \cite{chemspeed} and some undisclosed \cite{chemkonzert}.
As these systems are not available for critique, it is difficult to gauge and will not be discussed.