In order to shift the paradigm of automated synthesis, the points mentioned in this section must be followed to a certain degree.
It is impossible to perhaps meet all criteria, but steps can be made to ensure that any developed system makes best efforts to conform to these standards.

New system designs must offer clear benefits over commercially available alternatives in terms of price and useability.
Researchers should not feel pressured into spending vast sums of funding in order to enter the field of automated research.
Any proposed system should be cost effective in this regard, offering capabilities that match commercial alternatives to the best of their ability at a fraction of the cost.
As such, the proposed system must consider the possibility of financial scope creep, where the cost of the implementation starts to increase dramatically.
This can be avoided by sourcing cheaper alternatives to already existing hardware.

Any new design should be reliable and robust for the end-users.
Operational errors encountered by such a system should be handled gracefully and not jepordise experimental data.
Whilst eliminating all errors is an impossible task, best efforts should be made to attempt to eliminate these as far as possible.
The system should be reliable enough that a researcher is comfortable trusting such a system to execute their experiments without fear of software failure compromising their experiemnts.

Open-source hardware and software should be used and developed where appropriate for the system.
This not only opens up the possibility of collaboration from the larger scientific community but by-passes the issues surrounding the use of proprietary software and hardware.
NASA is pioneering an initiative to build an inclusive open science community over the next decade called the Open-Source Science Initiative (OSSI) \cite{ossi}.
The aim is to make science and by extension, the tools sued to facilitate the science (i.e. software) open for all.
Using close-sourced solutions may aid the research of those involved but creates barriers for those who do not have access to such tools.
By creating an open-source framework of software and hardware, it opens the possibility of attaining a wider scope when attempting to drive adoption of such a system and allows for contributions from the scientific community.

When designing a new system, modularity of the software and hardware should be considered so as to prevent a monolithic structure.
Using this idea of modularity, small modules of software can be developed as standalone units that can be inserted into the software stack as and when required.
This prevents issues that are encountered in monolithic systems where small changes to sections can break the entire system architecture.
An example of a modular architecture is the microservices design pattern which is used in many large companies such as Netflix \cite{netflix-microservices}.
These are small programs or services which are focussed on a single task that all work together as part of a larger technology stack.
This separation of concerns from a software perspective are imperative as it allows the software to become more maintainable as all integrated parts are bespoke units that can be worked on alone.
This also applies to the hardware as well as the system should be able to accept new hardware devices as they are required.
It is impossible to know from the outset what items of hardware are required as project requirements may change over time.
Having a modular structure allows for these items to be added or removed at any time without affecting the overall system.

When it comes to implementing the system software architecture, the choice of programming language is imperative.
The choice of language dictates how the developers will maintain and deliver the system as well as how end-users will interact with the end-product.
Any implementation should allow for cross-platform use in that the user can operate and the developer maintain the system on any underlying operating system.
This will help drive adoption for the wider masses.
The language choice must be efficient in that execution speed will not hinder the operational aspects of the automated system.
As these interactions usually occur on timescales orders of magnitude slower than CPU instruction execution, this is less of an issue.
Any implementation must be performed in a language that has a low learning curve so as not to dissuade potential new developers or users.
This implies that the implementation must be performed in a language that is easy to read and understand the execution workflow.
The language must also be tolerant to errors as well as in that it is fairly difficult to introduce syntactical errors.
Any errors enountered by the language should be handled gracefully so that they do little to interfere with the operational aspects of the code \cite{python-fault-tolerance}.
Logic errors can never be avoided but a language that is not as error-prone is an ideal candidate.

For the end-user, how they interact with the system can influence how well the system is received.
A complicated interface can drive potential users away from the system so the implementation of user interaction is a key point to address.
Ideally, the user should interact with the system via an intuitive user interface in which their experiments and tasks can easily be described and executed.
User interface design is an entire branch of computing science in itself so can be a difficult task to achieve correctly.
Lacking a user interface, the developers should at minimum enforce a simple and intuitive execution program that is easy to read and interpret by the user with well-documented functions and routines.
Having an execution program that takes simple inputs and executes the tasks supplied by the user can be an ideal replacement for a graphical interface in the cases where one cannot be developed.

In summary, the points discussed in this section can act as a baseline standard for which any automated system or architecture should adhere to.
Following these points can help bring automation in the chemistry research lab to the masses with cost-effective implementations with open-source contributions to allow for collaboration from the wider scientific community.