The \acrlong{acm} is the central control unit of the \acrlong{act} library as it is the point of contact for all \acrlong{cd}s.
The \acrlong{cm} acts as a gateway which commands are sent to and relayed to their appropriate device for processing (Figure \ref{fig:act-manager-devices}).
The manager maintains its own array of Registered Devices which is populated upon registration of a device (Figure \ref{fig:act-cm-devices}) and iterated through for initialisation of the devices and update method calls.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
    struct RegisteredDevice {
        // Name of the device
        char cmd[COMMANDHANDLER_MAX_LENGTH + 1];

        // Pointer to the device object
        void *ptrToObject;

        // Function pointer to the initialsiation method of the device
        void (*init)(void*);

        // Function pointer to the handleCommand method of the device
        void (*handleCommand)(const char*, void*);

        // Function pointer to the setHeader method of the device
        void (*setHeader)(const char*, void*);

        // Function pointer to the update method of the device        
        void (*update)(void*);
    };

    /* Method showing how a device is registered */
    void CommandManager::addDevice(
        const char *cmd,
        void* ptrToObject,
        void (*init)(void*),
        void (*handleCommand)(const char *, void*),
        void (*setHeader)(const char *, void*),
        void (*update)(void*)
    )
    {
        // Make space for a new device in the array
        devices = (RegisteredDevice*)realloc(deviceList, (deviceCount + 1) * sizeof(RegisteredDevice));

        /* Assign all fields for the new device */
        strncpy(deviceList[deviceCount].cmd, cmd, COMMANDHANDLER_MAX_LENGTH);
        devices[deviceCount].ptrToObject = ptrToObject;
        devices[deviceCount].init = init;
        devices[deviceCount].handleCommand = handleCommand;
        devices[deviceCount].setHeader = setHeader;
        devices[deviceCount].update = update;

        // Increment the count
        deviceCount++;
    }
    \end{minted}
    \caption{
        Struct showing what the Command Manager holds for each device.
        The initialisation method, update method, and handleCommand method are assigned here and called upon iteration.
    }
    \label{fig:act-cm-devices}
\end{figure}

\noindent
All \acrlong{cd}s are registered with the manager though the device's registration method before the manager itself has been initialised.
On manager initialisation, the registered device array is iterated over with each device being registered with the manager's \acrlong{ch} as a relay so received commands can be properly filtered to their appropriate device.
Once registered, each device is also initialised by calling its initialisation method (Figure \ref{fig:act-cm-initialisation}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Initialisation method for the CommandManager */
        void CommandManager::init() {
            // Register relays for each device
            registerDevices();

            // Initialise each device
            initDevices();
        }

        /* Registers all devices in the device list */
        void CommandManager::registerDevices() {
            // Iterate through all registered devices
            for(int i = 0; i < deviceCount; ++i) {
                // Add a relay to Manager Command Handler for each device.
                cmdHdl.addRelay(devices[i].command, devices[i].handleCommand, devices[i].ptrToObject);

                // Set the command header for the device.
                devices[i].setHeader(devices[i].command, devices[i].ptrToObject);
            }
        }

        /* Initialises devices in the devices list */
        void CommandManager::initDevices() {
            // Iterate through all devices
            for (int i = 0; i < deviceCount; ++i) {
                // Call each device's initialisation method
                (*devices[i].init)(devices[i].ptrToObject);
            }
        }
    \end{minted}
    \caption{
        Initialisation procedure for the Command Manager.
        Each device in the devices array has a relay added to the manager's Command Handler and the device's initialisation method is invoked.
    }
    \label{fig:act-cm-initialisation}
\end{figure}

\noindent
With all devices registered and initialised, the \acrlong{cm}'s \acrlong{ch} then waits for incoming commands and relays them to the appropriate device's handler.
Once a command has been parsed, each device's update method is then invoked to register any new changes with said devices (Figure \ref{fig:act-cm-update}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Main update method of the Manager */
        void CommandManager::update() {
            // Process incomming commands and relay them to appropriate device
            cmdHdl.processSerial();

            // Call each device's update method
            updateDevices();
        }
        
        /* Updates all devices */
        void CommandManager::updateDevices() {
            // Iterate through all devices
            for (int i = 0; i < deviceCount; ++i) {
                // Invoke each device's update method
                (*devices[i].update)(devices[i].ptrToObject);
            }
        }
    \end{minted}
    \caption{
        Update process for the \acrlong{cm}.
        The manager's \acrlong{ch} processes each incoming command and relays it to the appropriate device then invokes each device's update method.
    }
    \label{fig:act-cm-update}
\end{figure}
\noindent
Whilst the \acrlong{cm} is a short section of the library, it is important due to its ability to relay each command it receives to the appropriate device if registered.
The Manager sits at the front of not just the \acrlong{act} library but the \acrlong{ach} library as well.
Through this manager and its implementation, the process of accessing and interacting with \acrlong{cd}s is simplified greatly with the user needing only to include a few lines of code into the Arduino sketch (Section \ref{sec:act-bringing-it}).