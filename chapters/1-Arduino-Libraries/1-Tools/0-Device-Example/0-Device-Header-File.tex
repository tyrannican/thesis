The first step for adding a new device is the creation of the header file for the device class (\mintinline{text}{CommandSHT1X.h}).
This is where the method declarations are stated and where the user can declare constants and boiler plate code for the device.
Here the device is importing the \acrlong{ach} and \acrlong{acm} libraries so commands can be parsed, and the device registered at the higher level with the manager (Figure \ref{fig:act-header-includes}).
The third-party library for this device is also included here as this is where the implementations of the methods to obtain the desired values are located.
The \acrlong{cd} implementation of the device is to make it compatible with the \acrlong{ch} and \acrlong{cm}.
\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        // Header guard to prevent multiple inclusions
        #ifndef CommandSHT1X_H
        #define CommandSHT1X_H

        // Include the Command Handler for parsing commands
        // Include the Command Manager to register the device
        #include <CommandHandler.h>
        #include <CommandManager.h>

        // Include the third party library for this device
        // This is where most implementations are for the device.
        #include <SHT1X.h>

        /* Constant and Class declarations go here */

        #endif
    \end{minted}
    \caption{
        Including the necessary header files into the new device.
        The \acrlong{ch} and \acrlong{cm} are included here to allow for communication parsing and registering of the device with the \acrlong{cm}.
        Third party libraries are also included here if required.
    }
    \label{fig:act-header-includes}
\end{figure}

The next step after inclusion of the necessary header files is the declaration of the commands used by the device.
The command names and values can be at user's discretion, but it is advised to keep them short and as meaningful as possible.
It is these commands that the \acrlong{ch} will parse and build response messages from.
For this device, the user has declared three commands for requesting the values and three commands for reporting these values back to the user (Figure \ref{fig:act-header-commands}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Basic Commands - Boiler Plate */
        // Command is not recognised
        #define UNRECOGNISED_CMD "?"

        // Command to determine if device is present
        #define PING_CMD "PING"

        // ID for the device's ping command
        #define COMMANDSHT1X_ID "SHT1X"

        /* Incoming Commands from user - Requests for values */
        #define COMMANDSHT1X_REQUEST_FAHRENHEIT "RF"
        #define COMMANDSHT1X_REQUEST_CELCIUS "RC"
        #define COMMANDSHT1X_REQUEST_HUMIDITY "RH"

        /* Outgoing response messages - Reporting values to the user */
        #define COMMANDSHT1X_REPORT_FAHRENHEIT "F"
        #define COMMANDSHT1X_REPORT_CELCIUS "C"
        #define COMMANDSHT1X_REPORT_HUMIDITY "H"
    \end{minted}
    \caption{
        Commands are declared here for the new device.
        Here, commands for reading the temperature and humidity are declared as are their response commands.
    }
    \label{fig:act-header-commands}
\end{figure}

After the commands are declared, the declaration of the device class and desired methods comes next.
The device class will declare all the methods that are listed in the \acrlong{cd} class (Figure \ref{fig:act-device-template}) plus any necessary methods for the device itself:

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        class CommandSHT1X {
        public:
            // SHT1x object from the `SHT1x` third-party library
            SHT1x sht1x;

            // CommandHandler for the device
            CommandHandler cmdHdl;

            // Constructor for the device
            // Accepts two integers for SHT1X data pin and clock pin
            CommandSHT1X(int dataPin, int clockPin);

            // Method for registering the device with the Command Manager
            void registerToCommandManager(CommandManager& cmdMgr, const char *command);

            /* Command Device template methods omitted for clarity */
        private:
            /* Command Device template methods omitted for clarity */

            // Wrapper and counterpart method for obtaining fahrenheit value from the sensor
            static void wrapper_fahrenheit();
            void fahrenheit();

            // Wrapper and counterpart method for obtaining celsius value from the sensor
            static void wrapper_celsius();
            void celsius();

            // Wrapper and counterpart method for obtaining humidity value from the sensor
            static void wrapper_humidity();
            void humidity();
        };
    \end{minted}
    \caption{
        Class definition of the new device.
        This is the template that will have its implementation in the dedicated C++ file.
        Here, all methods are declared which details the logic the device will perform.
    }
    \label{fig:act-header-class-definition}
\end{figure}

This concludes the header file implementation section which declares all methods to be implemented for the device.
The next stage of adding a device to the library is to add definitions for each declared method stub in the header.
This is done in a separate \mintinline{text}{.cpp} file which shares the same name as the header (\mintinline{text}{CommandSHT1X.cpp}).