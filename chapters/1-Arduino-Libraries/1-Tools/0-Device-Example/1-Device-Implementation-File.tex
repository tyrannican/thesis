This file is where the definitions for all declared methods in the device's header file.
The first method implementation is that of the device's constructor function.
This will create the device and assign the \mintinline{cpp}{SHT1x} variable to an instance of the third-party device object (Figure \ref{fig:act-cpp-ctor}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        // Include the header file declarations for the device
        #include "CommandSHT1X.h"

        // Global pointer to be used in the wrapper methods for casting
        void* ptrToDeviceObject

        /* Device constructor function for creating an instance of CommandSHT1X */
        CommandSHT1X::CommandSHT1X(int dataPin, int clockPin) {
            // Create an instance of the SHT1X device and assign it to the class's instance variable
            SHT1x tmpDev(dataPin, clockPin);
            sht1x = tmpDev;
        }
    \end{minted}
    \caption{
        Constructor for the device.
        The pins where the device is connected to on the Arduino are declared here.
        An instance of the third-party device is created here and is used to perform the logic for the device.
    }
    \label{fig:act-cpp-ctor}
\end{figure}

The next method implementation is the registration of the device with the \acrlong{cm}.
A reference to the \acrlong{cm} is passed from the main Arduino sketch itself and is used to register the device along with the wrapper functions detailed in the boiler plate code section.
This will allow the manager to keep track of this device and relay the appropriate commands to it once they have been received (Figure \ref{fig:act-cpp-registration}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        void CommandSHT1X::registerToCommandManager(CommandManager& cmdMgr, const char *deviceID) {
            /**
            * Here we add the instance of this device to the CommandManager,
            * A device ID is given to identify the device.
            * The instance of the object (`this`) is passed so that it can be called
            * from the Manager.
            * The wrappers for initialisation, headers, handling commands
            * and the update function are also passed here to they can be called.
            * Any commands received by the manager can be handled by the
            * handleCommand wrapper and be relayed down to the device.
            */
            cmdMgr.addDevice(deviceID, this, wrapper_init, wrapper_handleCommand, wrapper_setHeader, wrapper_update);
        }
    \end{minted}
    \caption{
        Method for registering the new device with the \acrlong{cm}.
        A reference to the object and wrapper methods, along with an ID for the device are registered so that any incoming commands can be relayed to the device when received.
    }
    \label{fig:act-cpp-registration}
\end{figure}

The initialisation method is the next implementation and is one of the most important methods following registering the device with the \acrlong{cm}.
This is where all commands for the device that will be processed and parsed by the \acrlong{ch} are added.
Each command declaration in the header file is added here along with the wrapper to each method so they can act as a callback in the \acrlong{ch} (Figure \ref{fig:act-cpp-init}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Wrapper method for the initialisation method */
        void CommandSHT1X::wrapper_init(void *ptrToObj) {
            // Cast the void pointer to a pointer of CommandSHT1X and invoke
            CommandSHT1X* self = (CommandSHT1X*) ptrToObj;
            self->init();
        }

        /* Initialisation method */
        void CommandSHT1X::init() {
            // All commands are registered here with their wrapper functions
            // with the Command Handler
            cmdHdl.addCommand(PING_CMD, wrapper_ping);
            cmdHdl.addCommand(COMMANDSHT1X_REQUEST_FAHRENHEIT, wrapper_fahrenheit);
            cmdHdl.addCommand(COMMANDSHT1X_REQUEST_CELCIUS, wrapper_celsius);
            cmdHdl.addCommand(COMMANDSHT1X_REQUEST_HUMIDITY, wrapper_humidity);

            // Default handler is added her ein the case of unrecognised commands
            cmdHdl.addDefaultHandler(wrapper_unrecognised);
        }
    \end{minted}
    \caption{
        Initialisation method for the device.
        All commands declared in the header file are registered with the \acrlong{ch} along with their wrapper method implementations.
    }
    \label{fig:act-cpp-init}
\end{figure}

To allow the \acrlong{ch} to parse received commands, the device must be able to pass any received commands to the handler.
The \acrlong{cm} receives these commands and via the wrapper function that was registered with the device, the command is passed down to the \mintinline{cpp}{handleCommand} method of the device which invokes the \mintinline{cpp}{processString} method of the handler (Figure \ref{fig:act-cpp-handle-cmd}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Wrapper method for the handleCommand method */
        void CommandSHT1X::wrapper_handleCommand(const char *cmd, void *ptrToObj) {
            // Set the global pointer to the received pointer and cast to instance
            ptrToDeviceObject = ptrToObj;
            CommandSHT1X* self = (CommandSHT1X*) ptrToDeviceObject;

            // Invoke the handleCommand method with supplied cmd argument
            self->handleCommand(cmd);
        }

        /* handleCommand method implementation */
        void CommandSHT1X::handleCommand(const char *cmd) {
            // Parse the command string in the handler
            cmdHdl.processString(cmd);
        }
    \end{minted}
    \caption{
        Method for handling commands for the device.
        Each incoming command received from the \acrlong{cm} is passed to the device's instance of its \acrlong{ch}.
    }
    \label{fig:act-cpp-handle-cmd}
\end{figure}

To enable the user to know what device has been responding, each device implements a "command header" which prefixes each command to know what device has sent which message (Figure \ref{fig:act-cpp-set-header}).
This aids in identifying device messages when the user is logging device messages in their system.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        void CommandSHT1X::wrapper_setHeader(const char *header, void *ptrToObj) {
            // Cast to instance and invoke method
            CommandSHT1X* self = (CommandSHT1X*) ptrToObj;
            self->setHeader(header);
        }

        void CommandSHT1X::setHeader(const char *header) {
            // Set the header with the handler
            cdmHdl.setCmdHeader(header);
        }
    \end{minted}
    \[\textbf{M1},C,25.2;\]
    \caption{
        Method used by the device for setting the command header that is prefixed to all commands to know which device has sent what messages back to the user.
        Example of a command with the header set as \textbf{M1} with values to return to the user.
    }
    \label{fig:act-cpp-set-header}
\end{figure}

The next implementation is that of the \mintinline{cpp}{update()} method.
This is where any updates for the devices are meant to occur such as updating variables and state.
This runs on every loop of the Arduino's main \mintinline{cpp}{loop()} function as part of the \acrlong{cm}'s \mintinline{cpp}{update()} method.
As such, only "non-blocking" calls should be made in this method (Figure \ref{fig:act-cpp-update}).
Non-blocking calls are updates which do not wait (or block) the execution of the running program.
In such a system where this is called every few clock cycles of the Arduino's CPU, any call that effectively halts this loop can have negative effects.
Most notably halting the program execution with no notable sign to the user as to the reason.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Wrapper for the update method */
        void CommandSHT1X::wrapper_update(void *ptrToObj) {
            // Cast to instance and invoke method
            CommandSHT1X* self = (CommandSHT1X*) ptrToObj;
            self->update();
        }

        /* Update method for the device */
        void CommandSHT1X::update() {
            // As this device is simply used for requesting values from the sensor
            // No updates are necessary and hence an empty implementation.
            // It is a simple read operation which is non-blocking.
        }
    \end{minted}
    \caption{
        Update method for the device.
        This is where any updates required for the device are executed.
        These updates should be non-blocking updates.
        As this device is simply reading values from a sensor, this method is left blank.
    }
    \label{fig:act-cpp-update}
\end{figure}

To know that the device is present and ready to receive commands, a ping behaviour is implemented for the device.
When the ping command is received by the \acrlong{cm}, the command is relayed to the device via the wrapper method which should reply with a "pong" command.
This method is used for determining the presence of the device during the initialisation process and device registration of the Commanduino library (Section \ref{sec:cmd-manager}).
This reply message should ideally be the name of the device for all \acrlong{cd}s, but this is at the user's discretion.
In this case, the reply message will be \mintinline{text}{"SHT1X"}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Wrapper for the ping method */
        void CommandSHT1X::wrapper_ping() {
            // Case the global pointer and invoke ping method
            CommandSHT1X* self = (CommandSHT1X*) ptrToDeviceObject;
            self->bonjour();
        }

        /* Ping method. Relays response back to the user over serial */
        void CommandSHT1X::ping() {
            // Initialise a response to send
            cmdHdl.initCmd();

            // Add the command to know what command this response is for
            cmdHdl.addCmdString(PING_CMD);

            // Add a delimiter between terms
            cmdHdl.addCmdDelim();

            // Add the ID (SHT1X) of the device (Pong response)
            cmdHdl.addCmdString(COMMANDSHT1X_ID);

            // Terminate the response
            cmdHdl.addCmdTerm();

            // Send response back to the user
            cmdHdl.sendCmdSerial();
        }
    \end{minted}
    \caption{
        Method implementation for the ping command.
        When a device a ping command is received by the device's \acrlong{ch}, a reply message is constructed and sent back to the user.
        The reply message here is the device ID (\mintinline{cpp}{SHT1X}).
    }
    \label{fig:act-cpp-ping}
\end{figure}

With the boiler plate code now implemented, the next stage is to add definitions for the methods which interact with the device.
This device has three methods to obtain Celsius, Fahrenheit, and humidity values each with their own wrapper (Figure \ref{fig:act-cpp-implementation}).
For clarity's sake, one implementation will be shown here (Celsius) with the other two just replacing the Celsius command for their respective counterparts (Fahrenheit and humidity).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Wrapper for the celsius method */
        void CommandSHT1X::wrapper_celsius() {
            CommandSHT1X* self = (CommandSHT1X*) ptrToDeviceObject;
            self->celsius();
        }


        /* Method for obtaining the measured temperature in celsius from the sensor */
        void CommandSHT1X::celsius() {
            // Obtain the celsius value from the third party sensor object
            float celsiusValue = sht1x.readTemperatureC();

            // Initialise the response
            cmdHdl.initCmd();

            // Add the response command
            cmdHdl.addCmdString(COMMANDSHT1X_REPORT_CELCIUS);

            // Add delimiter to separate terms
            cmdHdl.addCmdDelim();

            // Add the measured float value
            cmdHdl.addCmdFloat(celsiusValue);

            // Terminate the response
            cmdHdl.addCmdTerm();

            // Send the response to the user
            cmdHdl.sendCmdSerial();
        }
    \end{minted}
    \caption{
        Celsius method implementation for the device.
        Here the device calls on the internal \mintinline{cpp}{SHT1X} object to obtain the value from the sensor and the value is returned to the user in a constructed reply message.
    }
    \label{fig:act-cpp-implementation}
\end{figure}

The final method to implement is the case when a received command is not recognised by the device's \acrlong{ch} (Figure \ref{fig:act-cpp-unrecognised}).
This relays the received command back to the user with the unrecognized command string to notify them that the command was not understood (in this case, the unrecognised command is \mintinline{text}{"?")}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
        /* Wrapepr for the unrecognised command method */
        void CommandSHT1X::wrapper_unrecognised(const char *cmd) {
            // Cast global pointer and invoke method with cmd argument
            CommandSHT1X* self = (CommandSHT1X*) ptrToDeviceObject;
            self->unrecognised(cmd);
        }

        /* Method for handling cases where received commands are not recognised by the handler */
        void CommandSHT1X::unrecognised(const char *cmd) {
            // Initialise the response
            cmdHdl.initCmd();

            // Add unrecognised command string
            cmdHdl.addCmdString(UNRECOGNISED_CMD);

            // Add the delimiter to separate terms
            cmdHdl.addCmdDelim();

            // Add the unrecognised received command
            cmdHdl.addCmdString(cmd);

            // Terminate the response
            cmdHdl.addCmdTerm();

            // Send the response back to the user
            cmdHdl.sendCmdSerial();
        }
    \end{minted}
    \caption{
        Method implementation for an unrecognised command.
        Here the device will reply with a \mintinline{text}{"?"} character in the case a received command is not recognised by the device's \acrlong{ch}.
    }
    \label{fig:act-cpp-unrecognised}
\end{figure}
