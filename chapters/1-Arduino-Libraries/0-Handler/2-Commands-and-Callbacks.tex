The \acrlong{ach}'s main feature is the ability to "register" a command string with the handler and associate a function call with it.
There are two types of item that can be registered with a handler:
\begin{itemize}
    \item \textbf{General Commands} - A simple 1-1 mapping of command string that invokes a callback function
    \item \textbf{Relay Commands} - Similar to a \textbf{General Command} except that the command is mapped with a pointer to a target object as well as a pointer to a callback function.
    The command passes the contents of the buffer to the callback function along with the pointer to the target object so that the object can call the function associated with it.
\end{itemize}
For the \textbf{General Commands}, the handler stores these as an array of key-value structures (Figure \ref{fig:ach-command-callback-pair}) which contains the command string itself and a function pointer to the callback function. When the handler receives this command in the future, the array is iterated through for the appropriate callback method with the matching command string and invokes said method.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
    struct CommandHandlerCallback {
        // Store the length of the command up to the maximum size allowed.
        char command[MAX_COMMAND_LENGTH + 1];

        // Store a function pointer to the callback function to be invoked (Value)
        void (*function)();
    };
    \end{minted}
    \caption{
        Struct to represent a Command-Callback pair.
        These pairs are held together in an array by the handler which is iterated through upon command parsing.
    }
    \label{fig:ach-command-callback-pair}
\end{figure}
\noindent
\textbf{Relay Commands} are stored in a similar way to \textbf{General Commands}, an array of key-value structures (Figure \ref{fig:ach-relay-command-callback-pair}) that contains the command string, a function pointer to the callback function and a pointer to the target object to which the callback function is associated with.
Each handler can hold a relay for other objects that may be present at a higher level in the software stack (See Section \ref{sec:act}).
When a command is received by a handler, it checks these relays for the command and passes the command, arguments, and pointer to the target object to the registered callback.
This allows for callbacks on other objects to be invoked from a separate handler if the case arises.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{cpp}
    struct RelayHandlerCallback {
        // Store the length of the command up to the maximum size allowed.
        char command[MAX_COMMAND_LENGTH + 1];

        // Pointer to target caller object
        void *ptrTotarget;

        // Function pointer with arguments to take the remaining
        // command arguments and pointer for the target object
        void (*function)(const char*, void*);
    };
    \end{minted}
    \caption{
        Struct to represent a Relay-Command-Callback pair.
        These pairs are stored in an array that the handler iterates through and relays the remaining command to the appropriate callback.
    }
    \label{fig:ach-relay-command-callback-pair}
\end{figure}
\noindent
The \acrlong{ach} offers two methods to the user for command and relay registration:
\begin{itemize}
    \item \mintinline{cpp}{CommandHandler::addCommand(const char*, void (*function)())}
    \item \mintinline{cpp}{CommandHandler::addRelay(const char*, void (*function)(const char*, void*), void*)}
\end{itemize}
Each function accepts a command and callback function pointer as arguments and creates either a \mintinline{text}{CommandHandlerCallback} or \mintinline[breaklines,fontsize=\footnotesize]{text}{RelayHandlerCallback} struct, depending on which function is called.
These structs are then added to a dynamically sized array that are iterated over during the command parsing process to find the appropriate command struct and invoke the callback function associated with it.
In the case where the relay handlers are checked, the command and its arguments are passed to the callback function as well as a pointer to the target object associated with said callback. The implementation of the \mintinline[breaklines,fontsize=\footnotesize]{text}{addCommand} can be seen in Figure \ref{fig:ach-command-registration}.
The implementation of the \mintinline{text}{addRelay} method is omitted as it mirrors this function except that it accepts an argument for a pointer to the target object.
\begin{figure}[H]
    \begin{minted}[breaklines,fontsize=\scriptsize]{cpp}
    /**
     * Adds a command and a callback to the list of available commands
     * When a token is found in the internal buffer, calls the function pointer
     * to deal with it.
     */
    void CommandHandler::addCommand(const char *command, void (*function)()) {
        // Dynamically resizes the command list array for the new entry and adds an entry
        commandList = (CommandHandlerCallback*)realloc(commandList, (commandCount + 1) * sizeof(CommandHandlerCallback));

        // Copys the incoming command to the struct
        strncpy(commandList[commandCount].command, command, MAX_COMMAND_LENGTH);

        // Set the function pointer for the callback
        commandList[commandCount].function = function;

        // Increases the command count
        commandCount++;
    }
    \end{minted}
    \caption{Method for registering a command with the handler. The commands are stored in a dynamically sized array that is iterated over when a command is found in the internal input buffer and invokes the appropriate callback function when matched.}
    \label{fig:ach-command-registration}
\end{figure}
