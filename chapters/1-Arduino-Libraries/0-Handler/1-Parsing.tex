The \acrlong{ach} class supplies three methods which are used to receive and parse commands from serial input. These are as follows:
\begin{enumerate}
    \item \mintinline{cpp}{CommandHandler::processSerial(&Stream)}
    \item \mintinline{cpp}{CommandHandler::processChar(char)}
    \item \mintinline{cpp}{CommandHandler::processString(const char* str)}
\end{enumerate}
The \mintinline{cpp}{CommandHandler::processStream(&Stream)} method simply reads data from the default serial input supplied by the Arduino language or a serial input defined by the user. The stream is then read from continuously so long as there is data available to read and each byte received is then passed down to the \mintinline{cpp}{CommandHandler::processChar(char chr)} method for processing (Figure \ref{fig:ach-process-serial}).

\begin{figure}[H]
    \begin{minted}[breaklines,fontsize=\scriptsize]{cpp}
    void CommandHandler::processSerial(Stream &inStream) {
        // There is data available to be read
        while(inStream.available() > 0) {
            // Read the byte (char and byte are synonomous in Arduino)
            char inChar = inStream.read();

            // Pass on for processing
            processChar(inChar);
        }
    }
    \end{minted}
    \caption{
        Implementation of the \mintinline{cpp}{processSerial} method of the \acrlong{ach}.
        The serial stream is read from continuously with each character being passed to the \mintinline{cpp}{processChar()} of the handler for processing.
    }
    \label{fig:ach-process-serial}
\end{figure}
\noindent
When a character has been received from the serial input, the \mintinline{cpp}{CommandHandler::processChar(char chr)} method is invoked. This method adds the character to an internal buffer maintained by the handler until a terminator character (e.g. \textbf{;}) is reached.
Once the terminator has been reached, the buffer is then parsed to check for a prefixed command.
If the command is found and matches those which have been registered, then the appropriate callback function is invoked.
The pseudocode in Figure \ref{fig:ach-pseudocode} highlights this process.

The final method for parsing messages (\mintinline{cpp}{CommandHandler::processString(const char *chr)}) mirrors the functionality of the \mintinline{cpp}{CommandHandler::processSerial(&Stream)} method.
It reads the string character by character and passes the contents to the \mintinline{cpp}{CommandHandler::processChar()} method.
When a handler processes and parses a command, there are cases where that command may be relayed to another handler which will then have to process the contents of the message.
As this is an internal communication, the serial input method cannot be used so this helper method was created to handle this case.

\pagebreak
\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize,breaklines]{text}
        process character method(character) {
            match = false;
            command = Search the internal buffer for a leading command based on delimiter;

            if (command matched) {
                for (every command in the registered commands) {
                    if (command equals the registered command) {
                        invoke registered command callback();
                        match = true;
                        break out of iteration;
                    }
                }

                for (every relay in the registered relays) {
                    if (command equals the registered relay command) {
                        invoke relay function(remaining contents of the buffer, pointer to target object);
                        match = true;
                        break out of iteration;
                    }
                }

                if (no command matched) {
                    if (default handler exists) {
                        call default handler function(command);
                    } else if (pointer to default handler exists) {
                        call default handler(command, pointer to default handler);
                    }
                }

                Clear the contents of the internal buffer;

            } else {
                Add character to contents of internal buffer(character);
            }
        }
    \end{minted}
    \caption{Pseudocode highlighting the steps taken when processing a single character received from an input. An internal buffer is checked to determine if a command already exists and if it does, a search of all registered commands and relays is undertaken to call the appropriate callback function. If no command is matched, the supplied character is added to the contents of the internal buffer.}
    \label{fig:ach-pseudocode}
\end{figure}