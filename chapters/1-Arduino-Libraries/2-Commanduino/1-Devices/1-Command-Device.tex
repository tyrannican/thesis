The \acrlong{cd} child classes are devices that represent a specific device that is defined in the \acrlong{act} library and loaded onto an Arduino.
They contain all methods that are specific to that device implementation plus all inherited methods from the \acrlong{cd} parent class for initialisation and handling commands.

Each \acrlong{cd} contains an initialisation method that is used to initialise variables and conditions for the device (Figure \ref{fig:cmd-cd-init-example}) which is called upon registration with the Commanduino \acrlong{cm}.
This initialisation is not always required and is omitted for devices that do not require it; the parent \acrlong{cd} class has an implementation that is left blank for these cases.
Each device registers all commands, similar to Figure \ref{fig:cmd-cd-register-req-example}, which are then used in their methods to send the commands plus any arguments to the Arduino where they are then parsed.
All commands that are defined for this device must match those which have been defined for the \acrlong{act} counterpart of the device.
The main design point of Commanduino is that it mirrors the \acrlong{act} library with the ability to add extra functionality.
Whilst each device can have its own methods for internal use and for other purposes, the commands that are sent between Python and the Arduino must match those for the device that is loaded onto the boards.
In cases where it is not, the "unrecognised" (\mintinline{text}{?}) command is triggered.

Whilst all \acrlong{cd}s can be accessed individually, it is the Commanduino \acrlong{cm} that orchestrates the operation of them all together, much in a similar way to how the \acrlong{act} \acrlong{cm} (Section \ref{sec:act-command-manager}) operates.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}

    # Command for setting the maximum speed of the Motor
    COMMANDACCELSTEPPER_SET_MAXSPEED = "SMS"

    class CommandAccelStepper(CommandDevice):
        """Command device which represents a AcceleStepper motor for Arduino.

        This class offers methods which allow the user to control the movement,
        speed, acceleration, and directionality of the motor on-demand.
        """
        ### Constructor omitted for clarity ###

        def initialisation(self):
            """Initialisation method.

            This is called when a device is registered with the Commanduino
            Command Manager class.
            This method is where the initial values for the device are set.
            Certain values are for internal use whilst others are used to
            construct commands to send to the Arduino to obtain device values.
            """

            # Set the speed of the motor when it is moving
            self.set_running_speed(self.init_speed)

            # Set the maximum speed for the motor
            self.set_max_speed(self.init_max_speed)

            # Set the acceleration of the motor
            self.set_acceleration(self.init_acceleration)

        ### Methods omitted for clarity ###

        def set_max_speed(self, steps_per_second: int):
            """Method for setting the maximum speed of the motor.
            The command is constructed using `steps_per_second` in the `send()`
            method and is sent to the Arduino.
            This will limit the speed of the motor when it is moving on the Arduino.

            Args:
                steps_per_second (int): Number of steps to move per second.
            """

            # Construct and send the command to Arduino with supplied arguments.
            self.send(COMMANDACCELSTEPPER_SET_MAXSPEED, steps_per_second)

    \end{minted}
    \caption{
        Example of the initialisation method of the \mintinline{python}{CommandAccelStepper} \acrlong{cd}.
        This method sets the speeds and acceleration of the motor upon device registration with the \acrlong{cm}.
    }
    \label{fig:cmd-cd-init-example}
\end{figure}