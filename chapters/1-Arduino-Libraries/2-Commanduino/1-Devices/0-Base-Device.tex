Each Command Device is built upon a foundational base class, similar to the \acrlong{ch}s, which contains all methods common to each device.
These child classes inherit these methods from the parent \mintinline{python}{CommandDevice} class and implement their own methods specific to each device.
Figure \ref{fig:cmd-cd-inheritance} highlights the software inheritance structure of the \acrlong{cd}s.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.15]{CommandDevices.png}
    \caption{
        Inheritance structure of Commanduino's \acrlong{cd}s demonstrated with two supported devices.
        Each device inherits the methods of the parent \mintinline{python}{CommandDevice} class and implements their own device-specific methods.
    }
    \label{fig:cmd-cd-inheritance}
\end{figure}

The base \mintinline{python}{CommandDevice} class is responsible for the initialisation and setup of the device.
This involves creating a base level \acrlong{ch} which is used to receive any requests and parse them to execute any registered callbacks.
The base level \acrlong{ch} is used here as opposed to the Serial/TCPIP variant because no sending of messages occurs here; all sending and receiving of requests is handled by the \acrlong{cm}'s \acrlong{ch} which will call the relay to send the message down to this device.
For the device to send any request to the Arduino, the base level \acrlong{ch} constructs the request and calls the \mintinline{python}{write()} method.
This method is registered and assigned with the \acrlong{cd} upon registration with the \acrlong{cm} which will use the \mintinline{python}{write()} method of the \acrlong{cm}'s handler.
This could belong to a Serial or TCPIP handler, dependent on user config.

To enable \acrlong{cd}s to send/receive commands, a method was developed to register these commands similar to the \acrlong{ch} but with greater functionality.
The registration works as follows:

\begin{itemize}
    \item A variable name is supplied so that an instance of it can be created for the device with a default value.
    This variable is used to store the values for the request being registered.
    \item A lock for this variable is created so that when any updates occur, they are safe and synchronos.
    \item Two command strings are supplied as a pair to represent the request to be sent to the Arduino and the response received from it.
    \item The response command is registered with the device's internal \acrlong{ch} which is associated with a supplied callback function that is invoked when the response is received.
    This callback will do any checks and conversions that are needed, and the instance variable created is updated with this received value.
    \item A \mintinline{python}{request()} method is defined internally which will send the request command plus any arguments to the Arduino.
    \item A \mintinline{python}{get()} method is also defined internally which will invoke the \mintinline{python}{request()} method and wait for a response.
    \item The internal \acrlong{ch} will parse the response and invoke the callback which will update the variable which the \mintinline{python}{get()} method will return to the user.
    \item Both of these methods are postfixed with the supplied request's variable name so that they can be called for each request that is registered.
\end{itemize}

Through this single \mintinline{python}{register_request()} method the user can register a command with device's internal \acrlong{ch}, assign instance variables to store values for this command, and create inline methods for requesting and updating these variables (Figure \ref{fig:cmd-cd-register-req}).
An example of how this works for a \acrlong{cd} is highlighted in Figure \ref{fig:cmd-cd-register-req-example}.


\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
    # Command pair to register for the device
    CMD_REQUEST_LEVEL = "R" # Requests a value from the Arduino
    CMD_ANSWER_LEVEL = "L" # Response tag from the Arduino

    class CommandAnalogRead(CommandDevice):
        """Representation of the CommandAnalogRead device for Commanduino.
        This demonstrates how requests are registered for a Command Device.
        """
        ### Constructor omitted for clarity ###
        def register_all_requests(self):
            """Registeres all requests and commands for the device.
            In this case, only one request is registered - To obtain a value for an
            analog pin on the Arduino
            """

            # Call parent class register request method to register a command
            # requesting the value of an Analog pin on the Arduino.
            self.register_request(
                CMD_REQUEST_LEVEL, # Request Command
                CMD_ANSWER_LEVEL, # Answer command
                "level", # Name of the variable to set for the device
                self.handle_level_command # Command callback to run when response is received
            )

        def handle_level_command(self, *arg):
            """Callback to be registered with the Command Handler when a response is received.
            Converts the response to an integer and sets the variable created when the request
            registerd.

            Args:
                *arg (Variable): Value received from the Arduino
            """

            # If the variable is not null, set the value
            if arg[0] is not None:
                self.level = int(arg[0])
                self.level_lock.ensure_released()
    \end{minted}
    \caption{
        Demonstration of a how requests are registered for a Command Device.
        Each command has a request and response command pair which are used to obtain values from the Arduino and to parse the response messages received.
    }
    \label{fig:cmd-cd-register-req-example}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
    def register_request(
        self, req_cmd, ans_cmd, variable, callback, init_val=None, timeout=DEFAULT_TIMEOUT
    ):
        """Method of the Command Device parent class for registering requests with a device.
        Registers a request to and from the device to the Arduino.
        This will register the command with the CommandHandler and create appropriate
        methods for requesting commands from the Arduino and parsing the responses
        for updating the variables associated with them

        Args:
            req_cmd (str): Command for requesting a value
            ans_cmd (str): Command for receiving the value
            variable (str): Name of the variable to set for the device
            callback (Callable): Function to execute when message is received from Arduino
            init_val (Any): Initial value to set the variable to (Default: None)
            timeout (float): Time to wait for a response before throwing error (Default: 0.01)
        """

        # Set the variable on the device with the initial value 
        setattr(self, variable, init_val)

        # Create a lock variable to ensure safe access to variable
        lock = variable + "_lock"
        setattr(self, lock, Lock(timeout))

        # Register the command with the CommandHandler
        self.cmdHdl.add_command(ans_cmd, callback)

        # Name of the request function to send request to Arduino
        req_func_name = "request_" + variable

        def request():
            """Request function to send command to the Arduino
            """
            self.send(req_cmd)
        
        # Register the request method for the request with the device
        setattr(self, req_func_name, request)

        # Name of the Get method to get the variable value
        get_func_name = "get_" + variable

        def get():
            """Requests the value from the Arduino and waits until a response is given.
            """

            # Lock access to the variable
            var_lock = getattr(self, lock)
            var_lock.acquire()

            # Call the request function and wait until safe to access
            getattr(self, req_func_name)()
            is_valid, elapsed = var_lock.wait_until_released()
            var_lock.ensure_released()

            # Return the variable if found or throw an timeout error
            return getattr(self, variable) if is_valid else raise CMDeviceReplyTimeout(self.cmdHdl.cmd_header, req_cmd, elasped)
        
        # Register the get method for the request with the device
        setattr(self, get_func_name, get)
    \end{minted}
    \caption{
        Register request method for a base level Command Device.
        The command and callback are registered with the Command Handler and function for requesting and receiving the value for the command are internally set.
    }
    \label{fig:cmd-cd-register-req}
\end{figure}

The \acrlong{cd} base class defines the minimal functionality which is needed for each device which inherits it.
Whilst the base \acrlong{cd} offers basic functionality, this can be easily expanded upon / overridden in the child classes.
As each device has different capabilities and setups, the ability to modify these methods to suit the device shows the flexibility of the inheritance design in this library.
