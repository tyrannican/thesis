One of the benefits of the Commanduino \acrlong{cm} is that it supports the ability to simulate operations of \acrlong{cd}s.
It achieves this by creating these bespoke \textbf{Virtual Device} objects (Figure \ref{fig:cmd-mgr-virtual-dev}) in place of the \acrlong{cd}s.
This is useful for cases where the user wishes to test their software without having to connect up the Arduino to the host machine.
Whilst this approach will not actually test the physical hardware, it allows the user to test their code flow execution to check for runtime and logic errors.
Each \textbf{Virtual Device} is constructed of \textbf{Virtual Attributes} (Figure \ref{fig:cmd-mgr-virtual-attr}) which are used to simulate method calls for the device.
The virtual attributes are set by the \mintinline{text}{"config"} parameter of the device configuration (Figure \ref{fig:cmd-mgr-dev-cfg}).
When these attributes are called upon, they log a message to the user to let them know that the appropriate parameters/methods have been called when they should be.
An example script using a virtual device and generated logs are shown in Figure \ref{fig:virt-device-script} and \ref{fig:virt-device-log} respectively.

\begin{figure}[H]
    \begin{minted}[fontsize=\tiny]{python}
    class VirtualDevice:
        """Creates a VirtualDevice object that mimics method calls for CommandDevices. """
        def __init__(self, name: str = None, device_info: Dict = {}):
            # Obtain a logger for this object
            self.logger = logging.getLogger(__name__).getChild(self.__class__.__name__)

            # Device info is supplied
            if device_info is not None:
                # Iterate through all entries and set them as attributes of the
                # VirtualDevice
                for key, value in device_info.items():
                    self.__dict__[k] = v
            
            # Note: self.__dict__ is an internal representation of the object
            # and it's associated attributes and values

        def __getattr__(self, name: str) -> VirtualAttribute:
            """Called when an attribute is requested for the object (obj.attr)
            If it exists, return the VirtualAttribute.
            If not, create a VirtualAttribute and return that.

            Args:
                name (str): Name of the atribute

            Returns:
                VirtualAtrtibute: VirtualAttribute (method call).
            """

            # The requested attribute is already set for the object
            if name in self.__dict__:
                # Return that attribute
                return self.__dict__[name]

            # Not present, create a VirtualAtribute and return it
            self.__dict__[name] = VirtualAttribute(name, self.logger)
            return self.__dict__[name]

        def __setattr__(self, name: str, value: Any) -> None:
            """Called when an attribute is set

            Args:
                name (str): Name of the attribute
                value (Any): Value to set attribute to
            """

            # Attribute is not present, log message that it's being created
            if name not in self.__dict__:
                self.logger.debug("Creating virtual attribute %s=%s", name, value)
            
            # Warn user that a callable attribute is being redefined
            elif callable(self.__dict__[name]):
                self.logger.warning("Redefining virtual method %s with virtual attribute", self.__dict__[name])
            
            # Set attribute
            self.__dict__[name] = value

    \end{minted}
    \caption{
        Virtual device class definition.
        Sets attributes found in the device configuration as methods that can be called and logged to the user to ensure correct program operation and flow.
    }
    \label{fig:cmd-mgr-virtual-dev}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class VirtualAttribute:
            """Represents a virtual attribute, or method call, for a VirtualDevice
            object.

            Simulates the calling of a method by logging a message to the user

            Args:
                name (str): Name of the attribute
                logger (logging.Logger): Logger to log messages
            """

            def __init__(self, name: str, logger: logging.Logger):
                self.name = name
                self.logger = logger
                self.logger.info("Created virtual method %s()", self.name)

            def __call__(self, *args, **kwargs):
                """Adds the ability to make this object callable as a function.
                Simple logs a message to the user saying that this attribute
                has been called with the supplied arguments and keyword arguments
                """

                # This simply makes the arguments passed look nice for the logger.
                args = ", ".join([str(arg) for arg in args])
                if args and kwargs:
                    args += ","
                kwargs = ", ".join([str(k)+"="+str(v) for k, v in kwargs.items()])

                # Log message for the user
                self.logger.info("Virtual call %s(%s%s)", self.name, args, kwargs)
    \end{minted}
    \caption{
        \mintinline{text}{VirtualAttribute} class which represents a virtualised method call for a \mintinline{text}{VirtualDevice}.
        This is used to mock methods that are present on \acrlong{cd}s by logging calls to the user.
    }
    \label{fig:cmd-mgr-virtual-attr}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class SimulationScript:
            """Simulation example of how virtual devices are used.
            When a simulation flag is passed to the CommandManager constructor
            via the config, this triggers the setup of VirtualDevices in place of CommandDevices.
            This allows the user to simulate hardware operations in Commanduino without requiring
            a physical setup.
            If no simulation flag is given, Commnaduino instead attempts to instantiate the hardware
            using the Arduino connection.
            """
            def __init__(self, config: Dict):
                config["simulation"] = True
                self.mgr = CommandManager(**config)
                if config['simulation']:
                    self.mgr.log("Mode = Simulation")

            def move_and_set_pin(self, value: int):
                """Moves a motor to a position and sets a pin to the given value.
                If this is a simulation, these objects on the manager are VirtualDevices with
                VirtualAttributes and these are set in place of the physical hardware.
                These simply log a message to the user.

                Args:
                    value (int): Value to set the pin to
                """

                # Move a motor to the given position
                self.mgr.motor1.move_to(34000)

                # Set the Pins PWM value
                self.mgr.pin1.set_pwm(value)

                # Home the motor
                self.mgr.home_motor("motor1")

        if __name__ == "__main__":
            cfg = read_config("config.json")
            s = SimulationScript(cfg)
            s.move_and_set_pin(150)
    \end{minted}
    \caption{
        Example of how Virtual Devices can be used in place of Command Devices.
        If a simulation flag is given to the Commanduino constructor via the config dictionary, it triggers the creation of Virtual Devices in place of Command Devices.
        These viretualised devices behave similar to their Command Device counterparts although they do not physically interact with hardware.
        They are designed to log messages to the user to ensure correct operating procedure.
    }
    \label{fig:virt-device-script}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{text}
        [12:56:31|04-01-22]::SimulateBot--CommandManager instantiated.
        [12:56:31|04-01-22]::SimulateBot--Mode = Simulation
        [12:56:42|04-01-22]::SimulateBot--Virutal Device (motor1) method (move_to) called with (value=34000)
        [12:56:43|04-01-22]::SimulateBot--Virutal Device (pin1) method (set_pwm) called with (value=150)
        [12:56:44|04-01-22]::SimulateBot--Virtual Device (motor1) method (home) called with ().
    \end{minted}
    \caption{
        Example of the log output from Figure \ref{fig:virt-device-script} execution.
        As this was a simulation, the Virtual Devices were called and logged their output to a log file.
        This is to aid the user in determining the correct operating procedure for their setup without having access to physical hardware.
    }
    \label{fig:virt-device-log}
\end{figure}
