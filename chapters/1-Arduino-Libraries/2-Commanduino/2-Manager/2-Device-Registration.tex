Once the \acrlong{ch}s have been created, the \acrlong{cm} then proceeds to register each device present in the device configuration (Figure \ref{fig:cmd-mgr-dev-cfg}).
Each configuration is passed to the \mintinline{python}{register_device(name: str, dev_cfg: Dict)} method of the manager where the appropriate instantiation steps occur (Figure \ref{fig:cmd-mgr-dev-registration}).
The first step is to determine if a \mintinline{text}{"command_id"} entry is present in the configuration.
If one is not present, then the manager will raise an error to the user as a device cannot be used without a valid ID.
Once a valid ID has been found, the manager uses what is known as the \textbf{CommandBonjour} service to determine if the device is loaded onto the Arduino.
This service checks all registered handlers and constructs a \textbf{Ping} command (detailed in Section \ref{sec:act-command-devices}) for the device, created from the device's \mintinline{text}{command_id}, to send to the Arduino's \acrlong{cm} handler which will relay the message to the appropriate device.
If the device is present, a response message is then sent from the Arduino to the Commanduino \acrlong{cm} handler which determines if the device is present and ready to be communicated with.
The device configuration is then used in an external register function (\mintinline{python}{create_and_setup_device()} Figure \ref{fig:cmd-mgr-external-reg}) which creates the appropriate \acrlong{cd} and constructs the initialisation commands detailed in the \mintinline{text}{"config"} portion of the device's configuration.
Once a device is registered, it is then added to a key-value pair data structure (dictionary) so that the user can access the \acrlong{cd} via the device name supplied for the device as a key.
In the case of simulating hardware, a \textbf{VirtualDevice} object is created in place of a \acrlong{cd} so that the user can simulate and test device calling methods.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
    def register_device(self, device_name: str, dev_cfg: Dict) -> None:
        """Register a device with the Command Manager using the supplied device configuration.

        Manager will attempt to contact the Arduino to determine if the device is present.
        If no device is present a minimal object is created in it's place.
        If simulating hardware, a VirtualDevice is created instead of a CommandDevice

        Args:
            device_name (str): Name of the supplied device
            dev_cfg (Dict): Device configutation
        """

        # Get the command ID of the device and throw error if not present
        command_id = dev_cfg.get("command_id", "")
        if command_id == "":
            raise CMDeviceConfigurationError(f"Invalid or missing 'command_id; in {device_name} config!")

        # Get any configuration settings for the device
        cfg = dev_cfg.get("config", {})

        # Not simulating hardware
        if not self._simulation:
            # Attempt to find the device on the Arduino
            try:
                # Create the ping service
                bonjour_service = CommandBonjour(self.commandhandlers)

                # Attemtps to locate the device on the Arduino
                handler, ping_id, elapsed = bonjour_service.detect_device(command_id)
            
            # No device found, raise error
            except CMBonjourTimeout:
                raise CMDeviceDiscoveryTimeout(f"Device {device_name} has not been found!")
            
            # Attempt to create a CommandDevice
            try:
                device = create_and_setup_device(handler, command_id, ping_id, cfg)
            
            # Canot create device, create minimal device object instead
            except CMDeviceRegisterError:
                device = create_and_setup_device(handler, command_id, DEFAULT_ID, cfg)
            
            # Add to dictionary for easy access
            self.devices[device_name] = device
        
        # Simulating hardware, create a virtual device instead
        else:
            device = VirtualDevice(device_name, cfg)
            self.devices[device_name] = device
    \end{minted}
    \caption{
        Device registration from a supplied configuration.
        Each device uses a ping service to determine if the device is present on the Arduino and a \acrlong{cd} is created if found.
        In the case of simulation, a \mintinline{text}{VirtualDevice} is created instead.
    }
    \label{fig:cmd-mgr-dev-registration}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
    def create_and_setup_device(handler: CommandHandler, cmd_id: str, bonjour_id: str, dev_cfg: Dict) -> CommandDevice:
        """Creates a CommandDevice from the device configuration and performs the device setup

        The devices' write method is set from the handler and the device `init()` method
        is called to initialise all settings and register all commands.

        Args:
            handler (CommandHandler): Command Handler to use for communication.
            cmd_id (str): Command ID for the device
            bonjour_id (str): Ping command to determine if device is on Arduino
            dev_cfg (Dict): Device configuration for the device

        Returns:
            CommandDevice: Initialised Command Device.
        """

        # Check if the device is present in the service register
        # This register contains all Command Device Ping commands with
        # related Command Device constructors
        if bonjour_id in BONJOUR_REGISTER:

            # Create the appropriate CommandDevice from the supplied configuration
            device = BONJOUR_REGISTER[bonjour_id].from_config(dev_cfg)

            # Add a relay for the device ID
            handler.add_relay(cmd_id)

            # Set the command header for the device
            device.set_command_header(cmd_id)

            # Set the write method for the device
            # This uses the supplied handler's `write()` method
            device.set_write_function(handler.write)

            # Initialise the device
            device.init()

            # Return the device
            return device
        
        # Not present in the register, raise an error
        else:
            raise CMDeviceRegisterError(bonjour_id)
    \end{minted}
    \caption{
        External function used to create and initialise a Command Device.
        The device's communication method is set from the handler and all commands are registered with initialisation commands sent to the Arduino.
    }
    \label{fig:cmd-mgr-external-reg}
\end{figure}
