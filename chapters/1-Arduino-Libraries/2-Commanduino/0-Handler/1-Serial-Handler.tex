The Serial \acrlong{ch} of Commanduino handles communication with the Arduino via serial communication which is the default handler for the library.
It inherits all the methods from the base CommandHandler class whilst defining its own methods for handling serial specific operations.
The handler is designed to run in a concurrent fashion so inherits the \mintinline{python}{threading.Thread} class in the Python standard library to enable such behaviour.
Figure \ref{fig:cmd-sch-ctor} shows the constructor for the \mintinline{python}{SerialCommandHandler} class showing the inheritance of the necessary classes and opening the serial connection.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class SerialCommandHandler(threading.Thread, CommandHandler):
            """Child class of the CommandHandler to handle communication to/from the Arduino
            using Serial Communication

            Args:
                port (str): Serial port to open
                baudrate (int): Communication rate for the serial connection.
                    Defaults to DEFAULT_BAUDRATE (115200)
                timeout (float): Wait time between sending/receiving commands before
                    error is thrown. Defaults to DEFUALT_TIMEOUT (0.01s)
                delim (str): Delimiter character for commands.
                    Defaults to DEFAULT_DELIM (,)
                term (str): Terminator character for commands.
                    Defaults to DEFAULT_TERM (;)
            """

            def __init__(
                self, port: str, baudrate: int = DEFAULT_BAUDRATE, timeout: float = DEFAULT_TIMEOUT, delim: str = DEFAULT_DELIM, term: str = DEFAULT_TERM):
                # Inherit the thread class by calling constructor
                threading.Thread.__init__(self)

                # Set to run until the program exists
                self.daemon = True

                # Locking mechanism to facilitate main run loop
                self.lock = threading.Lock()

                # Instantiate the CommandHandler parent class with appropriate arguments
                CommandHandler.__init__(self, delim, term)

                # Serial port name
                self.name = port

                # Attempt to open the serial connection
                # This try/catch exception handler wil lraise an error if the port cannot be opened.
                # This is part of the `open()` method but is included here for clarity
                try:
                    # Open connection
                    self._serial = serial.Serial(port, baudrate, timeout=timeout)
                
                # Error occurred, raise error handler exception message
                except (serialSerialException, TypeError, ValueError) as e:
                    raise CMHandlerCondfigurationError(e)
    \end{minted}
    \caption{
        Constructor of the \mintinline{python}{SerialCommandHandler} class of Commanduino highlighting the inheritance structure and initialisation methods.
    }
    \label{fig:cmd-sch-ctor}
\end{figure}

As mentioned previously, the handler is designed to run concurrently with other processes, so it is ran in its own thread.
The \mintinline{python}{run()} method (Figure \ref{fig:cmd-sch-run}) runs for as long as it holds what is known as a \textbf{Lock}: a multithreading concept.
Locks are used in multithreaded programs to ensure that resource access is synchronised between multiple threads.
So long as one thread holds the lock, all other threads are blocked from accessing the requested resource until the lock is released.
Whichever thread obtains the lock afterwards will then have access to the resource and the process continues.
A lock was used in the handler as a safety mechanism to ensure that no other thread may access the serial port for as long as it was in use.
The lock is only released when the user signals an interrupt to end the program or when an error is thrown.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def run(self) -> None:
            """Start the Handler thread for processing commands.
            This will run for as long as the current thread holds the lock.
            Any error or user interrupt will cause the lock to be released
            and will close the serial connection.
            """

            # Acquire the lock
            self.lock.acquire()

            # Run for as long as the lock is in the thread's possession
            while self.lock.locked():
                # Attempts to read from the serial connection
                try:
                    # Process a command received from the serial connection.
                    # This is similar to how the Arduino Command Handler operates
                    # The serial data is parsed character by character and processed
                    # by the parent class's `process_char(chr)` method
                    self.process_serial(self._serial)
                
                # Error occurred when reading from serial connection
                # This invokes the `stop()` method of the class which releases
                # the lock with `self.lock.release()`.
                except (serial.SerialException, serial.SerialTimeoutException) as e:
                    raise CMTimeout(f"Error reading from Serial port {self._serial.port} {e}") from None

            # Cleanup the serial connection by closing it.
            self.close()
    \end{minted}
    \caption{
        Implementation of the \mintinline{python}{run()} method which is the main loop of operation.
        Runs for as long as the lock is held and processes serial communications.
    }
    \label{fig:cmd-sch-run}
\end{figure}

For sending response messages, the \mintinline{python}{SerialCommandHandler} will first build the command with appropriate delimiters and terminators.
The command is then encoded using Python's default encoding (UTF-8 standard) and is then sent over the serial connection back to the Arduino (Figure \ref{fig:cmd-sch-send-msg}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
    def forge_command(self, command_id: str, *args) -> str:
        """CommandHandler base method for constructing commands to return
        to the Arduino.
        Each command is prefixed with the command header, command ID,
        and any supplied arguments separated by the delimiter and terminated
        with the terminator character

        Args:
            command_id (str): Command ID for the command
            *args (Variable): Arguments for the command
        """

        # Initialise the command with the header
        cmd = self.cmd_header

        # Append the command ID
        cmd += command_id

        # Iterate through all arguments
        for arg in args:
            # Add a delimiter and argument to the command
            cmd += self.delim
            cmd += str(arg)
        
        # Terminate the command
        cmd += self.term

        # Return the command
        return cmd

    def send(self, command_id: str, *args):
        """Creates a command and sends the command over serial back to the Arduino.

        Args:
            command_id (str): Command ID for the command
            *args (Variable): Arguments for the command.s
        """

        # Construct the command
        cmd = self.forge_command(command_id, *args)

        # Attempt to send over serial connection
        try:
            # Send the command over serial
            self._serial.write(cmd.encode())
        
        # Raise exception if any error occurs
        except serial.SerialException as e:
            raise CMCommunicaitonError(f"Error writing to serial port! {e}") from None
    \end{minted}
    \caption{Methods showing how command are constructed and sent to the user. The commands are constructed with supplied arguments, encoded, then sent over serial back to the Arduino.}
    \label{fig:cmd-sch-send-msg}
\end{figure}

This version of the \acrlong{ch} is essentially of a mirror of the \acrlong{ach} which performs the same job of processing messages received over serial connection.
To expand support for different communication protocols, it was decided to include a \acrlong{ch} which could support message parsing over TCP/IP connections.
This enables the ability to communicate with Arduino boards using the \acrlong{ach} and the \acrlong{act} libraries over networked connections.