It was decided to include support for another communication protocol for the \acrlong{ch} to expand the capabilities of Commanduino.
TCP/IP was selected the communication protocol as there are future plans to power the Arduino devices over Ethernet using "Power-over-Ethernet" (PoE).
This combines the communication and power into one single line.
However, doing so would eliminate the use of the serial port so communicating using TCP/IP packets over Ethernet would be the next step.
Once a suitable TCP/IP hardware solution is implemented for the Arduino board, this implementation will be added to the \acrlong{ach} library.
The only difference between the \mintinline{python}{SerialCommandHandler} and the \mintinline{python}{TCPIPCommandHandler} is simply their method of communication.
The TCP/IP handler uses network sockets inplace of serial connections to facilitate communication.
This allows for the end-user's computer and the target Arduino to communicate over a network as opposed to hardline connection using serial however this is not how the handler is primarily used; it is designed to use a hardline ethernet connection between the board and the end-user's computer.
Figure \ref{fig:cmd-tcp-ctor} shows how these sockets are set up for communication.

The TCP/IP handler supports two forms of network protocol: Transmission Control Protocol (\textbf{TCP}) and User Datagram Protocol (\textbf{UDP}).
The exact definitions of both of these protocols are not within the scope of this project or library but it included to know there are two forms of communication.
The \textbf{TCP} sockets are used in cases where each packet of data sent to the receiving end acknowledges that it has been received.
It is used when a two-way connection must be established, and both ends of the connection communicate synchronously with each other; this can be thought of as a "blocking" socket in that all data sent must be acknowledged and received.
\textbf{UDP} sockets on the other hand are the opposite; these connections do not care about the data being received and what order it arrived in, so long as it receives something.
This can be thought of as a "non-blocking" socket in that all data can be sent regardless of the receiving end acknowledging its arrival to the user or if any data packets are dropped during transmission.
For most cases, a TCP connection is required as the data contained in the packets will be commands to be parsed and processed so the "correctness" and validity of the data packets must be maintained; something that is not guaranteed when using UDP.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class TCPIPCommandHandler(threading.Thread, CommandHandler):
            """Child Handler class to for supporting TCP/IP communication.

            Args:
                port (str): Network prot to communicate over
                addr (str): Network address of the Arduino
                protocol (str): Network protocol to use (TCP or UDP)
                timeout (float): Network timeout.
                    Defaults to DEFAULT_TIMEOUT (0.01)
                delim (str): Delimiter character between command arguments.
                    Defaults to DEFAULT_DELIM (,).
                term (str): Terminator character for commands.
                    Defaults to DEFAUTL_TERM (;)

            """
            def __init__(
                self, port: str, addr: str, protocol: str = "TCP", timeout: float = DEFAULT_TIMEOUT,
                delim: str = DEFAULT_DELIM, term: str = DEFAULT_TERM
            ):
                # Initialise the Thread capabilities
                threading.Thread.__init__(self)
                self.daemon = True

                # Create a flag for determining when to exit
                self.exit_flag = threading.Event()
                self.exit_flag.cleart()

                # Instnantiate the parent CommandHandler class
                CommandHandler.__init__(self, delim, term)

                # Create a socket connection
                self._connection: socket.socket = None
                
                # Attempts to open the socket connection
                # This is part of the handler's `open()` method but is included
                # here for clarity.
                try:
                    # Using TCP, set up TCP socket
                    if protocol == "TCP":
                        self._connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    
                    # Using UDP, set up a UDP socket
                    elif protocol == "UDP":
                        self._connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    
                    # Unrecognised protocol, raise error
                    else:
                        raise CMHandlerConfigurationError(f"Unknown protocol: {protocol}")
                    
                    # Connect to the target network address and port.
                    self._connection.connect((addr, int(port)))
                    self._connection.settimeout(timeout)
                
                # Can't connect to socket
                except TimeoutError as e:
                    raise CMHandlerConfiguratioNError(f"Socket timeout: {e}")
                
                # General errors
                except (OSError, TypeError, ValueError) as e:
                    raise CMHandlerConfigurationError(f"Cannot open socket: {e}")

    \end{minted}
    \caption{
        \mintinline{python}{TCPIPCommandHandler} class showing creation of network sockets to facilitate communication.
        Two separate network protocols are supported: Transmission Control Protocol (\textbf{TCP}) and User Datagram Protocol (\textbf{UDP}).
    }
    \label{fig:cmd-tcp-ctor}
\end{figure}

Just as with the \mintinline{python}{SerialCommandHandler}, the \mintinline{python}{TCPIPCommandHandler} class was designed to run in parallel with other process in its own thread.
As it inherits the \mintinline{python}{threading.Thread} class of the Python standard library, the main execution method for this handler is the \mintinline{python}{run()} method (Figure \ref{fig:cmd-tcp-run}) which is overridden from the \mintinline{text}{Thread} class.
This will run for as long as the threading event that was defined in Figure \ref{fig:cmd-tcp-ctor} has not been set.
This acts similar to the locking design that as implemented in Figure \ref{fig:cmd-sch-run}.
This event, or flag, is set when the program is exited by the user, or an error occurs.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def run(self) -> None:
            """Main method of execution for the TCPIPCommandHandler
            So long as the exit flag is not set, a single char is read from
            the maintained socket connection and is passed to the parent
            CommandHandler `process_char()` method for processing.
            """

            # Run for as long as the exit flag is not set
            while not self.exit_flag.is_set():
                # Attempt to read a byte from the socket
                # This method is contained in the TCPIPCommandHandler `process_data()`
                # method but is included here for clarity
                try:
                    # Read a single byte from the socket
                    recvd_char = self._connection.recv(1)

                    # Pass on for processing
                    self.process_char(recvd_char)

                # No response from the socket, skip error
                except socket.timeout:
                    pass
                
                # General error, raise Communication exception
                except OSError as e:
                    raise CMCommunicationError(f"Error reading from socket: {e}")
    \end{minted}
    \caption{
        Implementation of the \mintinline{python}{run()} method which is the main loop of operation.
        Runs for as long as the exit flag is not set and processes communications over the network socket.
    }
     \label{fig:cmd-tcp-run}
\end{figure}

For building response messages, the same method detailed in Figure \ref{fig:cmd-sch-send-msg} is implemented for the TCP/IP handler.
All messages are encoded using the UTF-8 encoding standard before being transmitted over the network socket to the receiving end where they are then processed.

The TCP/IP handler was developed as a future-proofing method to support different types of communication standard.
Despite there not being a networked solution available for Arduino at this point, it was decided to be added in the event that a suitable solution is found.
This was achievable due to the nature of the communications themselves; the commands to be sent and received are in the same format but the method of delivery is different.
Both the Serial and TCP/IP handlers are interchangeable when operating Commanduino (See Section \ref{sec:arduino-lib-usage}).