One of the future plans for the Ulduar framework is support for \acrshort{xdl} bindings for the base-level system.
\acrshort{xdl} is an internally created mark-up language, designed by colleagues, which was designed to represent and describe synthetic procedures in a robust and machine-readable way \cite{xdl}.
The plan would be to generate \acrshort{xdl} files that represent the synthetic steps required to reach the end target.
These \acrshort{xdl} files would be parsed by the \acrshort{xdl} program to generate blocks of executable code (\textbf{Steps}) which consist of smaller steps that contain the hardware instructions for the system to execute.
The generation of the \acrshort{xdl} file would ideally be done automatically by extracting procedures from the literature and parsing them to generate the files using natural language processing techniques.
An overview of this ideal workflow can be found in Figure \ref{fig:xdl-workflow}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.38]{xdl-workflow.png}
    \caption{
        Overview of the \acrshort{xdl} workflow compared to a traditional workflow.
        All operations in the center column are performed manually by the chemist (Left).
        Most operations in the digital workflow (Right) are automated however the chemist can inspect and override the system output without modification of the underlying software.
        Figure adapted from Mehr et. al. \cite{xdl}.
    }
    \label{fig:xdl-workflow}
\end{figure}

\subsubsection{\acrshort{xdl} Steps}
The essential building blocks of \acrshort{xdl} are called \textbf{Steps} which represent synthetic actions that a platform would execute.
These steps are denoted as \acrshort{xml}-style tags that contain the execution parameters for the step such as reagent names, liquid volumes, destination flasks etc. (e.g. \mintinline{xml}{<Add reagent="water" vessel="reactor" volume="10mL" />}).
A total of 44 high-level steps have been developed at this top-level so far with other steps being \textbf{Separate} and \textbf{Evaporate} to name a few.
Each of these steps can be combined in a linear sequence or in a branched manner which are executed concurrently by using the asynchronous capabilities built into the \acrshort{xdl} eco-system.

These high-level abstractions, whilst effective, do not cover the entire process of synthetic chemistry alone so multiple layers of abstraction were built into the \acrshort{xdl} language.
The top-level steps (e.g. \mintinline{xml}{<Add />}) consist of multiple lower-level sub-steps and even lower-level base steps which contained the low-level hardware instructions which the system would eventually execute (Figure \ref{fig:xdl-steps-breakdown}).
It is in these sub-steps and base steps where the platform-specific controls are defined.
An example of a \acrshort{xdl} file that can be used in a Chemputer system is shown in Appendix \ref{apx:lidocaine-xdl}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{xdl-steps.png}
    \caption{
        Overview of \acrshort{xdl} internal representation.
        Section \textbf{B} demonstrates \acrshort{xdl}'s \acrshort{xml}-based representation and how it propagates process information from steps to sub-steps.
        Section \textbf{C} shows the multi-step process which maps chemical synthesis steps to relevant hardware modules within the target platform and how it recursively expands steps into sub-steps and base steps until it is reduced to basic hardware instructions.
        Figure adapted from Mehr et. al. \cite{xdl}
    }
    \label{fig:xdl-steps-breakdown}
\end{figure}

\subsubsection{Ulduar Support}
As stated previously in Section \ref{sec:chemputer-conclusion}, \acrshort{xdl} supports multiple implementations of the language dependent on the underlying hardware.
The platform controller (Manager \acrshort{api} for Ulduar or the Chempiler for the Chemputer) would act as the main platform "executor" which \acrshort{xdl} would import and use to interpret and process the execution steps.
This would require platform-specific bindings to be created for each system, similar to the \textbf{ChemputerXDL} bindings that were created for the Chemputer project.

To implement this for an Ulduar platform, this would require specific bindings that interact with the underlying software modules such as Commanduino.
Steps specific to interact with the Manager \acrshort{api} would need to be created that would invoke the software modules to achieve the execution of the synthetic steps.
Work is underway to create specific platform bindings for the Clusterbot and Nanobot systems so that they are "\acrshort{xdl}-ready" when the need arises.

One of the issues to tackle with this however is the platform representation using a graph data structure which is required by the \acrshort{xdl} program.
The inspiration for using the graph within \acrshort{xdl} was one taken from the Chemputer project as it was an accurate mapping of hardware contained within the system.
The issue for Ulduar systems, however, is that the Chemputer system is considered a "static" platform where hardware remains in place with only liquids moving within the system.
Ulduar systems by their nature contain multiple moving parts which can make a "static" mapping of hardware and locations difficult to represent.
To tackle this, an idea was proposed to use the graph in a slightly different manner in the Ulduar framework.
Instead of hardware mapping, the graph would instead take the place of the Manager configuration file; listing hardware and its appropriate configuration details as nodes as opposed to physical locations.
This would result in the graph being used to instantiate the hardware only and move the logic of pathfinding down to the Manager \acrshort{api} which would then be called upon from the \acrshort{xdl} program.

In the near future, it can be envisioned that both the Chemputer and Ulduar regimes of synthetic operation will operate under the same standard protocols using \acrshort{xdl} as the driving force to expand their respective systems further into the realms of automation.