To expand the capability of this system, a pH sampling module was developed in order to determine the pH of experimental solutions.
The hardware device chosen was the DrDAQ unit available from Pico Technology \cite{drdaq} (Figure \ref{fig:clusterbot-drdaq}).
The device offers a wide variety of data logging and sensors with the ability to measure pH readings so was the ideal solution for the task at hand.
Pico Technology offer a Software Development Kit (SDK) for their devices which contain both dynamic and shared libraries for Windows and Linux systems respectively to communicate with the hardware which simplified the process of developing a software module to load into the Ulduar framework.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.25]{DrDAQ_line_Layout_greyscale_flattened.png}
    \caption{
        Schematic of the DrDAQ USB device for pH measurements.
        pH measurements were the only sensor required for this project.
        \cite{drdaq}.
    }
    \label{fig:clusterbot-drdaq}
\end{figure}

\subsubsection{Core Driver}
In order to develop a \textbf{Core Driver} for the software module, the SDK libraries from Pico Technology had to be interfaced with via Python.
This was made possible by using the \mintinline{python}{ctypes} library available in the Python standard library.
The \mintinline{python}{ctypes} library is a foreign function library which allows developers to interact with dynamic and static libraries developed in C/C++ using C compatible data types and function calls.
Using this library, it was possible to interact with the PicoSDK libraries for the DrDAQ device and gain access to the measurement and sensor capabilities of the device.

The \textbf{Core Driver} for the module was responsible for loading the DLL or shared object file (depending on host OS) and obtaining a handle to the device (Figure \ref{fig:clusterbot-drdaq-core-ctor}).
Using this handle, it was then possible to call the foreign functions in the library with the required types.
The core driver gave access to a series of low-level control functions (Figure \ref{fig:clusterbot-drdaq-low-level}) that were then tied together in sensible methods in the \textbf{Control Driver} section of the module.
A full definition of the Core Driver can be found in Appendix \ref{apx:clusterbot-drdaq-core-driver}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class DrDaqCoreDriver:
            """Core Driver for the DrDAQ device.
            Uses CTypes to call on the DLL or SO file to interact with the
            device.
            """

            def __init__(self):
                # This function will load the appropriate DLL or SO file
                # dependent on host OS.
                #
                # Windows: ctypes.windll.LoadLibrary(lib_path)
                # Linux: ctypes.cdll.LoadLibrary(lib_path)
                self.lib = _load_library()

                # Setting a series of C compatible data types used
                # for measurements
                self.recording_block = ctypes.c_int16(200000)
                self.no_of_samples = ctypes.c_int16(20000)
                self.channel = ctypes.c_int16(5)
                self.no_of_active_channels = ctypes.c_int16(1)
                self.measurement_results = (ctypes.c_short * 20000)()

                # Obtain a handle for the device to be used when communicating
                # with the device
                self.handle = self.open_unit()

            def open_unit(self) -> ctypes.c_int16:
                """Obtains a handle for the device which is used for calling
                operational methods.
                """

                # Create a CType for the handle
                hdl = ctypes.c_int16()

                # Set the handle by passing in a reference to the type
                # This is used in subsequent operations
                self.lib.UsbDrDaqOpenUnit(ctypes.byref(hdl))

                return hdl

    \end{minted}
    \caption{
        Core Driver class for the pH software module.
        Sets up the appropriate variables using CTypes and obtains a handle for the device which is used to interact with the device.
        Operational methods omitted for clarity.
    }
    \label{fig:clusterbot-drdaq-core-ctor}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def set_DAQ_interval(self) -> int:
            """Sets the sampling rate of the Dr Daq unit.

            Returns:
                int: Status code of the operation
            """

            return self.lib.UsbDrDaqSetInterval(
                self.handle,
                ctypes.byref(self.recording_block),
                self.no_of_samples,
                ctypes.byref(self.channel),
                self.no_of_active_channels
            )
        
        def run_single_shot(self) -> int:
            """Performs a single sampling run of the Dr Daq unit.

            Returns:
                int: Status code of the operation
            """

            return self.lib.UsbDrDaqRun(
                self.handle, self.no_of_samples, ctypes.c_int16(1)
            )

    \end{minted}
    \caption{
        Example of low-level methods for interacting with the Dr DAQ unit.
        The core driver is responsible for defining methods that will be used in the Control Driver of the module.
    }
    \label{fig:clusterbot-drdaq-low-level}
\end{figure}

\subsubsection{Control Driver}
The \textbf{Control Driver} for the pH module follows the same strategy as defined in Section \ref{sec:ulduar-module-layout} in that the control driver of the module inherits the capabilities of the core driver (Figure \ref{fig:clusterbot-drdaq-control-ctor}).
The only additional information supplied to the control driver was calibration data used for determining accurate pH measurements.
The control driver offers two necessary methods for obtaining the pH of a solution from the device and are as follows:
\begin{itemize}
    \item Method to sample values from the attached probe.
    \item Method to fit those measurements to the supplied calibrations.
\end{itemize}

These methods were combined into another method for simplicity called \mintinline{python}{measure_pH()}.
Two other methods were also defined in the control driver: one to close the device and release the handle and one for updating any calibration data.
Implementation of the Control Driver for the pH module can be found in Appendix \ref{apx:clusterbot-drdaq-control-driver}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class DrDaqControlDriver(DrDaqCoreDriver):
            """Control driver for the DrDaq uniot which inherits from the
            Core Driver class.

            Args:
                calibrations (list): calibratiosn used for the pH calculation.
                    Defaults to [400., 700., 1000.] for pHs 4, 7, and 10.
                    Should be updated by the user.
            """

            def __init__(self, calibrations: list = [400., 700., 1000.]):
                super().__init__(self)

                # Set calibrations used for the device
                self.calibrations = calibrations

            def close_device(self):
                """Call on the driver to release the handle
                """

                self.close_unit()
            
            def update_calibrations(calibrations: list):
                """Update the pH calibrations.

                Args:
                    calibrations (list): New list of calibrations for pHs
                        4, 7, and 10.
                """
                self.calibrations = calibrations
    \end{minted}
    \caption{
        Implementation of the Control Driver for the pH software module.
        Inherits all capabilities from the core driver and allows for the setting of pH calibration data for pHs 4, 7, and 10.
        Using these calibrations, it is possible to accurately measure the pH of a given solution.
    }
    \label{fig:clusterbot-drdaq-control-ctor}
\end{figure}

Using this software module, an added layer of flexibility is granted to the system to be able to obtain accurate and fast pH measurements for any experiment.
The pH probe itself is mounted onto an XY frame (Figure \ref{fig:clusterbot-hardware}) controlled via the Commanduino module which allows for a sample to be taken at given position on the wheel of the platform.
This module was developed in a generic sense so that it can be used in any system which utilises the Ulduar framework however, although developed for this particular system, was never used as it was not required for the scope of the project.
The module itself is available for general use in any Ulduar project.