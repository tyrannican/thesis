Using the API design from the Ulduar framework (Section \ref{sec:ulduar-manager-extension}), a custom API was built (\mintinline{python}{ClusterbotManager}) for this system.
Most functionality for the platform was controlled via the Commanduino module, but an extended Modular Manager was constructed to account for the addition of the pH module that was unfortunately never used.
The API's main functionality was to control the movement of the Geneva wheel mechanism to allow for the parallelisation of reactions and to control the peristaltic pumps (which are stepper motors) for dispensing reagents into reaction vials and for pump calibration.
The pump calibration methods were included as helpers to aid the user in calibrating the exact flow rates for each pump.
The user would do this manually by weighing a dry vial before calibration, calling the calibration method that would run water into the vial for one minute, and then measuring the vial afterwards.
This was done a total of three times for each pump and the results averages to obtain an average flow rate per minute for the pump (Figure \ref{fig:clusterbot-pump-calibration-method}).
The Manager offered two methods for dispensing liquids: one for running the pump for a given period of time (\mintinline{python}{run_motor()}) and one for dispensing a given volume (\mintinline{python}{run_pump_by_volume()}).
Once each pump was calibrated; the system exclusively used the \mintinline{python}{dispense()} method to dispense accurate volumes (Figure \ref{fig:clusterbot-pump-by-volume}).
Methods were also defined to accommodate for the pH module to obtain the pH of a given reaction and to wash the probe after analysis was conducted.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def calibrate_pump(pump_name: str) -> float:
            """Manager API method for calibrating a pump's flow rate in mL/min.
            Averages the difference between the weights before and after water addition.
            This is done a total of three times

            Args:
                pump_name (str): name of the pump to calibrate

            Returns:
                float: Average flow rate of the pump in mL/min
            """

            # List to store calibration data
            calibrations = []

            # Run for a total of three times
            for _ in range(3):
                # Obtain the weight before water addition
                before = float(input("Weight before calibration: "))

                # Run for one minute
                self.run_motor(pump_name, 60)

                # Weight of vial after water addition
                after = float(input("Weight after calibration: "))

                # Add to calibrations
                calibrations.append(after - before)
            
            # Average the calibrations to obtain a single value
            return sum(calibrations) / 3

    \end{minted}
    \caption{
        Manager API method for calibrating the pumps on the system.
        Each vial was weighed before and after the addition of water which was added over one minute.
        This value is then averaged to obtain an accurate flow rate for the given pump in mL/min.
    }
    \label{fig:clusterbot-pump-calibration-method}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def dispense(pump: str, volume: float, calibrations: dict):
            """Dispenses a volume from a peristaltic pump.
            Calculates the run time based off of the pump's calibration value.

            Args:
                pump (str): Name of the pump
                volume (float): Voluem to dispense
                calibrations (dict): Flwo rates for each pump
            """

            # Checks if the pump has a flow rate defined for itself
            # Raise error if not found
            if pump not in calibrations:
                raise PumpNotFoundException(
                    f"Pump {pump} is not found!"
                )
            
            # Obtain the flow rate
            calibration = calibrations[pump]

            # Calculate the runtime
            runtime = (volume / (calibrations / 60))

            # Dispense volume by running for designated runtime
            self.run_motor(pump, runtime)
    \end{minted}
    \caption{
        Manager API method for dispensing a volume from a peristaltic pump.
        Achieved by calculating the running time for the motor from the volume and flow rate in mL/min.
    }
    \label{fig:clusterbot-pump-by-volume}
\end{figure}

\subsubsection{Manager API Configuration File}
With numerous stepper motors required for the peristaltic pumps and XY movement modules, two Arduino Mega2560 boards were used loaded with the appropriate firmware detailed in Figure \ref{fig:arduino-command-sketch} with appropriate command ID changes to prevent clashes.
The beauty of the Commanduino design is that one single configuration can be used to represent multiple boards with all commands being relayed to the appropriate boards thanks to the Serial Handlers.
Thus the Commanduino configuration for the API contained all entries for both boards with each board's connection port detailed in the \mintinline{text}{"ios"} section of the \mintinline{text}{"commanduino"} entry in the file.
The only other addition required for the configuration was the declaration of the pH module and its calibrations which would be passed to the \mintinline{python}{DrDaqControlDriver} when created.
With this configuration (Appendix \ref{apx:clusterbot-config}), the manager API would be instantiated with the appropriate modules loaded and ready for use.
