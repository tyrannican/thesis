Serial Labware was a library that was created to house all software adapters and modules for vendor hardware.
It is similar in design to the software modules from the Ulduar framework in that modules are created as drivers to interact with the hardware.
However, unlike the Ulduar framework where a module is for one specific piece of hardware, Serial Labware was designed to be a bank of many of these modules all contained in a single library.
That way, a user could simply import whatever device is supported from Serial Labware directly instead of a dedicated standalone module.
The success of this design has influenced the design of Ulduar for future refinement in that all software modules will be ported over to this library in time so that the framework can use fewer libraries and imports going forward.
Another sub-branch of this library was a spin-off designed to hold the driver for interacting with analytical devices such as NMR spectrometers andin-line mass spectrometers.
This library (\textbf{Analytical Labware}) follows similar design patterns to Serial Labware so will not be discussed however it should be noted that the software modules for the UV-Vis spectrometers in the Ulduar framework are in the process of being moved over to this library.

Many different pieces of lab equipment are supported by Serial Labware.
So far, support has been added for rotary evaporators, overhead stirrers, stirring and heating plates, chiller units, vacuum pumps, and many more.
As each device has different modes of communication (serial, TCP/IP, HTTP), a dedicated communication class for each mode was created to support them.
Every device driver that was created would have the appropriate \mintinline{python}{XConnection} class (\mintinline{text}{X} being the type of connection) loaded in with appropriate connection parameters passed.
To handle the communication to and from the device, a generic \mintinline{python}{LabDevice} class was created which contained common methods that would prepare commands to be sent to ensure that they are in the correct format and to parse any replies received back from the device.
Each type of device (temperature controller, pressure controller, stirring controller) had their own dedicated abstract class which all inherited from this generic \mintinline{python}{LabDevice} class.
These classes contained the abstract method stubs for operations that were related to the device (e.g. start stirring, set stirring speed, etc.).
An example abstract class of this design is highlighted in Figure \ref{fig:chemputer-sl-abstract-dev}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class AbstractStirringController(LabDevice):
            @abstractmethod
            def start_stirring(self) -> None:
                """ Starts stirring """
            
            def start(self) -> None:
                """ Starts the device """
                self.start_stirring()
            
            @abstractmethod
            def stop_stirring(self) -> None:
                """Stops stirring"""
            
            def stop(self) -> None:
                """Stops the device"""
                self.stop_stirring()
            
            @abstractmethod
            def set_speed(self, speed: int) -> None:
                """Sets the speed of stirring in RPM"""
            
            @abstractmethod
            def get_speed(self) -> int:
                """ Gets the stirring speed in RPM"""
            
            @abstractmethod
            def get_speed_setpoint(self) -> int:
                """Gets desired stirring speed set point in RPM"""
    \end{minted}
    \caption{
        Example of an abstract device controller for a Serial Labware device.
        These classes are abstract as, even though the common operations are the same (start stirring etc.), their implementation may be different.
        Devices may have different command and parameters that need to be passed to the inheriting child class which is responsible for this implementation.
    }
    \label{fig:chemputer-sl-abstract-dev}
\end{figure}

Each class having their abstract counterpart allowed for the implementation of vendor-specific hardware Python drivers to be written.
When creating a driver, the first step was to peruse the vendor software manual to discover the remote-control command syntax and extract each command into a device command class.
Once the connection parameters for communication were set, all abstract methods from the parent abstract controller were implemented.
These would effectively select the appropriate command, send it to the device and parse the receiving reply which was returned to the user.
This process was essentially performed for every device driver implemented in Serial Labware with minor changes required in some cases.
An example snippet of a device implementation can be seen in Figure \ref{fig:chemputer-sl-dev-impl}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class HeiTorque100PrecissionStirrer(AbstractStirringController):
            def __init__(
                self, device_name: str, connection_mode: str, address: Optional[str], port: Union[str, int]
            ):
                """Demonstration of the implementation of a stirring controller.

                Args:
                    device_name (str): Name of the device for logging reasons.
                    connection_mode (str): Type of connection (serial, tcp/ip, http)
                    address (Optional[str]): Address of the device if required
                    port (Union[str, int]): Connection port for the device
                """

                # Set commands from the Command Device class
                self.cmd = HeiTorque100PrecisionStirrerCommands

                # Set internal variables
                self._speed_setpoint = 0
                self.running = False

                # Define connection parameters
                connection_params = {
                    "port": port,
                    "address": address,
                    "baudrate": 9600,
                    "bytesize": serial.EIGHTBITS,
                    "parity": serial.PARITY_NONE
                }

                # Instantiate base abstract class
                super().__init__(device_name, connection_mode, connection_params)

            def set_speed(self, speed: int):
                """Implementaiton of setting the speed of the stirrer

                Args:
                    speed (int): Set speed in RPM.
                """

                # Set the speed if the device is not running
                if not self.running:
                    # Check the values are in a valid range and send the command to the device
                    self.check_value(self.cmd.SET_SPEED, speed)
                    self._speed_setpoint = speed
                
                # Device is running
                else:
                    # Send the speed and parse the reply
                    readback = self.send(self.cmd.SET_SPEED, speed)

                    # Raise error if the set speeds are not the same
                    if readback != speed:
                        self.stop()
                        raise SLDeviceReplyError("Error setting stir speed.")
                    self._speed_setpoint = speed
    \end{minted}
    \caption{
        Example demonstrating the implementation of a stirring controller in Serial Labware.
        Each device takes a series of connection parameters to establish the connection between the host machine and the target device.
        The abstract methods of the parent class are implemented with respect to the device in question.
        As this is merely an example, most methods have been omitted for clarity.
    }
    \label{fig:chemputer-sl-dev-impl}
\end{figure}
