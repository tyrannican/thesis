The ChemputerAPI library was developed to interface with any custom hardware that was developed internally (i.e. not from a vendor).
It was primarily created to interact with the in-house developed pump and valve hardware that was created specifically for this project.
Each pump and valve was loaded with custom-built firmware, written in C, to perform the movement operations such as moving the plunger up and down and rotating the valve mechanism to specific locations.
The method of communication with the devices was via a TCP/IP connection over Ethernet which was built into the device from the initial designs.
Each device had a pre-loaded TCP/IP server with a dedicated IP that was accessible through a network switch attached to the host computer.

The ChemputerAPI was a Python command-builder which would connect to the dedicated IP of the hardware and start a communication channel that commands could be through, and responses received.
The purpose of the API was to initialise a TCP/IP connection with the host device using low-level socket communication and pre-load a network and device configuration containing network and device parameters.
Each device (pump or valve) inherited a base class (\mintinline{python}{_ChemputerEthernetDevice}) which handled the communication between the device and host machine.
All of the commands were sent via the \mintinline{python}{_send_command()} method of the class which constructed the appropriate command, encoded the message, and sent it to the device.
Each response was received by the \mintinline{python}{_receive_response()} method which decoded the message and checked the status from the device for errors and other messages.
This method ran in its own separate thread from the main execution thread so as not to lead to blocking behaviour.
To ensure a constant communication stream was available (i.e. the device had not disconnected), a keep-alive signal was consistently sent from the host machine to the target device on a dedicated address to ensure the device was still available (Figure \ref{fig:chemputer-api-keepalive}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def _udp_keepalive(self, udp_host: Tuple[str, int]):
            """Broadcasts the UDP keepalive signal to ensure the board is still alive.

            Args:
                udp_host (Tuple[str, int]): Tuple contianing the IP Address and port
                    of the server e.g. (192.168.255.255, 3000)
            """
            # Creates a socket to use for the network communication
            self.udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

            # Loop forever
            while True:
                # Check if a disconnect signal has been received, finish if so
                if self._should_disconnect():
                    return

                # Send the keepalive token every 0.5 seconds to ensure alive status
                self.udp.sendto(KEEPALIVE_COOKIE.encode(), udp_host)
                time.sleep(0.5)
    \end{minted}
    \caption{
        UDP keepalive broadcast to ensure the device is still present.
        Each device automatically starts its own thread to ensure the keepalive is sent in the background.
    }
    \label{fig:chemputer-api-keepalive}
\end{figure}

Two classes (\mintinline{python}{ChemputerPump} and \mintinline{python}{ChemputerValve}) were created to represent both the pumps and valves.
Both of these inherited from the \mintinline{python}{_ChemputerEthernetDevice} class which gave them access to the network communication aspects.
Each of these classes also inherited from an abstract class representation of their respective device which itself inherited from a base class known as \mintinline{python}{ChemputerDevice}.
This gave the devices access to the \mintinline{python}{execute()} method which would be used to execute any command given to the device.
Figure \ref{fig:chemputer-api-class-diagram} shows a class diagram which details the class hierarchy for this API.

Each device implemented their own methods in relation to their mode of operation as well as extending the capabilities offered to it by inheriting the \mintinline{python}{ChemputerDevice} class via the abstract class definitions.
The \mintinline{python}{ChemputerPump} offered methods for moving the syringe motor to draw and dispense liquids with in-built calculation to determine volume from motor steps.
The \mintinline{python}{ChemputerValve} offered similar methods, moving the bearing unit to a set position which would align the input ports to the correct positions.
An example of method implementations are shown in Figure \ref{fig:chemputer-api-pump-example} with a full definition of the Chemputer API given in Appendix \ref{apx:chemputer-api-pumps-and-valves}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.20]{chemputer-api-class-diagram.jpg}
    \caption{
        Class diagram highlighting the class hierarchy of the Chemputer API for the Pumps and Valves.
        Each device inherits from the \mintinline{python}{_ChemputerEthernetDevice} which defines the network communication protocol.
        Each device inherits from its abstract counterpart which itself inherits directly from the \mintinline{python}{ChemputerDevice} class which defines the execution method for the device.
    }
    \label{fig:chemputer-api-class-diagram}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def move_absolute(self, vol_ml: float, speed_ml: float):
            """Moves the syringe plunger to a specfici volume, regardless of current
            position.

            Args:
                vol_ml (float): Volume in milliliters
                speed_ml (float): Speed to move in milliliters per minute
            """

            # Calculate the volume and speed for microliters
            target_volume_in_microliters = int(float(vol_ml) * 1000)
            speed_in_microliters_per_min = int(float(speed_ml) * 1000)

            # Send the command over the network
            # Uses the hard-coded command string defined in self.MOVE_ABSOLUTE
            self._send_command(
                self.MOVE_ABSOLUTE, target_volume_in_microliters, speed_in_microliters_per_min
            )

            # Set the current volume for the device
            self.volume = target_volume_in_microliters
    \end{minted}
    \caption{
        Example of how commands for a device are implemented in their class definition.
        This shows the method implementation for operating the syringe plunger in the \mintinline{python}{ChemputerPump} class.
        Internal calculations for volume translation occur here and the commands are sent to the device hardware over the network.
    }
    \label{fig:chemputer-api-pump-example}
\end{figure}