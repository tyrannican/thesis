The main focus of the re-design was to not repeat the mistakes of the original ChemOS platform.
The new system had to be leaner, more concise, and easy to maintain.
It was decided to mimic a structure similar to that of the Ulduar framework.
This took the form of a series of software modules (\textbf{Executors}) that were interfaceable via a centralised Manager-style \acrshort{api}.
This \acrshort{api} would then be accessible via a central control unit (\textbf{Chempiler}) that would perform platform setup duties such as parsing the graph and input files.
The Chempiler would then execute the platform operations based on the input file given.
A requirement of this re-design was to run the platform using a custom-made programming language to encode chemical syntheses.
A programming language (\textbf{\acrshort{chasm}}) was developed solely for this purpose and is described in Section \ref{sec:chemputer-chasm}.
A class hierarchy diagram is shown in Figure \ref{fig:chemputer-chempiler-class-diagram} highlighting the new structure of the Chemputer system.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.155]{chempiler-class-diagram.jpg}
    \caption{
        Class hierarchy of the new Chempiler system.
        Each type of hardware was split off into its own \textbf{Executioner} which would find the appropriate device and execute the given command for it.
        These Executioners were gathered into a single \textbf{Executor} \acrshort{api} that relayed commands to the appropriate device Executioner.
        All instructions were given as an input file to the Chempiler class which compiled down to a digestible format and executed line-by-line by the Executor.
        The Chempiler class also parsed the graph and created the appropriate device objects based on what information was present in the graph representation.
    }
    \label{fig:chemputer-chempiler-class-diagram}
\end{figure}
    

\subsubsection{Chempiler: Module Executioners}
Each set of hardware devices would be controlled via a module interface (similar to a control driver (Section \ref{sec:ulduar-module-layout})).
However, as the modules in the Ulduar framework represent a single device, this design would not work in this case.
It was decided that instead of representing a single hardware component as a module, all hardware components that shared a common theme would be grouped together under a single module instead.
These modules were named \textbf{Executioners} and each one represented a type of device that shared common functionality.
A list of the Chempiler Executioners are highlighted below:
\begin{itemize}
    \item \textbf{Vacuum Executioner}: Any hardware device that was related to Vacuum operations such as vacuum pumps.
    \item \textbf{Stirrer Executioner}: Any hardware device that was related to stirring and heating operations.
    These include stirring mantles, stirring hotplates, and overhead stirrers.
    \item \textbf{Rotavap Executioner}: Any hardware device that was considered a Rotary Evaporator.
    \item \textbf{Pump Executioner}: Chemputer API pump and valve hardware.
    \item \textbf{Chiller Executioner}: Any hardware device that was related to chilling operations.
\end{itemize}

Each Executioner class received a key-value pair data structure containing the collection of devices which related to that type of device.
This contained the name of the device as the key and the underlying device object as a value.
Each Executioner contained a series of methods that related to an operation of the device (e.g. start stirring, set temperature, etc.).
These methods would receive the name of the requested device and the object associated with it would perform the operation.
An example of this implementation can be seen in Figure \ref{fig:chempiler-module-exectioner-example}.

The Executioners receive a copy of the graph which represents the physical system architecture.
This is not always required but is supplied nonetheless in cases where the graph may be used to determine optimal routes through the system.
This is mainly used for the Pump Executioner which requires this information to route liquids throughout the system plumbing.


\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
    class ChillerExecutioner:
        """Chiller Executioner for the Chempiler

        Args:
            graph (networkx.DiGraph): Graph representaiton of the system
            chillers (Dict): Collection of chiller objects attached to the system.
        """

        def __init__(self, graph: networkx.DiGraph, chillers: Dict):
            self.chillers = chillers
            self.graph = graph
            self.logger = logging.getLogger("main_logger.chiller_executioner_logger")

        def start_chiller(self, chiller_name: str):
            """Start the chiller with the given name

            Args:
                chiller_name (str): Name of the chiller to start
            """

            self.logger.info(f"Starting chiller: {chiller_name}")

            # Get the appropriate chiller object from the collection
            chiller_obj = self.chiller[chiller_name]

            # Start the chilling operation
            chiller_obj.start()

            self.logger.info("Finished.")
    \end{minted}
    \caption{
        Example implementation of a module Executioner for the Chillers.
        Each Executioner receives a copy of graph for the system and a collection of objects associated with the specific type of device in question.
        The Executioners contain methods that receive a device name which is used to find the appropriate object from the collection and execute the requested method.
    }
    \label{fig:chempiler-module-exectioner-example}
\end{figure}

\subsubsection{Chempiler: Global Executor}
The global Executor was similar in design to the Ulduar framework's Manager \acrshort{api}.
It was a global \acrshort{api} that contained all Executioners for the Chempiler system similar to how the Ulduar framework contained the software module control drivers for their respective systems.
The major difference between these however is that Executor did not contain methods that combined operations between modules for example like the Nanobot system.
It was designed to be interfaced with via a single method called \mintinline{python}{execute()} which received a single command string.
This command string would be parsed to determine what class of Executioner the command was pertaining to and would relay the command to the appropriate handler.
These handler methods would split the command down to extract out the arguments such as device name and operational parameters and pass them to the Executioner responsible for that type of device.
The Executioner would then perform the requested operation for the device.

The global Executor was also responsible for parsing the graph data structure to extract out the hardware device objects.
The device objects were collected in a key-value pair data structure in relation the type of hardware they represented.
These were then passed down to their appropriate Executioners.
An example of hardware object extraction is highlighted in Figure \ref{fig:chempiler-executor-dev-extraction}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def get_device_objects(self, device_type: str):
            """Extract out all hardware objects for a given device type from the graph.

            Args:
                device_type (str): Type of device (e.g. PUMPS, VALVES, STIRRERS, etc.)
            """

            # Dictionary to hold all devices with name as key and device object as value
            device_dict = {}
            
            # Get all Chemputer Pump objects from the graph
            if device_type == "PUMPS":
                # Iterate through all nodes in the graph
                for node in self.graph.nodes():
                    # Current node is a Chemputer Pump
                    if self.graph.node[node]["class"] == "chemputer_pump":
                        # Obtain the device object
                        obj = self.graph.node[node]["obj"]

                        # Add to device dictionary
                        device_dict[obj.name] = obj

                # Return devices pertaining to this type of device.
                return device_dict
            
            # This is repeated for each type of device that has an associated Executioner.
    \end{minted}
    \caption{
        Hardware object extraction from the graph data structure.
        The graph is iterated over to find all device objects that relate to the type of device requested.
        These are stored in a key-value pair structure and given to the appropriate Executioner.
    }
    \label{fig:chempiler-executor-dev-extraction}
\end{figure}

Overall the global Executor was designed to be the heart of the system, similar to the Ulduar framework's Manager \acrshort{api}, that would facilitate the execution of operations for the Chempiler system.
Commands would be fed to the Executor from the top-level \textbf{Chempiler} class which would then be relayed to the appropriate device Executioners.

\subsubsection{Chempiler: Entry Point}
To parse the input files and prepare the system for Executor, a dedicated entry point class was created: the \mintinline{python}{Chempiler} class.
This was responsible for creating the graph data structure from a supplied input file and populating it with device objects from the parameters defined within.
It's main responsibility was to use the \acrshort{chasm} parser to parse an input \acrshort{chasm} file.
This would "compile" the contents into a series of commands that were added to a FIFO (First-In-First Out) data structure.
These commands would be popped from the queue and sent to the global Executor which would execute the command (Figure \ref{fig:chemputer-chempiler-command-queue}).
This class was designed to be as simple as possible with its only functionality to populate the graph, parse input \acrshort{chasm} files, and relay compiled commands to the Executor.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def run_platform(self, chasm_file: str):
            """Execute the chempiler system.
            Parses a ChASM file to compile the commands which are added to a Queue.
            Each command in the queue is passed to the Executor until the queeu is empty.

            Args:
                chasm_file (str): Path to the ChASM file
            """

            # Read and compile the ChASM file to extract commands
            with open(chasm_file) as fd:
                chasm = fd.read()
                self.command_queue = parser.parse(chasm)

            # Run for as long as the queue is not empty
            while not self.command_queue.empty():
                # Get the command at the head of the queue and execute.
                command = self.command_queue.pop()
                self.executor.execute(command)
    \end{minted}
    \caption{
        Method which runs the Chempiler engine.
        A \acrshort{chasm} file is read in and compiled by the \acrshort{chasm} parser into a command queue.
        These commands are then given to the Executor to be executed by the Chempiler engine.
    }
    \label{fig:chemputer-chempiler-command-queue}
\end{figure}

\subsubsection{Summary}
The new Chempiler architecture was a vast improvement from the previous ChemOS system.
It was designed to be modular with dedicated Executioners for each type of device hosted on the hardware.
These were interacted with via a global Executor which received commands from the top-level Chempiler entry-point.
This new architecture was leaner, well-documented, and much more maintainable than the previous system.
Unnecessary complexities were stripped away in place of smaller concise units that somewhat resembled the Ulduar framework design.
The one complexity that was required however was a dedicated scripting language to run the platform with as it was a key requirement of the project specification.
For this requirement, \acrshort{chasm} was created to act as a simplistic scripting language that when parsed, would generate a series of commands that the Chempiler engine could understand.
This is discussed in more detail in Section \ref{sec:chemputer-chasm}.
