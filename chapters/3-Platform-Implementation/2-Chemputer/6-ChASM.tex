The Chemical Assembly Language (\textbf{\acrshort{chasm}}) was designed as a basic scripting language that was to act as a layer on top of the Chempiler execution.
As a simple language specification it supports the assignment of variables, re-useable functions, comments, and basic loop functionality.
More complicated facets of higher-level programming languages such as switch statements, pointers, etc. were not pursued.
The language followed a basic syntax and grammar which is highlighted in Backus-Naur Form \cite{s136} in Figure \ref{fig:chempiler-chasm-syntax}.
\\
\begin{figure}[H]
    \centering
    \mintinline{text}{<instruction> ::= <PS_TOK> <OPCODE> "(" <ARGS> ")" ";" | <PS_TOK> <OPCODE> "(" ")" ";"}
    \caption{
        Overview of the general grammar and syntax of the \acrshort{chasm} language using Backus-Naur Form.
    }
    \label{fig:chempiler-chasm-syntax}
\end{figure}

\mintinline{text}{<OPCODE>} refers to a keyword associated with a Chempiler module operation (e.g. \mintinline{text}{MOVE, PUMP, SET_TEMP} etc.).
\mintinline{text}{<ARGS>} refers to the argument list specific to the command
\mintinline{text}{<PS_TOK>} refers to both the upper-case letters \textbf{P} and \textbf{S} which relate to \textbf{Parallel} and \textbf{Sequential} instruction respectively.
This relates directly to the mode of operation for the instruction.
Sequential instructions are carried out in sequence as they are reached whereas Parallel instructions are executed concurrently using their own dedicated thread for the operation.
If a parallel instruction is in operation, the next sequential operation will wait until all currently running parallel operations have finished.
In practice however, the parallel operations were never realised due to safety issues with concurrently running pieces of hardware.
To save development time and debugging issues with concurrent code, support was added for these operations in the Chempiler but were never used.

To improve code readability and to encourage re-use, support for variables were added.
As this was a basic implementation, support was only added for integers, floats, and strings.
Syntax for variable definition can be seen in Figure \ref{fig:chempiler-chasm-vars}.
\\
\begin{figure}[H]
    \centering
    \mintinline{text}{<variable> ::= "DEF" <NAME> "=" <VALUE> ";"}
    \caption{
        General syntax for the definition of variables in the \acrshort{chasm} language.
    }
    \label{fig:chempiler-chasm-vars}
\end{figure}

Variable re-assignment was not a feature implemented in \acrshort{chasm}.
As this language was designed to be compiled once by the Chempiler, with all logic happening within.
It was not deemed feasible to implement the ability to re-assign values.

As many operations for the Chempiler are repetitive in nature, the ability to define functions was deemed an appropriate feature to implement.
The body of the function allows for the definition of new instructions or calling of other functions however variables and functions must be defined before being used within the function body.
The syntax for function definitions is shown in Figure \ref{fig:chempiler-chasm-func}.
\\
\begin{figure}[H]
    \centering
    \begin{minted}{text}
        <function> ::= "DEF" <NAME> "(" <ARGS> ")" "{" <BODY> "}" ";"
                    |  "DEF" <NAME> "(" ")" "{" <BODY> "}" ";"
    \end{minted}
    \caption{
        General syntax for the definition of a function in the \acrshort{chasm} language.
    }
    \label{fig:chempiler-chasm-func}
\end{figure}

To repeat any operations or function calls, a loop functionality was implemented.
It was decided to implement a basic \mintinline{text}{for} loop where the contents of the loop (the body) were executed X number of times.
The same rules that were defined for the body of a function also applied here.
The syntax is highlighted in Figure \ref{fig:chempiler-chasm-loop}.
\\
\begin{figure}[H]
    \centering
    \begin{minted}{text}
                <loop> ::= "FOR" "(" <INTEGER> ")" "{" <BODY> "}" ";"
    \end{minted}
    \caption{
        General syntax for the definition of a \mintinline{text}{for}-style loop in the \acrshort{chasm} language.
    }
    \label{fig:chempiler-chasm-loop}
\end{figure}

Once all variables and functions were defined, the entire script would be executed in a block known as \mintinline{text}{MAIN}, similar to other programming languages.
The execution of the program would commence with calls to functions and instructions happening here.
This block is the only block that would be executed.
Syntax for the \mintinline{text}{MAIN} definition is shown in Figure \ref{fig:chempiler-chasm-main}.
\\

\begin{figure}[H]
    \centering
    \mintinline{text}{<script> ::= "MAIN" "{" <BODY> "}" }
    \caption{
        General syntax of the \mintinline{text}{MAIN} function definition in the \acrshort{chasm} language.
        This is the only block of code that is executed.
    }
    \label{fig:chempiler-chasm-main}
\end{figure}

A colleague working with the Chempiler system developed a simple parser using PLY, a Python implementation of Lex and YACC. \cite{ply}
This would parse the \acrshort{chasm} file, performing lexical analysis to compile the \acrshort{chasm} down to single instructions.
To support commenting in the \acrshort{chasm} language, any content between an octothorpe (\mintinline{text}{#}) and the end of a line would be ignored.
This allowed for in-line comments and line-spanning comments.
Once lexical analysis was complete, all functions, variables, and loops were replaced and copied appropriately so that the final result was a list containing only individual \acrshort{chasm} instructions.
These instructions were then executed by the Chempiler engine one by one.
An example of a \acrshort{chasm} script snippet is shown in Figure \ref{fig:chempiler-chasm-example} with the full script available in Appendix \ref{apx:chemputer-chempiler-chasm-example}.
The full specification of the \acrshort{chasm} language is available in the supporting publication for this section.

\begin{figure}[H]
    \begin{minted}{text}
                    # ChASM function to perform reflux on a vessel
                    DEF reflux_and_wait(temp, time, end_temp, vessel) {
                        # Set the temperature and begin heating
                        S SET_TEMP(vessel, temp);
                        S START_HEAT(vessel);

                        # Wait for the temperature
                        S STIRRER_WAIT_FOR_TEMP(vessel);

                        # Wait for reflux time
                        S WAIT(time);

                        # Cool down
                        S STOP_HEAT(vessel);
                        S SET_TEMP(vessel, end_temp);
                        S STIRRER_WAIT_FOR_TEMP(vessel);
                    }
    \end{minted}
    \caption{
        Example of \acrshort{chasm} function to perform reflux on a reaction vessel.
        Function is re-useable and will implant the individual instructions within when being compiled.
    }
    \label{fig:chempiler-chasm-example}
\end{figure}