The Watcher section of this project was a simple program that was tasked with watching the currently running generation of experiments and detecting the presence of raw, unprocessed UV-Vis data.
This data was generated by the Spectrometer software module which deposited the data in a parsable file format (\acrshort{json} file).
The raw data was then parsed and passed to the Watcher's \mintinline{python}{calculate_fitness()} method which would call on one of many fitness functions that would determine the quality of the experiment.
Once all fitness values for an experiment were calculated, they were collated into a single file and placed at the root of the generation's folder structure which would then be read by the Creator to generate new values for the experiments.
This process was repeated for the total number of generations determined by the user.

To assess the quality of the data produced by the platform, a series of fitness functions were developed.
These functions would parse in the raw UV-Vis data and based on a set of criteria, assign a "score" to the experiment.
The higher the score for the experiment, the closer it was to the desired target for that specific target.
Each target shape that the platform was designed to optimise (spheres, rods, and octahedrons) had its own dedicated function which itself contained a set of criteria and penalties associated with each.

\subsubsection{Fitness Calculation: Gold Nanospheres \cite{nanobotsi}}
The target for this shape was a single peak spectrum with the peak maximum at 553 nm.
A bench synthesis of the chosen particle was created to generate the ideal objective spectrum that would be used within the calculation.
There were two parameters that were considered for this fitness calculation: The position of the \(\lambda max\) value of the experimental data vs the objective and the absorbance value at which the experimental data intersected the objective peak.
Normal distributions for both the experimental and objective \(\lambda max\) value were generated.
The observed value in the experimental data (i.e. the Target) gave the initial value in the range of 0 and 1 which handled the peak position component.
To deal with the yield of the experiment (i.e. the absorbance level) was handled by the second component Abs\textsubscript{\(\chi\)}.
By adding the absorbance value at the target peak position (553 nm) of the experimental data to the first component yielded the spectra's fitness value, capping out at a maximum fitness value of 2.6 which accounted for all relevant spectra features for this shape target.

\subsubsection{Fitness Calculation: Gold Nanorods \cite{nanobotsi}}
To calculate the fitness for this shape, the UV-Vis spectra were divided into three distinct regions: \textbf{A} (490-560 nm), \textbf{B} (580-640 nm), and \textbf{C} (700-800 nm).
When rods of the ideal length would begin to grow (40-60 nm in length), region \textbf{A} would grow in absorbance slightly, region \textbf{B} would minimise in absorbance, and \textbf{C} would grow substantially.
To calculate the fitness from this regime, the area beneath each spectral region (A) would be calculated and a simple calculation was used to calculate a single value for fitness (Equation \ref{eq:nanobot-fitness-rods})
This calculation resulted in many samples achieving a value of 5 or greater which provided the optimised synthesis for this shape that would go on to act as "seeds" for the stage of the project.

\begin{equation}
    A\textsubscript{\textbf{A}} / (A\textsubscript{\textbf{B}} + A\textsubscript{\textbf{A}})
    \label{eq:nanobot-fitness-rods}
\end{equation}

\subsubsection{Fitness Calculation: Gold Octahedra \cite{nanobotsi}}
The fitness calculation used in the synthesis of gold octahedra particles was similar to the calculation for gold nanorods.
The target for this shape was a single peak spectrum that used the rods from the previous synthesis as seeds.
The target peak for this regime was 580 nm with all spectral features beyond 640 nm being sub-optimal.
Two distinct regions were therefore constructed: region \textbf{X} (560-640 nm) and region \textbf{Y} (640-740 nm).
The area of both regions was calculated (A\textsubscript{x} and A\textsubscript{y} respectively) and a simple calculation (Equation \ref{eq:nanobot-octa-eq}) was used to obtain a single value for fitness.
This encouraged the system to generate higher yielding samples with higher fitness values which optimised for a specific \(\lambda max \) value.

\begin{equation}
    A\textsubscript{x} / A\textsubscript{y}
    \label{eq:nanobot-octa-eq}
\end{equation}

Each fitness calculation had a series of penalties associated with them that were used to "punish" undesirable spectra.
Using this strategy, the system was encouraged to generate experiments that minimised the penalty and achieved maximum fitness value for the desired shape.
Each fitness calculation was designed in such a way that they could be inserted into the Watcher program and be called upon depending on the shape the user wished to synthesise.
This approach allowed for the extension of the system's capabilities to synthesise any shape of \acrshort{aunp} provided the target spectral features are known.
Three were selected for this project but the option to add more is supported and actively encouraged.
The source code for the Watcher program and the associated fitness calculations can be found in Appendix \ref{apx:nanobot-watcher} and \ref{apx:nanobot-fitness} respectively.
