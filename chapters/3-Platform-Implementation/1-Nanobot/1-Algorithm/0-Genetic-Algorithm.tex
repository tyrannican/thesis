The algorithm used for the Nanobot system was a standard optimisation \acrlong{ga} (an evolutionary algorithm) \cite{ga-standard} written by a previous colleague.
The \acrshort{ga} would initially start out with a set of random values and evaluated the results based on their '\emph{fitness factor}'.
The fitness factor chosen for these experiments was the measured data in the UV-Vis spectra of the reaction.
Dependent on the values of the fitness factor, the system would begin a process of selection and recombination.
This involves the process of selecting successful experiments to continue onwards whilst combining parameters from other solutions to create a new set.
This process is continued until a final condition is met and the results do not improve any further.
The process of the \acrshort{ga} can be split into the following steps:

\begin{itemize}
    \item \textbf{Initialisation}: An initial set of randomly generated parameters.
    All values generated are in the range of 0-1 and are normalised to mL.
    \item \textbf{Evaluation}: Analyse the fitness results for each experiment.
    \item \textbf{Selection}: Decides on what solutions (values) continue onto the next generation.
    This imposes a "survival of the fittest" regime on the values.
    Four survivors are selected to survive from one generation to the next.
    Each value (genome) consists of multiple values (genes) which are used in subsequent computations.
    These survivors are sampled with a probability function proportional to their fitness function.
    A "SoftMax" transformation is performed on these values, ensuring that they will never be selected twice for survival.
    \item \textbf{Recombination}: The combination of two or more of the selected "parent" solutions to create new and potentially better "offspring".
    This is achieved through the "one-point" method which takes one point from each of the parent's formulation and splits it.
    These parts are then combined to generate a new formulation for the offspring.
    This can be summarised as the following:
    \begin{itemize}
        \item Two parents are sampled with a probability proportional to their fitness functions after a SoftMax transformation.
        \item A "child" is created from these two parent genomes using a crossover function.
        The child gene was computed for each gene in the genome using the following set of rules:
        \begin{itemize}
            \item 25\% of the time, the child inherits entirely from Parent A
            \item 25\% of the time, the child inherits entirely from Parent B
            \item 50\% of the time, the child genome is a weighted average of Parent A and Parent B genome.
            This mixed weight was sampled randomly and uniformly in the range of [0, 1].
        \end{itemize}
    \end{itemize}
    \item \textbf{Mutation}: Randomly modifies a solution whilst recombination occurs.
    One or more traits of an individual are modified, leading to a "random walk" in the direction of a candidate solution.
    This mutation is applied according to the following rules:
    \begin{itemize}
        \item Each gene has a 30\% chance of being mutated.
        \item If a gene is mutated, its value is shifted by adding a number sample from a Gaussian probability distribution with a standard deviation of 0.1
    \end{itemize}
    \item \textbf{Replacement}: Any offspring created through this process replace their parental counterparts.
    \item \textbf{Termination}: End of the experiment
\end{itemize}

This process of \textbf{Evaluation} through to \textbf{Replacement} cycled continuously, creating new generations of experiments each cycle.
This process was repeated numerous times until the total number of generations defined by the user had been satisfied (Figure \ref{fig:nanobot-ga-cycle}).

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.25]{nanobot-ga-cycle.jpg}
    \caption{
        \acrshort{ga} cycle diagram.
        The process of \textbf{Evaluation} through to \textbf{Replacement} happens continuously until the total number of generations have been created.
        The \textbf{Initialisation} and \textbf{Termination} steps happen only once.
    }
    \label{fig:nanobot-ga-cycle}
\end{figure}

This entire process was packaged into a Python class that could be imported into the \textbf{Creator} program of the system.
That way, the creator could invoke the \acrshort{ga} to create and update the generations as and when required.
A full description of the \acrlong{ga} software can be found in Appendix \ref{apx:nanobot-ga}.