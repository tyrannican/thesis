\subsubsection{Core Driver}
As stated previously, the \textbf{PyCont} library's \mintinline{python}{MultiPumpController} class would form the basis of the core driver of this module.
The core driver was simple and only required a configuration to instantiate the \mintinline{python}{MultiPumpController} object.
Helper methods were defined to access a given \mintinline{python}{C3000Controller} object from the multi-pump controller (Figure \ref{fig:nanobot-pumps-core-driver}).
Almost all of the initialisation of the Triconts is handled by the PyCont library's \mintinline{python}{MultiPumpController} class so not much else is included apart from resetting the position of the hardware back to zero.

The reason for creating a core driver instead of just using the \mintinline{python}{MultiPumpContoller} directly from PyCont is one of consistency.
The ideal way for adding new software modules into systems that use the Ulduar framework is to have this \mintinline{text}{Core Driver -> Control Driver} structure.
Deviations from this structure, although seeming sensical, break this consistency and is advised against for readability and code hygiene reasons.
In short, this is not a programmatic issue; it is one on readability and consistency.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class TricontCoreDriver:
            """Creates the core driver for the pump module.

            Args:
                config (dict): PyCont configuration for a MultiPumpController.
            """

            def __init__(self, config: dict):

                # Create the controller and initialise all pumps with parameters
                self.controller = MultiPumpController(config)
                self.controller.smart_initialise()
                
                self.config = config
                self._reset_position()

            def _reset_position(self):
                """Resets the position of all pumps.
                Moves the pumps to a volume 0.
                """

                # Applys the method `go_to_volume` to all pumps in the controller.
                self.controller.apply_command_to_all_pumps("go_to_volume", 0)

            def get_pump_object(self, name: str) -> C3000Controller:
                """Gets a pump object from the PyCont Controller.

                Args:
                    name (str): Name of the pump to get.
                
                Returns:
                    C3000Controller: Pump object.
                
                Raises:
                    UndefinedPumpError: Pump does not exists in the Controller
                """

                # Attempt to get the pump object
                try:
                    return getattr(self.controller, pump_name)

                # Not present, raise an error
                except AttributeError:
                    raise UndefinedPumpError(f"No pump named {name} in the PyCont controller!")
    \end{minted}
    \caption{
        Implementation of the core driver of the Pump software module.
        Most initialisation is handled by the \mintinline{python}{MultiPumpController} of PyCont so not much else is included.
        Helpers to reset the pumps and obtain a pump object are supplied for convenience.
    }
    \label{fig:nanobot-pumps-core-driver}
\end{figure}

\subsubsection{Control Driver}
The control driver for this module was designed as a simple wrapper around the functionality already defined in the PyCont library.
Using the helpers defined in the core driver, methods were created to allow the user to supply the name of a pump as a string and supply a volume in mL.
To make this module as generic as possible to promote re-use in other platforms, wrappers were created that mimicked the behaviour of the PyCont library dispense functions.
Three methods wrappers were created to mimic this behaviour:
\begin{itemize}
    \item \textbf{Pump}: Draw liquid into the syringe from a supplied input valve position.
    \item \textbf{Deliver}: Deliver loaded volume in the syringe to an output valve position.
    \item \textbf{Transfer}: Combination of the above, drawing liquid in through an input valve and dispensing it out through an output valve.
\end{itemize}
These wrappers would simply mirror the call on the \mintinline{python}{C3000Controller} object associated with a pump, obtained using the helper method \mintinline{python}{get_pump_object()} defined in the core driver (e.g. Figure \ref{fig:nanobot-pumps-transfer}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def transfer(
            self,
            name: str,
            volume: float = 0,
            in_valve: Optional[str] = "I",
            out_valve: Optional[str] = "O"
        ):
            """Mirrors the transfer method defined in the PyCont library.
            Moves liquid from one valve position to another.

            Args:
                name (str): Name of the target pump
                volume (float): Volume to dispense
                in_valve (Optional[str]): Input valve. Defaults to "I".
                out_valve (Optional[str]): Output valve. Defaults to "O"
            """

            # Obtain the C3000Controller object from the PyCont controller
            pump_obj = self.get_pump_object(name)

            # Transfer the liquid
            pump.transfer(volume, in_valve, out_valve)

    \end{minted}
    \caption{
        Transfer method in the pump module's control driver.
        Mirrors the capability of PyCont, offering the option to obtain a pump object using a string for convenience.
    }
    \label{fig:nanobot-pumps-transfer}
\end{figure}

For convenience, a method was written that would accept a collection of pump names and volumes that would represent reagents and allows them to be dispensed in a sequential order.
This beneficial method would allow the user to simply define what reagents would be dispensed for a given experiment in one go and dispense them with a single call to the driver.
The reagent representation could take on two forms: a \textbf{Tuple} object that would contain the reagent information for the pump (Name, Volume, In Valve, Out Valve e.g. \mintinline{python}{("acid", 5.0, "I", "O")}) or a dedicated \textbf{Reagent} data class (Figure \ref{fig:nanobot-pumps-reagent-class}) that contained the same information and could be compiled down to a tuple for use.
The \mintinline{python}{dispense_reagents()} method is detailed in Figure \ref{fig:nanobot-pumps-dispense_reagents}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class Reagent:
            """Helper class to represent a Reagent to dispensed by the system.

            Args:
                name (str): Name of the reagent, matching pump name in PyCont config
                volume (float): Volume to be dispensed
                in_valve (Optional[str]): Input valve. Defaults to "I"
                out_valve (Optional[str]): Output valve. Defaults to "O".
            """
            def __init__(
                self,
                name: str,
                volume: float,
                in_valve: Optional[str] = "I",
                out_valve: Optional[str] = "O"
            ):
                self.name = name
                self.volume = volume
                self.in_valve = in_valve
                self.out_valve = out_valve
            
            def as_tuple(self) -> Tuple[str, float, str, str]:
                """Represents the reagent as a tuple.

                Returns:
                    Tuple[str, float, str, str]: Reagent representation as a tuple.
                """

                return (self.name, self.volume, self.in_valve, self.out_valve)
    \end{minted}
    \caption{
        Helper class to aid in the representation of a Reagent to be dispensed by the system.
        Reagents take the name of the reagent (matching the pump name associated with it), the volume to dispense, and the input and output valves for the operation.
        This is offered for convenience and is a cleaner representation as compared to a raw Tuple.
    }
    \label{fig:nanobot-pumps-reagent-class}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def dispense_reagents(self, *reagents: List[Union[Tuple[str, float, str, str], Reagent]]):
            """Dispenses a collection of reagents from each of their respective pumps.

            Args:
                reagents (List[Union[Tuple[str, float, str, str], Reagent]]):
                    List of reagents wither as a tuple or as a Reagent class
            """

            # Iterate through all reagents given
            for reagent in reagents:
                # If is a Reagent class, convert to a tuple
                if isinstance(reagent, Reagent):
                    reagent = reagent.as_tuple()
                
                # Trnasfer the reagent, passing in all information available
                self.transfer(*reagent)
    \end{minted}
    \caption{
        Method to dispense a series of Reagents in a single call to the driver.
        The user would either create a series of tuples or Reagent objects that would represent the dispensing conditions for their given experiment and pass it to the method.
        This will then dispense each of the reagents with the given volumes in the order that they were supplied.
        Increases efficiency and leads to cleaner code.
    }
    \label{fig:nanobot-pumps-dispense_reagents}
\end{figure}

\subsubsection{Summary}
The PyCont pump module's design and implementation are to help the user streamline the most effective uses of the underlying PyCont library.
The wrapper methods for easily interfacing with the PyCont controller and the helpers to bulk dispense reagents offer much flexibility when using this module as part of the Ulduar framework.
A basic version is detailed in this section and is open to extension when the need arises.
The goal was to develop a simplistic interface in which a user could perform the most basic actions of dispensing liquids but can be modified and extended to offer much more flexibility available through the underlying PyCont library.
A full definition of the PyCont pump module's control driver is available in Appendix \ref{apx:nanobot-tricont-control-driver} as the core driver has been detailed in Figure \ref{fig:nanobot-pumps-core-driver}.

One downside of this module however is the financial barrier to entry.
Tricont C-series pumps can cost upwards of £700 from experience and when using several as part of this platform, can come at a major expense.
This module was created as these devices were a readily available option at the time but the point can be made that the cost of using such devices can severely damage the uptake of this module.
However, the alternative of using highly accurate peristaltic pumps via the Commanduino module is still a valid choice and comes at a fraction of the cost per unit (around £40 from experience).