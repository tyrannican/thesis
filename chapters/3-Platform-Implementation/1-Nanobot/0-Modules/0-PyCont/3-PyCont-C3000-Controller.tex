The \mintinline{python}{C3000Controller} class was an \acrshort{api} designed to represent a single Tricont pump.
The main focus of the class was to provide an API to the user which would allow them to interact with the device and perform operations expected of a syringe pump (e.g. setting the valve position, moving the syringe plunger etc.).
Full source code for this API can be found on the library's GitHub page. \cite{pycont}

The communication between the host machine and the Tricont pumps was handled by custom-built serial communication class constructed purely for this project.
The \mintinline{python}{PumpIO} class accepted a \mintinline{python}{DTInstructionPacket} from the controller and sent it to the device whilst passing on any received response as a \mintinline{python}{DTStatus} packet to the decoder.
As serial communication has been detailed previously in this document, it will not be covered here.

To instantiate a \mintinline{python}{C3000Controller} object, a series of configuration values has to be supplied:
\begin{itemize}
    \item \textbf{Name}: Name of the Tricont device.
    \item \textbf{Address}: Address of the Tricont device.
    \item \textbf{Total Volume}: Total volume of the attached syringe.
    \item \textbf{Microstep Mode}: Stepper mode for the internal stepper motor.
    \item \textbf{Top Velocity}: Top speed of the Tricont device.
    \item \textbf{Initial Valve Position}: Initial position of the valve upon initialisation.
\end{itemize}

These parameters would be used to calculate internally used values such as motor steps per volume etc., and to create the protocol that would be used to forge packets to be sent to the device.
A series on initialisation packets is sent on first use of the device to ensure it is ready and in the proper configuration (Figure \ref{fig:nanobot-pycont-c3000-init}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def initialise(
            self, valve_position: str = None, max_repeat: int = MAX_REPEATS, secure: bool = True
        ) -> bool:
            """Initialises a C3000 Tricont pump.
            Attempts to send initialisation packets to and from the device.
            If no response is reached after X times, raises an error

            Args:
                valve_position (str): Position to initialise the valve to
                max_repeat (int): Number of times to attempt initialisation.
                secure (bool): Wait until operation completed.
            """

            # Set valve position to default value in the class.
            if valve_position is None:
                valve_position = self.initialise_valve_position

            # Attempt to construct and send packet to Tricont.
            for _ in range(max_repeat):
                self.initialise_valve_only()
                self.set_valve_position(valve_position, secure=secure)
                self.initialise_no_valve()
            
            # Device has initialised successfully, return
            if self.is_initialised():
                return True
            
            # Error in communicating with Tricont.
            raise ControllerRepeatedError(f"Repeated error from pump: {self.name}")

        def initialise_valve_only(self, operand: str = "0,0", wait: bool = True):
            """Method for constructing the packet to initialise the Tricont with the valve only.

            Args:
                operand (str): Argument required to be passed to the packet
                wait (bool): Wait until the operation is successful.
            """

            # Construct packet
            packet = self._protocol.forge_initialise_valve_only_packet(operand)

            # Send packet
            self.write_and_read_from_pump(packet)

            # Wait if required to do so
            if wait:
                self.wait_until_idle()
    \end{minted}
    \caption{
        Method demonstrating the process of initialising a C3000 Tricont pump with the \mintinline{python}{C3000Controller} API.
        The initialisation will construct the packets for initialising the valves and send them to the device using the \mintinline{python}{PumpIO} class.
    }
    \label{fig:nanobot-pycont-c3000-init}
\end{figure}

The Tricont \acrshort{api} was ready to be interacted with once this initialisation had been completed.
The \acrshort{api} was designed not just to construct the packets required to fulfil a request but to offer in-built checks and safety guarantees.
These would prevent any mistaken requests from going through and potentially leading to liquid overflows or even dangerous situations (toxic chemical leakage).
For example, a request to draw liquid into a syringe that exceeds the maximum volume would not go through at all (Figure \ref{fig:nanobot-pycont-c3000-pump}).
These safety checks are necessary additions to the \acrshort{api}.
When working in a research lab, corrosive and/or toxic materials may be handled and if these checks are not added, this may lead to potentially fatal consequences.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def pump(
            self,
            volume_ml: float,
            from_valve: str,
            speed_in: int = None,
            wait: bool = False,
            secure: bool = True
        ) -> bool:
            """Draw liquid into the syringe.
            Checks if the volume is pumpable before doing anything else.
            Will forge and send packets to initiate the sequence.

            Args:
                volume_ml (float): Voluem to draw in.
                from_valve (str): Valve to draw the liquid in from.
                speed_in (int): SPeed to draw the liquid in at.
                wait (bool): Wait until the operation is complete
                secure (bool): Wait until the operation complete.

            Returns:
                bool: Success of the operation
            """

            # Check volume is valid to draw in
            if self._is_volume_pumpable(volume_ml):
                # Set the speed accordingly
                if speed_in is not None:
                    self.set_top_velocity(speed_in, secure=secure)
                else:
                    self.ensure_default_top_velocity(secure=secure)

                # Set the valve position
                if from_valve is not None:
                    self.set_valve_position(from_valve, secure=secure)

                # Calculate the steps to move
                steps_to_pump = self.volume_to_step(volume_ml)

                # Forge and send the packet
                packet = self._protocol.forge_pump_packet(steps_to_pump)
                self.write_and_read_from_pump(packet)

                # Wait if required
                if wait:
                    self.wait_until_idle()
                
                # Successful operation
                return True

            # Unsuccessful operation
            return False
    \end{minted}
    \caption{
        Method demonstrating the safety checks built into the Tricont API.
        Here a check is in place to determine if a requested volume is able to be drawn into the syringe.
        If the volume is more than what the pump can do for the syringe, then the request is ignored.
        These simple checks are necessary from a safety perspective.
    }
    \label{fig:nanobot-pycont-c3000-pump}
\end{figure}
