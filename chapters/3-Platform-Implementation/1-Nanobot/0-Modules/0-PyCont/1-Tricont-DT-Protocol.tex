The Tricont pumps offer three separate protocols for communication (\textbf{OEM}, \textbf{DT} (Data Terminal), and \textbf{CAN} (Controller Area Network)).
As all that was required of the PyCont library was to communicate commands via Python strings, the DT protocol was selected as this was designed for basic use via terminal programs using ASCII characters.

The DT protocol for Triconts was split into two types of block: \textbf{Command Block} for sending commands to the pump device and the \textbf{Answer Block} for receiving status replies from the pump.
The \textbf{Command Block} was split into four sections which would form the command sent to the pump device:

\begin{enumerate}
    \item \textbf{Start Character} (\mintinline{text}{"/"}): This indicates the beginning of the command block.
    \item \textbf{Pump Address} (\mintinline{text}{0-9, A-F}): The address of the pump which would be the ASCII characters specific to the pump.
    \item \textbf{Data Block}: The command to be sent to the pump (See Figure \ref{fig:nanobot-tricont-cmd-example}).
    \item \textbf{End Character} (\mintinline{text}{"\r"}): Indicates the end of the command block.
\end{enumerate}

The DT protocol expects that a command that the user wishes to send (e.g. Figure \ref{fig:nanobot-tricont-cmd-example}) would have the appropriate characters appended and prepended to the message before being sent to the pump device addressed by the command.
This is one of the primary reasons PyCont was developed; to automate this entire process and to provide methods common to certain actions.
An example of a DT protocol command block can be seen in Figure \ref{fig:nanobot-tricont-dt-message}.

\begin{figure}[H]
    \[\textbf{[/] [B] [IA3000OA0R] [\textbackslash r}]\]
    \caption{
        DT protocol command block example, separated by square brackets for clarity.
        The command described in Figure \ref{fig:nanobot-tricont-cmd-example} has the DT protocol characters added to it.
        The message is prepended with the start character (\mintinline{text}{"/"}) and the pump's address (\mintinline{text}{B}).
        The command then has a carriage return appended to the end and the message would then meet the standard expected of a DT protocol command block.
    }
    \label{fig:nanobot-tricont-dt-message}
\end{figure}

The \textbf{Answer Block} consists of six sections which would detail any response that was received from a pump device after a command had been sent:

\begin{enumerate}
    \item \textbf{Start Character} (\mintinline{text}{"/"}): Indicates the beginning of the response.
    \item \textbf{Master Address} (\mintinline{text}{"0"}): Master address of the host system which is always 0.
    \item \textbf{Status Character}: The status of the pump and any error code raised.
    \item \textbf{Data Block} (optional): Response for any Query command that has been received.
    \item \textbf{ETX} (\mintinline{text}{^C}): Indicates the end of the command string
    \item \textbf{End Character} (\mintinline{text}{"\r\n"}): Indicates the end of the answer block.
\end{enumerate}

When the Tricont pump receives a command block, the pump attempts to execute the command and, once complete, will construct an appropriate response to return back to the user depending on the success of the operation (Figure \ref{fig:nanobot-tricont-dt-response}).
As these hardware devices can sometimes fail, it is imperative that the user is made aware of any potential issues that arise from the device.
These response codes are detailed in full in the software manual for the device \cite{tricont-software-manual}.

\begin{figure}[H]
    \[\textbf{[/] [0] [k] [\textbackslash r\textbackslash n]}\]
    \caption{
        DT protocol answer block example, separated with square brackets for clarity.
        Response begins with the start character and the master address of the system.
        The status code \mintinline{text}{k} is returned, which indicates an illegal plunger move, and is terminated with the CRLF characters (carriage return, line feed).
    }
    \label{fig:nanobot-tricont-dt-response}
\end{figure}

With the DT protocol specification, alongside the command list and error codes, the PyCont library was developed to automate this process of constructing commands and parsing responses to simplify the process of communicating with Tricont pumps.
It was decided that three features would be required in PyCont to achieve this goal:

\begin{itemize}
    \item A representation of the DT protocol in Python.
    \item A command builder that would construct and encode message to be sent and decode any received response.
    \item A controller class that would implement operations of the device (e.g. transfer liquids, set valve positions, etc.) with the ability to concurrently interact with multiple pumps.
\end{itemize}

These three features would work together in allowing the user to make a request (e.g. \mintinline{python}{transfer(liquid_volume, in_valve, out_valve)}) that would construct the appropriate command in the DT protocol format and would be sent over serial communication to the pump and decode any response received.
An overview of the design can be seen in Figure \ref{fig:nanobot-pycont-design}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.2]{pycont-overview.jpg}
    \caption{
        High-level overview of the design of the PyCont library.
        Three classes to represent different sections of the DT Protocol were created: \textbf{DTCommand} to represent a raw command to be treated, \textbf{DTPacket} to represent the complete command to be sent to the device, and the \textbf{DTStatus} to represent an answer block received from the device.
        These were utilised by the \textbf{PumpIO} class which was responsible for the serial communication to and from the device.
        This allowed for the construction of the requested command packets to be sent to the device and decoded any response that was received.
        This was used by the \textbf{C3000Controller} class which offered high-level methods to perform specific operations that an individual C3000 pump would allow for.
        These methods would call upon the IO class to construct and send the packets.
        Finally, a \textbf{MultiPumpController} was created to allow for multiple pumps to used alongside one another.
    }
    \label{fig:nanobot-pycont-design}
\end{figure}