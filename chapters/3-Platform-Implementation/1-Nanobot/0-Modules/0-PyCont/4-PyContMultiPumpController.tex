With an in-depth \acrshort{api} developed for single Tricont pumps, it was decided to implement a class which allows the user to control multiple C3000 controllers at once.
Rarely are single Tricont pumps used exclusively so allowing a user to control multiple at the same time was considered an essential feature to add.
This \mintinline{python}{MultiPumpController} class accepted a configuration file, similar to Commanduino (Section \ref{sec:act-command-manager}), that contained all the necessary information for each pump as well as default values and IO configurations (Figure \ref{fig:nanobot-pycont-multi-config}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{json}
                                    {
                                        "io": {
                                            "port": "/dev/ttyUSB0",
                                            "baudrate": 9600,
                                            "timeout": 1
                                        },
                                        "default": {
                                            "volume": 5,
                                            "micro_step_mode": 2,
                                            "top_velocity": 8000,
                                            "initialise_valve_position": "I"
                                        },
                                        "pumps": {
                                            "reagent1": {
                                                "switch": "0",
                                                "volume": 2.5
                                            },
                                            "reagent2": {
                                                "switch": "1"
                                            }
                                        }
                                    }
    \end{minted}
    \caption{
        Configuration that is supplied to the PyCont \mintinline{python}{MultiPumpController} class for instantiating a series of \mintinline{python}{C3000Controller} objects.
        The configuration details the following.
        \textbf{IO}: The serial communication parameters used by each pump.
        \textbf{Default}: Default values that are applied to each pump.
        \textbf{Pumps}: Collection of pumps that are to be used by the controller.
        Each pump supplies its address as part of the \mintinline{text}{"switch"} parameter and details any variation of the default parameters.
    }
    \label{fig:nanobot-pycont-multi-config}
\end{figure}

The multi-controller would create an instance of the C3000 controller for each device listed in the configuration, each with their own values set.
The multi-controller added these devices to an accessible key-value pair data structure that was accessible through each pump's given name.
This allowed for the user to access individual Triconts by name and call their \acrshort{api} methods.
One of the most powerful features of the multi-controller was the ability to request an \acrshort{api} call and apply it to each pump registered in the multi-controller (Figure \ref{fig:nanobot-pycont-multi-commands}).
This means that a single request, such as moving the syringe plunger to position 0, could be applied to each pump in the controller in a single call, leading to a cleaner interface for the \acrshort{api}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def apply_command_to_pumps(
            self, pumps: List[str], command: str, *args, **kwargs
        ) -> Dict[str, Any]:
            """Takes a command plus arguments and attempts to call the method on
            each pump listed in the controller.

            Args:
                pumps (List[str]): List of pump names
                command (str): API method to call

            Returns:
                Dict[str, Any]: Return codes for each pump
            """

            # Dictionary to hold return values
            return_calls = {}
            
            # Iterate through each pump in the supplied list
            for pump in pumps:
                # Check if the method exists on the pump
                if hasattr(self.pumps[pump], command):
                    # Get the method and call it, storing any return
                    func = getattr(self.pumps[pumps], command)
                    return_calls[pump] = func(*args, **kwargs)
            
            # Return the return calls
            return return_calls

    \end{minted}
    \caption{
        Method to apply a given command to each pump in a given list.
        Will check if the pump object has the appropriate method and attempt to call the method with arguments supplied.
        This is a utility function to apply a single command to multiple pumps at once.
    }
    \label{fig:nanobot-pycont-multi-commands}
\end{figure}

The main focus of the multi-pump controller was to create and collect a series of C3000 controllers under a single object so they could be accessible in one place.
Utility functions are mainly what the multi-controller offers whereas the actual control of the devices is defined in the C3000 controller class.