The \mintinline{python}{C3000Protocol} class was designed to use the DT Protocol specification to construct packets to be sent to the pump device and to decode responses received.
A series of constants were defined that represented each command available to the Tricont pump as stated in the reference software manual.
Using these commands, alongside the address of the chosen pump and any operational arguments, a series of \mintinline{python}{DTCommand} objects would be created that represented the command and would be chained together to form a \mintinline{python}{DTInstructionPacket} object that represented a DT protocol command block (Figure \ref{fig:nanobot-pycont-c3000-protocol}).
This command block would be returned to the calling object which would send it to the Tricont pump.
Any received response would be converted to a \mintinline{python}{DTStatus} object that would be decoded and spliced to return the status code of the previously sent command back to the calling code (Figure \ref{fig:nanobot-pycont-c3000-protocol-response}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def forge_packet(
            self, dtcommands: Union[List[DTCommand], DTCommand], execute: bool = True
        ) -> DTInstructionPacket:
            """Create a DTInstruction packet which represents the command block of the DT Protocol.
            Accepts either a single command or list of commands and chains them together to
            form a single DTInstructionPacket with appropriate syntax set.

            Args:
                dtcommands (Union[List[DTCommand], DTCommand]): Single or multiple DTCommands to
                    create an instruction packet with
                
                execute (bool, optional): Append the execute command (R) to the command to
                    execute the command right away. Defaults to True.

            Returns:
                DTInstructionPacket: DT Protocol command block to be sent to the pump device.
            """

            # Convert command to a list if it is a single command
            if isinstance(dtcommands, DTCommand):
                dtcommands = [dtcommands]
            
            # Append execute flag to the end of the command if set
            if execute:
                dtcommands.append(DTCommand(CMD_EXECUTE))
            
            # Create an instruction packet
            return DTInstructionPacket(self.address, dtcommands)

        def forge_deliver_packet(self, argument: int) -> DTInstructionPacket:
            """Create a DTInstructionPacket that represents the command for delivering a
            volume set by the argument.

            Args:
                argument (int): Operational argument value for the command

            Returns:
                DTInstructionPacket: Packet for the deliver command.
            """

            # Build the command and return the packet.
            dtcommand = DTCommand(CMD_DELIVER, str(argument))
            return self.forge_packet(dtcommand)

    \end{minted}
    \caption{
        Excerpt from the \mintinline{python}{C3000Protocol} class demonstrating how commands and packets is constructed.
        The methods \mintinline{python}{forge_[NAME]_packet()} represents commands that the Tricont can perform.
        These methods construct the command or commands from any given arguments and return a \textbf{DTInstructionPacket} object which represents the command block of the DT Protocol.
        As there are several of these methods, only one has been shown for clarity however they follow the same structure.
    }
    \label{fig:nanobot-pycont-c3000-protocol}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def decode_packet(self, dtresponse: bytes) -> Optional[Tuple[str, str, str]]:
            """Decodes the response received by the Tricont pump and returns the values to the user.

            Args:
                dtresponse (bytes): Raw response received from the Tricont.

            Returns:
                Optional[Tuple[str, str, str]]: Address, status code, and data if any; else None.
            """

            return DTStatus(dtresponse).decode()
    \end{minted}
    \caption{
        Decoding of received responses from a Tricont using the \mintinline{python}{C3000Protocol}.
        The raw response is passed to this method which constructs a \mintinline{python}{DTStatus} object from the raw bytes and returns the decoded data.
        This data is the address, the status code of the last operation, and any additional data.
    }
    \label{fig:nanobot-pycont-c3000-protocol-response}
\end{figure}