The core driver of the module consisted of a generic representation of an Ocean Insight spectrometer.
This generic class (\mintinline{python}{OceanOpticsSpectrometer} would accept a type of spectrometer from the user as a string (e.g. UV, IR, Raman, etc.) and from that, the supported model of spectrometer would be returned.
This was then passed to the \mintinline{python}{pySeabreeze} library which created its own representation of the spectrometer which the user could then interact with (Figure \ref{fig:nanobot-uv-core-driver-spec-model}).
Two methods were implemented for the core driver: \mintinline{python}{set_intgeration_time()} which would set the sampling time for the device, and \mintinline{python}{scan()} which would trigger the data collection (Figure \ref{fig:nanobot-uv-core-driver-scan}).
These methods were sufficient to obtain the raw data but the driver itself is open to more modification if required.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def _get_spectrometer(spec_class: str, spec_types: Dict[str, str]) -> seabreeze.Spectrometer:
            """Obtain a handle to a seabreeze Spectrometer from a supplied spectrometer type.

            Args:
                spec_class (str): Type of spectrometer to create (UV, IR, Raman)
                spec_types (Dict[str, str]): Spectrometer types and signatures.
                    USed in detecting connected spectrometer devices.

            Returns:
                seabreeze.Spectrometer: Handle to spectrometer device if found

            Raises:
                NoSpectrometerDetected: No spectrometers are plugged in.
                UnsupportedSpectrometer: Tyope of spectrometer is not supported
            """

            # Get all connected spectrometers from the seabreeze library
            devices = seabreeze.list_devices()

            # None found, not plugged in
            if not devices:
                raise NoSpectrometerDetected("No spectrometers are detected. Are they plugged in?")
            
            # Check if spectrometer is supported
            if spec_class in spec_types.keys():
                # Iterate through all connected devices
                for dev in devices:
                    # Device's name contains signature; this is the correct device
                    # Return a seabreeze spectrometer object
                    if spec_types[spec_class] in str(dev):
                        return seabreeze.Spectrometer(dev)
            
            # Spectrometer isn't supported yet
            raise UnsupportedSpectrometer(f"Spectrometer {spec_class} is unsupported")

    \end{minted}
    \caption{
        Creates a spectrometer object from the seabreeze library if detected.
        This is used as an internal spectrometer object that interacts with the hardware in the core driver's representation of a spectrometer.
    }
    \label{fig:nanobot-uv-core-driver-spec-model}
\end{figure}

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def scan(self, scans: int = 3) -> Tuple[List[float], List[float]]:
            """Obtains a spectrum from the spectrometer device.
            Scans for X times to obtain the intensities and averages the result.

            Args:
                scans (int): Number of scans to perform before averaging.
                    Defaults to 3.

            Returns:
                Tuple[List[float], List[float]]: Wavelengths and averaged intensities measured.
            """

            # List to store the intensity data
            average = []

            # Open communication with the attached spectrometer
            self._spectrometer.open()
            
            # Perform X scans
            for _ in range(scans):
                # Obtain data from the device
                wavelengths, intensities = self.spectrometer.spectrum()

                # Append intensity data to array for later
                average.append(intensities)

                # Wait before the next scan.
                time.sleep(self._delay)
            
            # Close communication with the device
            self.spectrometer.close()
            
            # Use Numpy to average the data 
            intensities = numpy.mean(average, axis=0)

            return (wavelength, intensities)

    \end{minted}
    \caption{
        Scan method for the \mintinline{python}{OceanOpticsSpectrometer} class.
        Calls on the spectrometer device to scan X times and storing the intensity data.
        This is then averages and returned to the user alongside the measured wavelengths.
    }
    \label{fig:nanobot-uv-core-driver-scan}
\end{figure}

Unlike previously described core drivers which power a single control driver, this one was designed to be the basis of many different control drivers, each for a different class of spectrometer (e.g. UV, IR, Raman, etc.).
For this project, it formed the foundation of the UV control driver, but support was added for different classes of device (IR, Raman) as these were being used in other projects not related to this work.
Full details of the core driver of this module can be found in Appendix \ref{apx:nanobot-uv-core-driver}.