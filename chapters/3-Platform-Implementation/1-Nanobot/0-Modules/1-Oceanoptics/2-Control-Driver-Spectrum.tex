The \mintinline{python}{UVSpectrum} class was responsible for curating the obtained data from the spectrometer and manipulating the data to display it in a graph format.
For any reference spectrum, this was simply responsible for holding the wavelength and intensity data.
For a sample spectrum (i.e. a spectrum of a measured reaction sample), the process was more involved.
The QEPro spectrometers only allow for the UV intensity of the sample to be monitored which is not ideal as UV spectra are usually measured in terms of the absorbance of light (\emph{A}).
To obtain the absorbance from the measured intensity at a given wavelength, the Beer-Lambert relation (Equation \ref{eq:beer-lambert}) can be used.
This is a relationship between the absorbance of light through a material and the properties of the material which is a liquid in this case.
This relation is the log\textsubscript{10} of the reference intensity (\emph{I\textsubscript{0}}) over the measured intensity (\emph{I}).

\begin{equation}
    A = \log_{10}{\frac{I_0}{I}}
    \label{eq:beer-lambert}
\end{equation}

\noindent
On creation of any \mintinline{python}{UVSpectrum} object, if a reference was given, this process was automatic (Figure \ref{fig:nanobot-uv-beer-lambert}).
To give the user the most flexibility, the \mintinline{python}{UVSpectrum} class offered the ability to plot any data held by the object and save it to disk for future reference.
This was done using the \mintinline{python}{matplotlib} library available through Python.
As with plotting, the ability to extract the raw data (wavelengths, intensities, absorbances, reference, etc.) into an easily digestible format (\acrshort{json} file) was made available to the user as well.
An example of a sample UV spectrum obtained from this module can be seen in Figure \ref{fig:nanobot-uv-example-sepctrum}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def beer_lambert(self):
            """Beer-Lambert relation to convert intensity data to absorbances.
            For each intensity in the reference and corresponding sample, take
            the log10 of the reference value over the measured value.
            """

            # Iterate through the reference and measured intensities
            # As these are be the same size at the same wavelength, they're related to one another
            for reference, measured in zip(self.reference["intensities"], self.intensities):
                # Attempt the conversion
                try:
                    # Some cases may have zero values, skip these
                    if ref == 0 or measured == 0:
                        continue
                    
                    # Perform the relation
                    self.absorbances.append(numpy.log10(reference / measured))
                
                # Generic exception in case something goes wrong
                except Exception:
                    break
            
            # Convert to numpy array for easy handling
            self.absorbances = numpy.array(self.absorbances)

            # Get the wavelength where the absorbance is at the highest value
            self.max_peak = self.wavelengths[numpy.argmax(self.absorbances)]
    \end{minted}
    \caption{
        Method to calculate absorbances from measured intensity data.
        Uses the Beer-Lambert relation together with the reference data to get the absorbance values for the sample.
    }
    \label{fig:nanobot-uv-beer-lambert}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.75]{uv-example.png}
    \caption{
        Example spectrum plotted from the data held in the \mintinline{python}{UVSpectrum} class.
        Highlights the absorbance data at each given wavelength with the maximum peak wavelength highlighted for convenience.
    }
    \label{fig:nanobot-uv-example-sepctrum}
\end{figure}