Similar to the Clusterbot \acrshort{api} (Section \ref{sec:clusterbot-software-api}), a custom \acrshort{api} was created (\mintinline{python}{NanobotManager}) for this system.
The movement of physical parts was handled by the Commanduino module in the same fashion as the Clusterbot system with the peristaltic pumps replaced with the Tricont pump module.
The additional modules were added as per specification detailed in Section \ref{sec:ulduar-manager-extension} with the small caveat of the spectrometers module which required an extra layer of checks (Figure \ref{fig:nanobot-manager-ctor}).

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class NanobotManager(ModularManager):
            """Extended Manager API for the Nanobot system.

            Args:
                config (str): Configuration file
            """

            def __init__(self, config: str):
                # Instantiate the base manager
                super().__init__(config)

                # Create Tricont module if entry in the configuration
                self.triconts = None
                if "triconts" in config["modules"]:
                    from .tricont_driver import TricontControlDriver
                    self.triconts = TricontControlDriver(config=config["triconts"])
                
                # Create UV module if entry in the configuration
                self.uv = None
                if "spectrometers" in config["modules"]:
                    # Check for a UV spectrometer entry in the spectrometer section
                    if "uv" in config["spectrometers"]:
                        from .spectrometer_drivers import QEPro2192
                        self.uv = QEPro2192(config["spectrometers"]["uv"])

                # Use Commanduino module to home all the motors.
                self.home_all_motors()
    \end{minted}
    \caption{
        Constructor for the \mintinline{python}{NanobotManager} \acrshort{api}.
        Support is added to instantiate the Tricont module if an entry in the config is found and similarly for the spectrometers.
        As many spectrometers may be added in the future, they are manually searched for in the config when creating the \acrshort{api} object.
    }
    \label{fig:nanobot-manager-ctor}
\end{figure}

The main focus of this \acrshort{api} was to facilitate the movement of liquids through an in-line UV flow cell so that UV spectra could be obtained for a given reaction.
To aid with the ability to call the spectrometer and obtain a spectrum object, a method was defined (\mintinline{python}{call_spectrometers()}) that would allow the user to determine what type of spectrum they wished to obtain (reference or sample).
This method also allowed for the extension if other spectrometers were to be added.
To move the reaction sample into the UV flow cell, a dedicated method (\mintinline{python}{obtain_spectra()}) was created to allow for this behaviour.
This method would do the following:

\begin{itemize}
    \item Lower the sample module into position with the Commanduino module.
    \item Call the Tricont module to move sample into the in-line flow cell.
    \item Call any spectrometers and obtain the spectrum with the Spectrometer module.
    \item Move remaining sample back into sample vial with the Triconts.
    \item Raise the module back to home position with Commanduino.
\end{itemize}

This allowed the user to dispense their reactions and when the time came, autonomously obtain spectral samples of their reaction mixtures.
After a sample had been taken, the in-line flow cell had to be adequately cleansed of the previous reaction so as not to contaminate future samples.
A cleaning routine was developed that would move the sample to waste and flush the tubing with appropriate cleaning solvents and remove the solvents to a waste drum.
This routine was repeated numerous times, washing with water and acid, to ensure no contaminants were left in the lines (Figure \ref{fig:nanobot-api-cleaning}).
Once clean, the cycle of obtaining spectra and cleansing would repeat for how many reactions were present on the Geneva wheel mechanism.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        def clean_routine(self, cycles: int = 3):
            """Cleans the sample vial and in-line UV flow cell with water and acid.

            Args:
                cycles (int): Number of cleaning cycles to perform.
                    Defaults to 3.
            """

            # Lower the sample station motor to its lowest position (into the vial)
            self.move_motor_to_position("sample", LOW_POSITION)

            # Iterate X times for cleaning
            for _ in range(cycles):
                # Standalone method to clean vial and flow cell with water and acid
                self.clean_vial_and_lines()
            
            # Move vial contents (cleaning solvents) to the waste drum
            self.triconts.transfer("sample", 12.5, "1", "6")

            # Move the sample station motor back to its home position.
            self.home_motor("sample")
    \end{minted}
    \caption{
        Method to clean a sample vial and the UV flow cell tubing.
        First lowers the sample module into the vial and proceeds to call on the cleaning routine X times until the lines are purged of contaminants.
        Once complete, the cleaning solvents are moved to a waste drum and the sample module is raised out of the vial.
        This is repeated for every vial on the Geneva wheel system.
    }
    \label{fig:nanobot-api-cleaning}
\end{figure}

The \mintinline{python}{NanobotManager} \acrshort{api} was a small extension on the base \mintinline{python}{ModularManager} which implemented methods to allow for spectrometers to be called from the Spectrometer module and had dedicated cleaning routines to ensure the sample tubing was cleansed of contaminants.
Although not a vast addition to the base \acrshort{api}, it offered all the functionality to be able to automate dispensing, sampling, and cleaning of the system in a simple and lean fashion that was fit for the project's needs with the option to extend in the future if required.