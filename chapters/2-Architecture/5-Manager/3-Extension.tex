One of the main features of the Modular Manager is its extensibility.
As stated previously, the base version of the \acrshort{api} can be built upon to include more software modules each with their own control methods and \acrshort{api} calls through inheritance.
Inheriting from the base class allows the "extended" manager access to all base-level functionality of the \acrshort{api} as well as its own "extended" \acrshort{api} methods utilising the loaded software modules for that specific system (Figure \ref{fig:ulduar-manager-extended-simple}).
This allows for platforms that share a common core (Commanduino Module) but different capabilities (Different Software Modules) to use the same configuration setup and software architecture which ensures easier development and maintenance of these systems.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.2]{ulduar-extended-class.png}
    \caption{
        Simplified illustration of how the Extended Manager inherits from the \mintinline{text}{ModularManager}.
        All functionality defined in the base-level ModularManager is inherited by the \mintinline{text}{ExtendedManager} which implements its own \acrshort{api} logic for it's loaded software modules.
        This allows for all Ulduar systems to retain a similar software structures whilst being able to differ physically.
    }
    \label{fig:ulduar-manager-extended-simple}
\end{figure}

To extend the Ulduar framework for a more developed Manager \acrshort{api}, the convention is to inherit the base level \mintinline{text}{ModularManager} class and name this child \mintinline{text}{[SYSTEM NAME]Manager} (e.g. \mintinline{text}{ExtendedManager}).
The configuration file for an extended system will contain all new software modules the user wishes to include along with their configurations (similar to Figure \ref{fig:ulduar-manager-config}).
The appropriate software module control drivers are then loaded up and instantiated by the Manager.
The developer must then define the \acrshort{api} calls they wish to include in their system.
As stated previously, as every system is different, this is at the developer and end-user's discretion to what they wish to include and how it is implemented.
This extension style of design for the Ulduar framework \acrshort{api} allows for a much wider support for a variety of different system designs.
These systems can be designed in a modular fashion, being able to insert and remove hardware as and when required and the Ulduar \acrshort{api}s support this behavior natively.
This give both the developer and system designer ultimate control in what hardware they can include in their Ulduar system, the only limit being the ability to communicate with the hardware itself.
Example of an extended Manager \acrshort{api} (\mintinline{text}{ExtendedManager}) can be seen in Figure \ref{fig:ulduar-manager-extended}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        class ExtendedManager(ModularManager):
            """Extended variant of the ModularManager class.

            These "extended" classes allow for more variety in the software modules
            that can be loaded into Ulduar Systems.
            Each extended API retains all functionality defined in the base
            `ModularManager` class plus all functionality defined within the child.

            Args:
                configuration (str): Configuration file to be loaded for the system.
            """

            def __init__(self, configuration: str):
                # Initialise the base Modular Manager class
                super()_.__init__(configuration)

                # Instantiate a pump module for this API
                self.pump_driver = None
                if 'pumps' in self.modules:
                    self.pump_driver = PumpControlDriver(cfg=self.config["pumps"])
                
                # Instantiate a camera module for this API
                self.camera_driver = None
                if 'camera' in self.modules:
                    self.camera_driver = CameraControlDriver(cfg=self.config["camera"])

            """Here is where ALL API calls for this system will be defined.
            This can be any number of methods that use any of the loaded software modules
            """

            def move_horizontal_motor_and_dispense(self, pump: str, volume: float):
                """Example method bases loosely on a base method included in the Modualr Manager.
                Will move motors to a specific position, dispense a liquid, activate a camera, and
                home all motors.

                Args:
                    pump (str): Name of the pump to dispense liquid from
                    volume (float): Volume to dispense
                """

                # Move motors into position
                self.cmduino.move_motor_to_position("horizontal_motor", 5000)
                self.cmduino.move_motor_to_position("arm_motor", 20000)

                # Dispense liquid and take an image
                self.pump_driver.dispense(pump, volume)
                self.camera_driver.take_image("image.jpg")

                # Home all motors.
                self.cmduino.home_motor("arm_motor")
                self.cmduino.home_motor("horizontal_motor")

    \end{minted}
    \caption{
        Example of an extended Modular Manager.
        Instantiates software modules defined in the configuration that can be used to define API calls that represent operations the robotic system can perform.
        Any number of API calls be defined that call upon any number of supported software modules.
        All systems using the Ulduar architecture follow this software structure so allows for easier maintenance and development of these systems.
    }
    \label{fig:ulduar-manager-extended}
\end{figure}