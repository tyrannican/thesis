The primary function of the \textbf{Core Driver} is the initialisation of the module itself.
With each module being designed to control a section of hardware in the Ulduar systems, the core driver is responsible for providing configurations and settings to the device when it is powered up.
Any initialisation routines are also meant to occur at this level.
This means that if a device has any requirements for example homing a motor or resetting readings, then these routines occur down at this level.
The core driver is intended to be a base-level Python class in which the \textbf{Control Driver} is meant to inherit from.
For implementations, the convention used is to give the \textbf{Core Driver} a name and append the \mintinline{python}{CoreDriver} suffix (e.g. \mintinline{python}{PumpCoreDriver}).

\subsubsection{Initialisation \& Setup}
For the Commanduino \textbf{Core Driver}, all initialisation occurred in the \mintinline{python}{CommanduinoCoreDriver} Python class.
The driver created an instance of the Commanduino \acrlong{cm} which was initialised with a supplied configuration (Section \ref{sec:cmd-manager}).
Initialisation of the Arduino board was done via the \acrlong{cm} but separate methods were written for a more custom initialisation procedure; in this case this was to home a selection of motors.
To allow for easy access to \acrlong{cd} objects stored in the \acrlong{cm}, a helper method was defined that would check the validity of any device name that was given; the device name would be checked for in the \acrlong{cm}'s \mintinline{text}{device} contents.
The method will only return true if the supplied device name is present in the \acrlong{cm}'s device list.
This method was defined to act as a helper in the \mintinline{python}{ControlDriver} section of the stack.
Before accessing any underlying \acrlong{cd} objects, this method is used to check for its presence before attempting to obtain it, thus reducing errors and the potential for crashes.
An example of a \textbf{Core Driver} for the Commanduino suite is shown in Figure \ref{fig:ulduar-core-driver-ctor}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        """Example of a CoreDriver for the Arduino control via Commanduino.
        This class is responsible for the base level initialisation of the device
        and any other low-level setup.
        Depending on the system setup, this can be subject to change.
        """

        # Import the CommandManager from Commanduino
        from commanduino import CommandManager

        class CommanduinoCoreDriver:
            """Core Driver for the Arduino controller.

            This will create an instance of the CommandManager and supply
            base level methods for initialising and helper methods.

            Args:
                configuration (Dict): Commanduino configuration
            """

            def __init__(self, configuration: Dict):
                # Save the configuration for future use
                self.cfg = configuration

                # Create an instance of a CommandManager
                self.manager = CommandManager.from_config(configuration)

                # Home all motors once created
                self.home_motors()

            """ Base Level initialisation routines are defined here """

            def home_motors(self):
                """Homes all motors loaded onto the board.
                As motors may have different names, this is included for
                demonstration purposes
                """

                # Home all motors numbered 1-5
                for i in range(1, 6):
                    self.manager[f"motor{i}"].home()

            def valid_device(self, device_name: str) -> bool:
                """Checks if a supplied device name is present in the CommandManager's
                device list.

                Args:
                    device_name (str): name of the device to check for.

                Returns:
                    bool: If the device is present or not.
                """

                return device_name in self.manager.devices
    \end{minted}
    \caption{
        Example of a \textbf{Core Driver} for the Commanduino suite.
        An instance of a \acrlong{cm} is created with a supplied configuration to initialise the board and helper methods defined to help at the higher levels of the module stack (\textbf{Control Driver} sections).
    }
    \label{fig:ulduar-core-driver-ctor}
\end{figure}