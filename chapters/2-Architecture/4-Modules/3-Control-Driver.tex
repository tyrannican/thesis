The \textbf{Control Driver} section of a software module is the main interface with the underlying hardware.
This higher-level section of the module is where all control methods for the device are defined.
The \textbf{Control Driver} acts as a child class of the \textbf{Core Driver} so that it can access the initialisation and helper methods in lower levels of the stack.
Using this inheritance approach, the software module can have separate sections responsible for different layers of the stack but also contain these to a single point-of-access object that can be called from the central \acrshort{api}.
The \textbf{Control Driver} follows a similar naming convention to the \textbf{Core Driver} in that the \mintinline{text}{ControlDriver} suffix is appended to the driver's given name.

\subsubsection{Implementation}
As the \textbf{Control Driver} is a child of the \textbf{Core Driver}, the constructor of the control object calls upon the base level constructor, passing in any configuration and initialisation parameters (Figure \ref{fig:ulduar-control-ctor}).
The class offers a series of control functionality that uses the Commanduino \acrlong{cm} to access devices and allow the user full control of the hardware.
These control methods were designed to encompass all the relevant operations that a user may wish to undertake when controlling the underlying Arduino hardware.
These operations contain, but are not limited to:

\begin{itemize}
    \item Move any motor to a specific position.
    \item Run any motor for a fixed period of time.
    \item Reverse any motor's direction.
    \item Set the running speed of any motor.
    \item Read or write to any pin that is available to the system.
\end{itemize}

These operations allow for control of any stepper motor or analog/digital pins available to Commanduino.
Given that Commanduino offers a whole breadth of available \acrlong{cd}s, wrapper methods to control each of them can be added as and when required.
The operations defined here are enough to get an automated system utilising stepper motors running but can be expanded in the future.
An overarching view of the class structure of the Commanduino Control Driver can be seen in Figure \ref{fig:ulduar-control-class-diagram}.

\begin{figure}[H]
    \begin{minted}[fontsize=\scriptsize]{python}
        """Example of the inheritance of a Control Driver of the base level Core Driver
        for the Commanduino software module.
        """

        # Import the Core Driver
        from .core_driver import CommanduinoCoreDriver

        class CommanduinoControlDriver(CommanduinoCoreDriver):
            """Control Driver for the Commanduino software module.
            Here, all wrapper methods for controlling underlying Command Devices
            registered with the Command Manager are defined.

            This gives the user higher level access to the underlying hardware
            and more fine-grained control.

            Args:
                configuration (Dict): Configuration for the Commanduino Core Driver.
            """

            def __init__(self, configuration: Dict):
                # Call the parent constructor to gain access to underlying
                # capabilities
                super().__init__(configuration)

            """ Control methods are defined here """

            def move_motor_to_position(self, device_name: str, position: int):
                """Moves a motor on the Arduino to a specific position.

                Args:
                    device_name (str): Name of the device defined in Commanduino
                    position (int): Position to move to (in steps).
                """

                # Check the device is a valid device in Commanduino's Command Manager
                if self.valid_device(device_name):
                    # Get the motor object
                    device = self.manager[device_name]

                    # Move the motor to the requested position.
                    device.move_motor_to_position(position)
    \end{minted}
    \caption{
        Example of the Commanduino Control Driver constructor.
        It inherits all functionality of the Core Driver where all initialisation of the hardware occurs.
        This allows the Control Driver to access the lower-level items defined in the lower layers (e.g. the \acrlong{cm}).
    }
    \label{fig:ulduar-control-ctor}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.2]{Ulduar-Control-Driver-Class.png}
    \caption{
        Class diagram demonstrating how the Commanduino Control Driver layout.
        As shown in Figure \ref{fig:ulduar-control-ctor}, the Control Driver inherits the Core Driver and defines its own methods for interacting with the underlying hardware of the Arduino.
    }
    \label{fig:ulduar-control-class-diagram}
\end{figure}